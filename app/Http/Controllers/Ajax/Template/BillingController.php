<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Billing;
use App\BillingInvoice;
use App\BillingInvoiceRow;
use App\BillingQuote;
use App\BillingQuoteRow;
use App\BillingQuoteStorage;
use App\BillingService;
use App\BillingStorage;
use App\BillingVat;
use App\ClockingSystem;
use App\Company;
use App\Http\Controllers\Controller;
use App\Model\Organisation;
use App\Project;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BillingController extends Controller
{
    public function showAllQuotiesForProject(Project $project, Billing $billing)
    {
        $quoties = BillingQuote::where('billing_id','=',$billing->id)->paginate(10);
        $services = BillingService::all();

        return view('back.crm.quoty.quoty.template.index',[
            'project' => $project,
            'billing' => $billing,
            'services' => $services,
            'quoties' => $quoties
        ]);
    }

    public function loadWorky($projectId, $billingId)
    {
        $allWorkys = ClockingSystem::where('completed', '=', 1)->where('project_id', '=', $projectId)->get();

        $invoiceRows = collect();
        foreach ($allWorkys as $workys) {
            $records = collect([
                'service' => $workys['billing_service_id'],
                'description' => $workys['job'],
                'price' => $workys['price'],
                'vat' => ($workys['vat'] . " %"),
                'hour' => $this->calculateHoursFromSeconds($workys['total_time']),
                'total_price_excl' => ("€ " . $this->calculatePrice($workys['price'], $workys['total_time'])),
                'total_price_incl' => ("€ " . $this->calculatePriceInclVat($workys['price'], $workys['total_time'], $workys['vat'])),
                'vat_diff' => ("€ " . $this->calculateVatDiff($this->calculatePrice($workys['price'], $workys['total_time']), $this->calculatePriceInclVat($workys['price'], $workys['total_time'], $workys['vat']))),
            ]);
            $invoiceRows->push($records);
        }
        $allVat = BillingVat::all();
        $services = BillingService::all();
        return view('back.crm.forms.billing.invoice.create', [
            'invoiceRows' => $invoiceRows,
            'allVat' => $allVat,
            'services' => $services
        ]);

    }

    public function createInvoicyFromWorky(Request $request, $projectId, $billingId)
    {
        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ],$message);


        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.invoice.create', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $invoiceArray = $request->tableArray;

        $project = Project::findOrFail($projectId);
        $billing = Billing::all()->where('project_id', '=', $project->id)->first();

        $billingInvoice = new BillingInvoice();
        $billingInvoice->billing_id = $billing->id;
        $billingInvoice->save();


        foreach ($invoiceArray as $item) {
            BillingInvoiceRow::create([
                'billing_invoice_id' => $billingInvoice->id,
                'description' => $item['description'],
                'price' => $item['price'],
                'hour' => $item['hour'],
                'vat' => $item['vat'],
                'discount' => $item['discount'],
                'total_price_incl' => $item['total_price_incl'],
                'total_price_excl' => $item['total_price_excl'],
                'vat_diff' => $item['vat_diff'],
                'created_at' => Carbon::now()->timezone('Europe/Amsterdam'),
            ]);
        }

        session()->flash('success', 'Succesfully created Invoicy from Workys');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }

    public function createBilling($projectId, $organisationId)
    {
        $billing = new Billing();
        $billing->organisation_id = $organisationId;
        $billing->project_id = $projectId;
        $billing->save();

        return $billing;
    }

    public function showInvoicy($projectId, $billingId, $invoicyId)
    {


        $invoiceRows = BillingInvoiceRow::where('billing_invoice_id', '=', $billingId)->get();
        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.crm.forms.billing.invoice.create', [
            'invoiceRows' => $invoiceRows,
            'allVat' => $allVat,
            'services' => $services
        ]);
    }

    public function showQuoty($projectId, $billingId, $quotyId)
    {


        $quoteRows = BillingQuoteRow::where('billing_quote_id', '=', $billingId)->get();

        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.crm.forms.billing.quote.create', [
            'invoiceRows' => $quoteRows,
            'allVat' => $allVat,
            'services' => $services
        ]);
    }

    public function updateQuoty(Request $request, $projectId, $billingId, $quotyId)
    {


        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ], $message);

        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.quote.update', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $quoteArray = $request->tableArray;

        $billingQuoteId = BillingQuote::findOrFail($quotyId);

        foreach ($quoteArray as $item) {
            $quoteRow = BillingQuoteRow::updateOrCreate([
                'id' => $item['id']
            ], [
                'billing_quote_id' => $billingQuoteId->id,
                'billing_service_id' => $item['service'],
                'description' => $item['description'],
                'price' => $item['price'],
                'hour' => $item['hour'],
                'vat' => $item['vat'],
                'discount' => $item['discount'],
                'total_price_incl' => $item['total_price_incl'],
                'total_price_excl' => $item['total_price_excl'],
                'vat_diff' => $item['vat_diff'],
                'updated_at' => Carbon::now()->timezone('Europe/Amsterdam'),
            ]);
        }

        session()->flash('success', 'Succesfully updated Invoicy');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project = Project::findOrFail($projectId)
        ]);

    }

    public function updateInvoicy(Request $request, $projectId, $billingId, $invoicyId)
    {
        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ], $message);

        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.invoice.create', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $invoiceArray = $request->tableArray;

        $billingInvoiceid = BillingInvoice::findOrFail($invoicyId);

        foreach ($invoiceArray as $item) {
            $invoiceRow = BillingInvoiceRow::updateOrCreate(
                [
                    'id' => $item['id']
                ],
                [
                    'billing_invoice_id' => $billingInvoiceid->id,
                    'billing_service_id' => $item['service'],
                    'description' => $item['description'],
                    'price' => $item['price'],
                    'hour' => $item['hour'],
                    'discount' => $item['discount'],
                    'total_price_incl' => $item['total_price_incl'],
                    'total_price_excl' => $item['total_price_excl'],
                    'vat_diff' => $item['vat_diff'],
                    'updated_at' => Carbon::now()->timezone('Europe/Amsterdam'),
                ]
            );
        }

        session()->flash('success', 'Succesfully updated Invoicy');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project = Project::findOrFail($projectId)
        ]);

    }
    //TODO:: ADD CUSTOM MESSAGES
    //TODO:: FIX TOTAL PRICE IN JS
    //TODO:: change func name into createInvoice
    //TODO:: Add VAT BACK!!!
    public function create(Request $request, $projectId)
    {
        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ], $message);


        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.invoice.create', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $invoiceArray = $request->tableArray;

        $project = Project::findOrFail($projectId);
        $billing = Billing::all()->where('project_id', '=', $project->id)->first();

        $billingInvoice = new BillingInvoice();
        $billingInvoice->billing_id = $billing->id;
        $billingInvoice->save();

        foreach ($invoiceArray as $item) {
            BillingInvoiceRow::create([
                'billing_invoice_id' => $billingInvoice->id,
                'billing_service_id' => $item['service'],
                'description' => $item['description'],
                'price' => $item['price'],
                'hour' => $item['hour'],
                'discount' => $item['discount'],
                'total_price_incl' => $item['total_price_incl'],
                'total_price_excl' => $item['total_price_excl'],
                'vat_diff' => $item['vat_diff'],
                'created_at' => Carbon::now()->timezone('Europe/Amsterdam'),
            ]);
        }

        session()->flash('success', 'Succesfully created Invoice');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }

    public function createQuote(Request $request, $projectId, $billingId)
    {
        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ], $message);

        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.invoice.create', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $quoteArray = $request->tableArray;
        $project = Project::findOrFail($projectId);

        $billingQuote = new BillingQuote();
        $billingQuote->billing_id = $billingId;
        $billingQuote->save();

        foreach ($quoteArray as $item) {
            BillingQuoteRow::create([
                'billing_quote_id' => $billingQuote->id,
                'description' => $item['description'],
                'price' => $item['price'],
                'hour' => $item['hour'],
                'vat' => $item['vat'],
                'discount' => $item['discount'],
                'total_price_incl' => $item['total_price_incl'],
                'total_price_excl' => $item['total_price_excl'],
                'vat_diff' => $item['vat_diff'],
                'created_at' => Carbon::now()->timezone('Europe/Amsterdam'),
            ]);
        }

        session()->flash('success', 'Succesfully created Quoty');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }

    public function createQuoteForOverview(Request $request, $projectId, $billingId)
    {
        $message = [
            'required' => 'This field is required.',
            'numeric' => 'This :field only accepts numbers.',
            'between' => 'discount has to be between 0 and 100',
        ];

        $validator = Validator::make($request->tableArray, [
            '*.service' => 'required',
            '*.description' => 'required|min:3|max:30',
            '*.price' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.hour' => 'required|numeric|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            '*.discount' => 'required|numeric|between:0,100|regex:/^[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ], $message);

        $allVat = BillingVat::all();
        $services = BillingService::all();
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.billing.invoice.create', [
                    'allVat' => $allVat,
                    'services' => $services
                ])->withErrors($validator)->render()
            ), 400);
        }

        $quoteArray = $request->tableArray;
        $project = Project::findOrFail($projectId);

        $billingQuote = new BillingQuote();
        $billingQuote->billing_id = $billingId;
        $billingQuote->save();

        $billing = Billing::findOrFail($billingId);

        foreach ($quoteArray as $item) {
            BillingQuoteRow::create([
                'billing_quote_id' => $billingQuote->id,
                'description' => $item['description'],
                'price' => $item['price'],
                'hour' => $item['hour'],
                'vat' => $item['vat'],
                'discount' => $item['discount'],
                'total_price_incl' => $item['total_price_incl'],
                'total_price_excl' => $item['total_price_excl'],
                'vat_diff' => $item['vat_diff'],
                'created_at' => Carbon::now()->timezone('Europe/Amsterdam'),
            ]);
        }

        session()->flash('success', 'Succesfully created Quoty');
        return app()->call('App\Http\Controllers\Ajax\Template\billingController@showAllQuotiesForProject', [
            'project' =>$project,
            'billing' => $billing
        ]);
    }

    public function generateInvoice($projectId, $billingId, $billingTypeId)
    {
        $billingType = 'invoice';

        $billing = Billing::findOrFail($billingId);


        $this->generatePDF($billingType, $billing, $billingTypeId);

        $project = Project::findOrFail($projectId);


        session()->flash('success', 'Succesfully generated Invoice');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }

    public function generateQuote($projectId, $billingId, $billingTypeId)
    {
        $billingType = 'quote';
        $billing = Billing::findOrFail($billingId);

        $this->generatePDF($billingType, $billing, $billingTypeId);

        $project = Project::findOrFail($projectId);

        session()->flash('success', 'Succesfully generated quote');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }


    public function generatePDF($billingType, $billingId, $billingTypeId)
    {

        switch ($billingType) {
            case "invoice":
                $billingInvoice = BillingInvoice::findOrFail($billingTypeId);

                $billingInvoiceRows = BillingInvoiceRow::where('billing_invoice_id', '=', $billingInvoice->id)->get();

                $company = Company::all();

                $billing = Billing::findOrFail($billingId);

                $organisation = Organisation::findOrFail($billing->first()->organisation_id);

                $project = Project::findOrFail($billing->first()->project_id);
                $date = Carbon::now('Europe/Amsterdam')->format('Y-md');
                $totalPrice = 0;
                foreach ($billingInvoiceRows as $iRows) {
                    $totalPrice += $iRows->total_price_incl;
                }
                //$settings = Settings::get();
                $pdf = PDF::loadview('PDF.billing.invoice.invoice', [
                    'billingInvoiceRows' => $billingInvoiceRows,
                    'billingInvoice' => $billingInvoice,
                    'project' => $project,
                    'company' => $company->first(),
                    'totalPrice' => $totalPrice,
                    'organisation' => $organisation,
                ])->setPaper('a4', 'landscape');
                $filename = 'invoicy_' . $project->name . '_' . $organisation->name . "_" . $billingInvoice->id . '_' . $date;
                $path = storage_path('pdf/billing/invoice/');
                $file = $path . $filename . '.pdf';
                $pdf->save($file);


                $billingInvoice->has_pdf = 1;
                $billingInvoice->update();

                $storage = new BillingStorage();
                $storage->path = $path;
                $storage->name = $filename;
                $storage->billing_invoice_id = $billingInvoice->id;
                $storage->save();

                return $pdf->download('invoicy_' . $project->name . '_' . $organisation->name . '_' . $date . '.pdf');
                break;
            case "quote":

                $billingQuote = BillingQuote::findOrFail($billingTypeId);

                $billingQuoteRows = BillingQuoteRow::where('billing_quote_id', '=', $billingQuote->id)->get();

                $company = Company::all();

                $billing = Billing::findOrFail($billingId);

                $organisation = Organisation::findOrFail($billing->first()->organisation_id);

                $project = Project::findOrFail($billing->first()->project_id);
                $date = Carbon::now('Europe/Amsterdam')->format('Y-md');
                $totalPrice = 0;
                foreach ($billingQuoteRows as $iRows) {
                    $totalPrice += $iRows->total_price_incl;
                }

                //$settings = Settings::get();
                $pdf = PDF::loadview('PDF.billing.quote.quote', [
                    'billingQuoteRows' => $billingQuoteRows,
                    'billingQuote' => $billingQuote,
                    'project' => $project,
                    'company' => $company->first(),
                    'totalPrice' => $totalPrice,
                    'organisation' => $organisation,
                ])->setPaper('a4', 'landscape');
                $filename = 'quoty_' . $project->name . '_' . $organisation->name . "_" . $billingQuote->id . '_' . $date;
                $path = storage_path('pdf/billing/quote/');
                $file = $path . $filename . '.pdf';
                $pdf->save($file);


                $billingQuote->has_pdf = 1;
                $billingQuote->update();

                $storage = new BillingQuoteStorage();
                $storage->path = $path;
                $storage->name = $filename;
                $storage->billing_quote_id = $billingQuote->id;
                $storage->save();

                return $pdf->download('quoty_' . $project->name . '_' . $organisation->name . '_' . $date . '.pdf');
                break;
        }

    }

    public function calculateHoursFromSeconds($seconds)
    {
        $hours = $seconds / 3600;

        return round($hours, 2, PHP_ROUND_HALF_EVEN);
    }

    public function calculateVatDiff($priceExcl, $priceIncl)
    {
        $vatDiff = $priceIncl - $priceExcl;
        return round($vatDiff, 2, PHP_ROUND_HALF_UP);
    }

    public function calculatePrice($price, $seconds)
    {

        $minutes = $seconds / 60;
        $hours = $minutes / 60;
        $totalPrice = $price * $hours;

        return round($totalPrice, 2, PHP_ROUND_HALF_UP);
    }

    public function calculatePriceInclVat($price, $seconds, $vat)
    {
        $totalVat = (100 + $vat) / 100;
        $totalPriceInclVat = $this->calculatePrice($price, $seconds) * $totalVat;
        return round($totalPriceInclVat, 2, PHP_ROUND_HALF_UP);
    }


}
