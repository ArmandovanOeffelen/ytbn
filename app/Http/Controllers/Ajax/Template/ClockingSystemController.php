<?php

namespace App\Http\Controllers\Ajax\Template;

use App\BillingService;
use App\BillingVat;
use App\ClockingSystem;
use App\ClockingSystemHours;
use App\Http\Controllers\Controller;
use App\Project;
use App\WorkyGeneral;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClockingSystemController extends Controller

{

    public function show($projectid)
    {

        $project = Project::findOrFail($projectid);

        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.crm.forms.clockingSystem.create_completed', [
            'allVat' => $allVat,
            'services' => $services
        ]);

    }

    public function showNewWorky($projectid)
    {

        $project = Project::findOrFail($projectid);

        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.crm.forms.clockingSystem.start', [
            'services' => $services
        ]);

    }


    public function start(Request $request, $projectid)
    {

        $validator = Validator::make($request->all(), [
            'service' => 'required',
            'job' => 'required',
            'price' => 'required',
            'vat' => 'required'
        ]);


        $project = Project::findOrFail($projectid);
        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.clockingSystem.start', [
                    'project' => Project::findOrFail($projectid),
                    'services' => BillingService::all(),
                ])->withErrors($validator)->render()
            ), 400);
        }

        $worky = new ClockingSystem();
        $worky->project_id = $projectid;
        $worky->organisation_id = $project->organisation_id;
        $worky->billing_service_id = $request->service;
        $worky->price = $request->price;
        $worky->vat = $request->vat;
        $worky->paused = 0;
        $worky->completed = 0;
        $worky->total_time = 0;
        $worky->job = $request->job;
        $worky->save();

        $workyHours = new ClockingSystemHours();
        $workyHours->start_time = Carbon::now()->timezone('Europe/Amsterdam');
        $workyHours->worky_id = $worky->id;
        $workyHours->employee_id = Auth::user()->id;
        $workyHours->save();


        session()->flash('success', 'Successfully added worky.');

        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }


    public function pause($projectid, $id)
    {
        $project = Project::findOrFail($projectid);

        $findWorkyToPause = ClockingSystemHours::where('total_time', '=', null)->where('worky_id', '=', $id)->whereNull('end_time')->get();


        if ($findWorkyToPause->isEmpty()) {
            session()->flash('error', 'Sorry! We are not able to pause the Worky. Please contact support@ytbn.com with the following error-code: 0203.');

            return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
                'project' => $project
            ]);
        }

        $workyHours = ClockingSystemHours::findOrFail($findWorkyToPause->first()->id);


        $workyHours->end_time = Carbon::now()->timezone('Europe/Amsterdam');


        $starttime = Carbon::createFromTimeString($workyHours->start_time);
        $endtime = Carbon::createFromTimeString($workyHours->end_time);
        $starttime->format('H:i:s');
        $endtime->format('H:i:s');

        $totalTime = $starttime->diffInSeconds($endtime);
        $workyHours->total_time = $totalTime;

        $workyHours->update();


        $worky = ClockingSystem::findOrFail($workyHours->worky_id);
        $worky->paused = 1;

        $calculatedTotalTime = $this->calculateTotalTime($worky->total_time, $totalTime);
        $worky->total_time = $calculatedTotalTime;

        $totalPrice = $this->calculatePriceInclVat($worky->price, $calculatedTotalTime, $worky->vat);

        $worky->total_price = $totalPrice;
        $worky->update();


        session()->flash('success', 'Successfully paused worky.');

        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }


    public function unpause($projectid, $id)
    {

        $object = ClockingSystem::findOrFail($id);
        $object->paused = 0;
        $object->update();

        $object1 = new ClockingSystemHours();
        $object1->start_time = Carbon::now()->timezone('Europe/Amsterdam');
        $object1->worky_id = $object->id;
        $object1->employee_id = Auth::user()->id;
        $object1->save();


        $project = Project::findOrFail($projectid);

        session()->flash('success', 'Successfully unpaused worky.');

        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }

    public function restart($projectid, $id)
    {

        $findWorkytoRestart = ClockingSystem::find($id);

        $project = Project::findOrFail($projectid);

        if ($findWorkytoRestart === null) {

            session()->flash('error', 'Sorry! We are not able to pause the Worky. Please contact support@ytbn.com with the following error-code: 0205.');

            return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
                'project' => $project
            ]);
        }
        $findWorkytoRestart->completed = 0;
        $findWorkytoRestart->update();


        $createWorkyHours = new ClockingSystemHours();
        $createWorkyHours->start_time = Carbon::now()->timezone('Europe/Amsterdam');
        $createWorkyHours->worky_id = $findWorkytoRestart->id;
        $createWorkyHours->employee_id = Auth::user()->id;
        $createWorkyHours->save();

        session()->flash('success', 'Succesfully restarted worky');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }

    public function delete($projectid, $id)
    {
        $Worky = ClockingSystem::findOrFail($id);
        $Worky->delete();

        $workyHours = ClockingSystemHours::where('worky_id', '=', $id)->get();

        //Build this security in otherwise might spit out an error.
        if (!$workyHours->isEmpty()) {
            foreach ($workyHours as $deletingHours) {
                $deletingHours->delete();
            }
        }


        $project = Project::findOrFail($projectid);

        session()->flash('success', 'Succesfully deleted worky');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }

    public function createCompletedWorky(Request $request, $projectid)
    {
        $validator = Validator::make($request->all(), [
            'job' => 'required',
            'price' => 'required',
            'minutes' => 'required',
            'hours' => 'required',
            'service' => 'required',
        ]);

        $project = Project::findOrFail($projectid);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.clockingSystem.create_completed', [

                    'project' => Project::findOrFail($projectid),
                    'services' => BillingService::all()
                ])->withErrors($validator)->render()
            ), 400);
        }
        $hours = $request->hours;
        $minutes = $request->minutes;

        $totalminutes = $hours + $minutes;


        $seconds = $hours * 3600;
        $seconds += $minutes * 60;

        $vat = $request->vat;
        $price = $request->price;
        $totalprice = $this->calculatePriceInclVat($price, $seconds, $vat);

        $worky = new ClockingSystem();
        $worky->project_id = $projectid;
        $worky->organisation_id = $project->organisation_id;
        $worky->price = $price;
        $worky->job = $request->job;
        $worky->billing_service_id = $request->service;
        $worky->paused = 0;
        $worky->vat = $vat;
        $worky->total_time = $seconds;
        $worky->total_price = $totalprice;
        $worky->completed = 1;
        $worky->save();


        session()->flash('success', 'Successfully added an already completed worky.');

        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }


    public function complete($projectid, $id)
    {

        $worky = ClockingSystem::findOrFail($id);
        $project = Project::findOrFail($projectid);

        if ($worky->paused === 0) {


            $workyToComplete = ClockingSystemHours::where('worky_id', '=', $id)->where('end_time', '=', NULL)->get();

            if ($workyToComplete->isEmpty()) {
                session()->flash('error', 'Sorry! We are not able to pause the Worky. Please contact support@ytbn.com with the following error-code: 0204.');

                return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
                    'project' => $project
                ]);

            }

            $wTC = $workyToComplete->first();
            $wTC->end_time = Carbon::now()->timezone('Europe/Amsterdam');

            $starttime = Carbon::createFromTimeString($wTC->start_time);
            $endtime = Carbon::createFromTimeString($wTC->end_time);
            $starttime->format('H:i:s');
            $endtime->format('H:i:s');

            $totalTime = $starttime->diffInSeconds($endtime);
            $wTC->total_time = $totalTime;

            $wTC->update();

            if ($worky->total_time === 0) {
                $worky->total_time = $totalTime;
                $calculatedTotalTime = $totalTime;
            } else {
                $calculatedTotalTime = $this->calculateTotalTime($worky->total_time, $totalTime);
            }

        } else {
            $calculatedTotalTime = $worky->total_time;
        }

        //We need to calculate price now
        $totalPrice = $this->calculatePriceInclVat($worky->price, $calculatedTotalTime, $worky->vat);


        $worky->paused = 0;
        $worky->total_price = $totalPrice;
        $worky->completed = 1;
        $worky->update();

        session()->flash('success', 'Succesfully completed worky');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);

    }


    public function createWorkyForOrganisation($organisation,$workyJob)
    {

        $workyJobx = WorkyGeneral::findOrFail($workyJob->id);
        $workyGeneral = new WorkyGeneral();
        $workyGeneral->organisation_id = $organisation->id;
        $workyGeneral->price = $workyJobx->price;
    }


    public function calculatePrice($price, $seconds)
    {

        $minutes = $seconds / 60;
        $hours = $minutes / 60;
        $totalPrice = $price * $hours;

        return round($totalPrice, 2, PHP_ROUND_HALF_UP);
    }

    public function calculatePriceInclVat($price, $seconds, $vat)
    {
        $totalVat = (100 + $vat) / 100;
        $totalPriceInclVat = $this->calculatePrice($price, $seconds) * $totalVat;
        return round($totalPriceInclVat, 2, PHP_ROUND_HALF_UP);
    }

    public function calculateTotalTime($currentTime, $newTime)
    {

        $newTotalTime = $currentTime + $newTime;

        return $newTotalTime;
    }

}