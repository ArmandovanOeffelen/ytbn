<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Svg\Tag\Image;
use Illuminate\Filesystem\Filesystem;

class CompanyController extends Controller
{
    public function show($id)
    {
        $company = Company::findOrFail($id);


        return view('back.settings.forms.company.update',[
            'company' => $company
        ]);
    }

    public function showForLogo($id)
    {
        $company = Company::findOrFail($id);

        return view('back.settings.forms.company.upload_logo',[
            'company' => $company
        ]);
    }

    public function updatelogo(Request $request,$id)
    {


        $validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:jpeg,jpg,png|max:2048',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.settings.forms.company.upload_logo',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $directoryToClean = new Filesystem();
        $directoryToClean->cleanDirectory('storage/company/logo');

        $company = Company::findOrFail($id);

        $path = 'public/company/logo';
        $file = $request->file('logo');
        $logoname = 'logo.'.$file->getClientOriginalExtension();
        $file->storeAs($path,$logoname);

        $company->company_logo_path = 'storage/company/logo/'.$logoname;
        $company->update();



        session()->flash('success', 'Successfully updated the company logo.');

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent', [
            'company' => $company
        ]);

    }
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'website' => 'required|min:3|max:150',
            'phone' => 'required|min:6|max:30',
            'email' => 'required|min:3|max:30|email',
            'adress_street' => 'required|min:2|max:30',
            'adress_street_number' => 'required|min:1|max:30',
            'adress_postal' => 'required|min:2|max:30',
            'adress_city' => 'required|min:2|max:30',
            'adress_state' => 'required|min:2|max:30',
            'adress_country' => 'required|min:2|max:30',
            'coc_number' => 'required|min:2|max:30',
            'vat_number' => 'required|min:2|max:30',
            'bankaccount_number' => 'required|min:2|max:30',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.settings.forms.company.update',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $companyx = Company::findOrFail($id);
        $companyx->name = $request->name;
        $companyx->website = $request->website;
        $companyx->phone = $request->phone;
        $companyx->email = $request->email;
        $companyx->street = $request->adress_street;
        $companyx->street_number = $request->adress_street_number;
        $companyx->postalcode = $request->adress_postal;
        $companyx->city = $request->adress_city;
        $companyx->state = $request->adress_state;
        $companyx->country = $request->adress_state;
        $companyx->coc_number = $request->coc_number;
        $companyx->vat_number = $request->vat_number;
        $companyx->bankaccount_number = $request->bankaccount_number;
        $companyx->update();

        $company = Company::findOrFail($id);

        session()->flash('success', 'Successfully updated Company.');

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showContent', [
            'company' => $company
        ]);
    }

    public function showContent(Company $company)
    {

        return  response()->view('back.settings.company.template.index',[
            'company' => $company
        ]);
    }

    public function showContentWithCommand(Company $company)
    {

        return  response()->view('back.settings.company.template.index',[
            'company' => $company
        ]);
    }
}
