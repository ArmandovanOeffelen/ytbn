<?php

namespace App\Http\Controllers\Ajax\Template;

use App\ClockingSystem;
use App\ContactPerson;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ContactPersonController extends Controller
{
        public function show($id){
            $contactPerson = ContactPerson::findOrFail($id);


            return view('back.crm.forms.contactPerson.update',[
                'contactPerson' => $contactPerson,
                ]);
        }

        public function update(Request $request,$projectid,$id){
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:150',
                'phone' => 'required|min:6|max:30',
                'email' => 'required|min:3|max:30|email'

            ]);

            if ($validator->fails()) {
                return response()->json(array(
                    'html' => view('back.crm.forms.contactPerson.update',[
                    ])->withErrors($validator)->render()
                ), 400);
            }

            $object = ContactPerson::findOrFail($id);
            $object->name = $request->name;
            $object->phone = $request->phone;
            $object->email = $request->email;
            $object->update();

            $project = Project::findOrFail($projectid);

            session()->flash('success', 'Successfully updated contactperson.');

            return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
                'project' => $project
            ]);

        }
}
