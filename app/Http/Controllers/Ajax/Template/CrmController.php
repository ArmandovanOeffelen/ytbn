<?php

namespace App\Http\Controllers\Ajax\Template;

use App\ContactPerson;
use App\Http\Controllers\Controller;
use App\Model\Crm;
use App\Model\Organisation;
use App\Project;
use App\ProjectStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CrmController extends Controller
{
    public function showContent()
    {
        $organisations = Organisation::orderBy('id','desc')->limit(5)->get();
        $projectOverview = Project::orderBy('id','desc')->limit(5)->get();


        return view('back.crm.templates.index', [
            'organisations'  => $organisations,
            'projectOverview'  => $projectOverview,
        ]);
    }

    public function showOrganisation(Request $request)
    {
        return view('back.crm.forms.createCrmProject', [
            'organisations' => Organisation::all()
        ]);


    }


    public function delete(Request $request, $id)
    {

        $object = Organisation::findOrFail($id);
        $object->delete();

        // TODO: redirect to index template if that works
        $paginationOrganisations = Organisation::paginate(10);
        $paginationOrganisations->setPath('/back/crm/overview');
        return response()->json(array(
            'html' => view('back.crm.templates.index', [
                'paginationOrganisations' => $paginationOrganisations
            ])->render()
        ));
    }

}