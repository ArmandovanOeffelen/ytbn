<?php

namespace App\Http\Controllers\Ajax\Template;

use App\BillingService;
use App\BillingVat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GeneralSettingsController extends Controller
{
    public function show()
    {
        $allVat = BillingVat::all();
        $service = BillingService::all();
        return view('back.settings.general.template.index',[
            'allVat' => $allVat,
            'service' => $service
        ]);
    }


    //TODO:: vat cant be higher then 100%.
    public function createService(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'vat' => 'required|between:0,100|integer',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.settings.forms.general.service.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $service = new BillingService();
        $service->name = $request->name;
        $service->vat = $request->vat;
        $service->save();

        session()->flash('success', 'Succesfully added a billing service');
        return app()->call('App\Http\Controllers\Ajax\Template\GeneralSettingsController@show', [

        ]);
    }
}
