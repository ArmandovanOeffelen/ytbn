<?php

namespace App\Http\Controllers\Ajax\Template;


use App\Billing;
use App\ClockingSystem;
use App\Http\Controllers\Controller;
use App\Model\Organisation;
use App\Project;
use App\ProjectStatus;
use App\User;
use App\WorkyGeneralJob;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class OrganisationController extends Controller
{

    public function showContent($id)
    {
        $projectManagers = User::all();
        $projectStatus = ProjectStatus::all();

        return view('back.crm.forms.project.createForOrganisation',[
            'projectManagers' => $projectManagers,
            'projectStatus' => $projectStatus,
        ]);
    }


    public function show(Request $request, $id)
    {
        return view('back.crm.forms.organisation.update', [
            'organisation' => Organisation::findOrFail($id)
        ]);
    }


    public function showOrganisation($organisationx)
    {

        $organisation = Organisation::find($organisationx->id);

        $project = Project::where('organisation_id',$organisation->id)->get();

        $worky = ClockingSystem::where('organisation_id',$organisation->id)->where('completed','=',1)->get();
        $billing = Billing::find($organisation->id);

        $organisationWorkies = WorkyGeneralJob::all();

        $projectManagers = User::all();
        $projectStatus = ProjectStatus::all();

        return view('back.crm.organisation.templates.index',[
            'organisationWorkies' => $organisationWorkies,
            'projectManagers' => $projectManagers,
            'projectStatus' => $projectStatus,
            'organisation' => $organisation,
            'project' => $project,
            'worky' => $worky,
            'billing' => $billing
        ]);
    }


    public function showAllOrganisations(){


        $paginationOrganisations = Organisation::simplePaginate(10);
        $paginationOrganisations->setPath('back/crm/organisations/overview');
        return view('back.organisation.templates.index',[
            'paginationOrganisations'  => $paginationOrganisations
        ]);
    }
    public function createOrganisation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'website' => 'required|min:3|max:150',
            'phone' => 'required|min:6|max:30',
            'email' => 'required|min:3|max:30|email',
            'adress_street' => 'required|min:2|max:30',
            'adress_street_number' => 'required|min:1|max:30',
            'adress_postal' => 'required|min:2|max:30',
            'adress_city' => 'required|min:2|max:30',
            'adress_state' => 'required|min:2|max:30',
            'adress_country' => 'required|min:2|max:30',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.organisation.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $organisation = new Organisation();
        $organisation->name = $request->name;
        $organisation->website = $request->website;
        $organisation->phone = $request->phone;
        $organisation->email = $request->email;
        $organisation->street = $request->adress_street;
        $organisation->street_number = $request->adress_street_number;
        $organisation->postalcode = $request->adress_postal;
        $organisation->city = $request->adress_city;
        $organisation->state = $request->adress_state;
        $organisation->country = $request->adress_country;
        $organisation->save();


        session()->flash('success', 'Successfully added organisation.');

        return app()->call('App\Http\Controllers\Ajax\Template\OrganisationController@showAllOrganisations', [

        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'website' => 'required|min:3|max:150',
            'phone' => 'required|min:6|max:30',
            'email' => 'required|min:3|max:30|email',
            'adress_street' => 'required|min:2|max:30',
            'adress_street_number' => 'required|min:1|max:30',
            'adress_postal' => 'required|min:2|max:30',
            'adress_city' => 'required|min:2|max:30',
            'adress_state' => 'required|min:2|max:30',
            'adress_country' => 'required|min:2|max:30',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.organisation.create',[
                ])->withErrors($validator)->render()
            ), 400);
        }


        $organisation = new Organisation();
        $organisation->name = $request->name;
        $organisation->website = $request->website;
        $organisation->phone = $request->phone;
        $organisation->email = $request->email;
        $organisation->street = $request->adress_street;
        $organisation->street_number = $request->adress_street_number;
        $organisation->postalcode = $request->adress_postal;
        $organisation->city = $request->adress_city;
        $organisation->state = $request->adress_state;
        $organisation->country = $request->adress_country;
        $organisation->save();

        session()->flash('success', 'Successfully added organisation.');

        return app()->call('App\Http\Controllers\Ajax\Template\CrmController@showContent', [

        ]);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'website' => 'required|min:3|max:150',
            'phone' => 'required|min:6|max:30',
            'email' => 'required|min:3|max:30|email',
            'adress_street' => 'required|min:2|max:30',
            'adress_street_number' => 'required|min:1|max:30',
            'adress_postal' => 'required|min:2|max:30',
            'adress_city' => 'required|min:2|max:30',
            'adress_state' => 'required|min:2|max:30',
            'adress_country' => 'required|min:2|max:30',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.organisation.update',[
                    'organisation' => Organisation::findOrFail($id)
                ])->withErrors($validator)->render()
            ), 400);
        }


        $organisation = Organisation::findOrFail($id);
        $organisation->name = $request->name;
        $organisation->website = $request->website;
        $organisation->phone = $request->phone;
        $organisation->email = $request->email;
        $organisation->street = $request->adress_street;
        $organisation->street_number = $request->adress_street_number;
        $organisation->postalcode = $request->adress_postal;
        $organisation->city = $request->adress_city;
        $organisation->state = $request->adress_state;
        $organisation->country = $request->adress_country;
        $organisation->update();

        $organisation = Organisation::find($id);

        session()->flash('success', 'Successfully updated organisation.');

        return app()->call('App\Http\Controllers\Ajax\Template\OrganisationController@showOrganisation', [
            'organisationx'  => $organisation
        ]);

    }

    public function updateForAllOrganisations(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'website' => 'required|min:3|max:150',
            'phone' => 'required|min:6|max:30',
            'email' => 'required|min:3|max:30|email',
            'adress_street' => 'required|min:2|max:30',
            'adress_street_number' => 'required|min:1|max:30',
            'adress_postal' => 'required|min:2|max:30',
            'adress_city' => 'required|min:2|max:30',
            'adress_state' => 'required|min:2|max:30',
            'adress_country' => 'required|min:2|max:30',

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.organisation.update',[
                    'organisation' => Organisation::findOrFail($id)
                ])->withErrors($validator)->render()
            ), 400);
        }


        $organisation = Organisation::findOrFail($id);
        $organisation->name = $request->name;
        $organisation->website = $request->website;
        $organisation->phone = $request->phone;
        $organisation->email = $request->email;
        $organisation->street = $request->adress_street;
        $organisation->street_number = $request->adress_street_number;
        $organisation->postalcode = $request->adress_postal;
        $organisation->city = $request->adress_city;
        $organisation->state = $request->adress_state;
        $organisation->country = $request->adress_country;
        $organisation->update();

        $organisation = Organisation::find($id);

        session()->flash('success', 'Successfully updated organisation.');

        return app()->call('App\Http\Controllers\Ajax\Template\OrganisationController@showAllOrganisations', [
            'organisationx'  => $organisation
        ]);

    }

    public function delete($id)
    {

        $organisation = Organisation::findOrFail($id);
        $organisation->delete();

        session()->flash('success', 'Successfully deleted organisation.');

        return app()->call('App\Http\Controllers\Ajax\Template\OrganisationController@showAllOrganisations', [

        ]);
    }

}