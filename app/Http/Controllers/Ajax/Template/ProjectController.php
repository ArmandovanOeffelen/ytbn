<?php

namespace App\Http\Controllers\Ajax\Template;


use App\Billing;
use App\BillingInvoice;
use App\BillingQuote;
use App\ClockingSystem;
use App\ContactPerson;
use App\Model\Organisation;
use App\Project;
use App\ProjectStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ProjectController
{

    public function showContent(Project $project)
    {


        $noncompletedWorky = ClockingSystem::where('project_id','=',$project->id)->where('completed','=',0)->latest()->limit(5)->get();


        $completedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '1')
            ->latest()->limit(5)->get();

        $billing = Billing::all()->where('project_id','=',$project->id)->first();

        $billingInvoiceNotGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',0)
            ->latest()->limit(5)->get();


        $billingInvoiceGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',1)
            ->latest()->limit(5)->get();

        $billingQuoteNotGenerated = BillingQuote::
        where('billing_id','=',$billing->id)->
            where('has_pdf','=', 0)->
            latest()->
            limit(5)->
            get();
        $billingQuoteGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 1)
            ->latest()->limit(5)->get();


        return view('back.crm.project.templates.index', [
            'billing' => $billing,
            'completedWorky' =>$completedWorky,
            'noncompletedWorky' => $noncompletedWorky,
            'billingInvoiceNotGenerated' => $billingInvoiceNotGenerated,
            'billingInvoiceGenerated' => $billingInvoiceGenerated,
            'billingQuoteNotGenerated' => $billingQuoteNotGenerated,
            'billingQuoteGenerated' => $billingQuoteGenerated,
            'project' => $project,
        ]);
    }

    public function createProject(Request $request,$id)
    {

        $validator = Validator::make($request->all(), [

            //project validation
            'name' => 'required|min:3|max:150',
            'project_manager' => 'required',
            'project_summary' => 'max:255',
            'start_date' => 'date|date_format:Y-m-d',
            'end_date' => 'date|date_format:Y-m-d|after:start_date',
            'project_summary' => 'required',

            //contact persion validation
            'contactperson_name' => 'required|min:3|max:30',
            'contactperson_email' => 'required|email',
            'contactperson_phone' => 'required|min:3|max:30',

            //project status validation
            'project_status' => 'required'
        ]);

        $projectManagers = User::all();
        $organisation = Organisation::all();
        $projectStatus = ProjectStatus::all();

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.project.create',[
                    'projectManagers' => $projectManagers,
                    'organisation' => $organisation,
                    'projectStatus' => $projectStatus
                ])->withErrors($validator)->render()
            ), 400);
        }

        //create contactperson before project because we need the id
        $contactPerson = new ContactPerson();
        $contactPerson->name = $request->contactperson_name;
        $contactPerson->email = $request->contactperson_email;
        $contactPerson->phone = $request->contactperson_phone;
        $contactPerson->save();


        //Create project
        $project = new Project();
        $project->name = $request->name;
        $project->user_id = $request->project_manager;
        $project->summary = $request->project_summary;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->organisation_id = $id;
        $project->contact_person_id = $contactPerson->id;
        $project->project_status_id = $request->project_status;
        $project->save();

        //Creating billing object voor project.
        $billing = new BillingController();
        $billing->createBilling($project->id,$id);

       $organisationx = Organisation::findOrFail($id);

        session()->flash('success', 'Succesfully added project for organisation');
        return app()->call('App\Http\Controllers\Ajax\Template\OrganisationController@showOrganisation', [
            'organisationx' => $organisationx
        ]);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [

            //project validation
            'name' => 'required|min:3|max:150',
            'project_manager' => 'required',
            'organisation' => 'required',
            'project_summary' => 'max:255',
            'start_date' => 'date|date_format:Y-m-d',
            'end_date' => 'date|date_format:Y-m-d|after:start_date',
            'project_summary' => 'required',

            //contact persion validation
            'contactperson_name' => 'required|min:3|max:30',
            'contactperson_email' => 'required|email',
            'contactperson_phone' => 'required|min:3|max:30',

            //project status validation
            'project_status' => 'required'
        ]);

        $projectManagers = User::all();
        $organisation = Organisation::all();
        $projectStatus = ProjectStatus::all();

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.project.create',[
                    'projectManagers' => $projectManagers,
                    'organisation' => $organisation,
                    'projectStatus' => $projectStatus
                ])->withErrors($validator)->render()
            ), 400);
        }

        //create contactperson before project because we need the id
        $contactPerson = new ContactPerson();
        $contactPerson->name = $request->contactperson_name;
        $contactPerson->email = $request->contactperson_email;
        $contactPerson->phone = $request->contactperson_phone;
        $contactPerson->save();


        //Create project
        $project = new Project();
        $project->name = $request->name;
        $project->user_id = $request->project_manager;
        $project->summary = $request->project_summary;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->organisation_id = $request->organisation;
        $project->contact_person_id = $contactPerson->id;
        $project->project_status_id = $request->project_status;
        $project->save();

        //Creating billing object voor project.
        $billing = new BillingController();
        $billing->createBilling($project->id,$project->organisation_id);

        $organisations = Organisation::orderBy('id','desc')->limit(5)->get();
        $projectOverview = Project::orderBy('id','desc')->limit(5)->get();

        session()->flash('success', 'Successfully added project.');

        return view('back.crm.templates.index', [
            'organisations'  => $organisations,
            'projectOverview'  => $projectOverview,
        ])->with('status', 'Succesfully added!!');
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150',
            'project_manager' => 'required',
            'organisation' => 'required',
            'project_summary' => 'max:255',
            'start_date' => 'date|date_format:Y-m-d',
            'end_date' => 'date|date_format:Y-m-d|after:start_date',
            //project status validation
            'project_status' => 'required'

        ]);

        $projectManagers = User::all();
        $organisation = Organisation::all();
        $projectStatus = ProjectStatus::all();

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.crm.forms.project.update', [

                    'project' => Project::findOrFail($id),
                    'projectManagers' => $projectManagers,
                    'organisation' => $organisation,
                    'projectStatus' => $projectStatus
                ])->withErrors($validator)->render()
            ), 400);
        }

        //Update project
        $object = Project::findOrFail($id);
        $object->name = $request->name;
        $object->user_id = $request->project_manager;
        $object->summary = $request->project_summary;
        $object->start_date = $request->start_date;
        $object->end_date = $request->end_date;
        $object->organisation_id = $request->organisation;
        $object->contact_person_id = $request->project_manager;
        $object->project_status_id = $request->project_status;
        $object->update();


        $project = Project::findOrFail($id);

        session()->flash('success', 'Succesfully updated Project');
        return app()->call('App\Http\Controllers\Ajax\Template\ProjectController@showContent', [
            'project' => $project
        ]);
    }
    public function show($id)
    {
        $project = Project::find($id);

        $projectManagers = User::all();
        $organisation = Organisation::all();
        $projectStatus = ProjectStatus::all();
        $completedWorky = ClockingSystem::all()->where('project_id','=',$id)->where('completed','=','1');
        $nonCompletedWorkys = ClockingSystem::all()->where('project_id','=',$project->id)->where('completed','=',0);


        return view('back.crm.forms.project.update', [
            'project' => $project,
            'projectManagers' => $projectManagers,
            'organisation' => $organisation,
            'completedWorky' =>$completedWorky,
            'nonCompletedWorkys' =>$nonCompletedWorkys,
            'projectStatus' => $projectStatus
        ]);
    }



}

