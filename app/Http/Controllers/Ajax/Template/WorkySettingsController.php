<?php

namespace App\Http\Controllers\Ajax\Template;

use App\BillingService;
use App\Http\Controllers\Controller;
use App\WorkyGeneral;
use App\WorkyGeneralJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkySettingsController extends Controller
{

    public function show(){

        $workyJob = WorkyGeneralJob::all();

        return view('back.settings.worky.templates.index',[
            'workyJob' => $workyJob
        ]);
    }


    public function showForm()
    {

        $billingService = BillingService::all();
        return view('back.settings.forms.worky.create_general_worky_organisation',[
            'billingService' => $billingService
        ]);
    }

    public function showFormForUpdate($workyGeneralJob)
    {

        $worky = WorkyGeneralJob::findOrFail($workyGeneralJob);
        return view('back.settings.forms.worky.update_general_worky_organisation',[
            'workyGeneralJob' => $worky
        ]);
    }

    public function update(Request $request, $workyGeneralJob)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150'

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.settings.forms.worky.update_general_worky_organisation',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        $worky = WorkyGeneralJob::findOrFail($workyGeneralJob);
        $worky->name = $request->name;
        $worky->update();

        $workyJob = WorkyGeneralJob::all();
        session()->flash('success', 'You have editted the existing worky!');
        return app()->call('App\Http\Controllers\Ajax\Template\WorkySettingsController@show', [
            'workyJob' => $workyJob
        ]);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:150'

        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('back.settings.forms.worky.create_general_worky_organisation',[
                ])->withErrors($validator)->render()
            ), 400);
        }


        $worky = new WorkyGeneralJob();

        if($worky->checkMaximumWorkies() == true){

            $workyJob = WorkyGeneralJob::all();
            session()->flash('warning', 'The worky you have been trying to add has not been added, you have already 5 existing workies. Therefor you have reached the limit.');
            return app()->call('App\Http\Controllers\Ajax\Template\WorkySettingsController@show', [
                'workyJob' => $workyJob
            ]);
        }
        $worky->name = $request->name;
        $worky->save();




        $workyJob = WorkyGeneralJob::all();
        session()->flash('success', 'You have succesfully added a general worky!');
        return app()->call('App\Http\Controllers\Ajax\Template\WorkySettingsController@show', [
            'workyJob' => $workyJob
        ]);

    }
}
