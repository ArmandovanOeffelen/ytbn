<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingInvoice;
use App\BillingQuote;
use App\BillingQuoteStorage;
use App\BillingService;
use App\BillingStorage;
use App\BillingVat;
use App\ClockingSystem;
use App\Company;
use App\Mail\send_invoice;
use App\Mail\send_quote;
use App\Project;
use App\ProjectStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BillingController extends Controller
{
    public function showAllQuotiesForProject($projectId, $billingId)
    {
        $project = Project::findOrFail($projectId);
        $billing = Billing::findOrFail($billingId);
        $quoties = BillingQuote::where('billing_id','=',$billingId)->paginate(10);
        $services = BillingService::all();

        return view('back.crm.quoty.quoty.index',[
            'project' => $project,
            'billing' => $billing,
            'services' => $services,
            'quoties' => $quoties
        ]);
    }

    public function index(){
        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.quoty.templates.index',[
            'allVat' => $allVat,
            'services' => $services
        ]);
    }

    public function show(){
        $allVat = BillingVat::all();
        $services = BillingService::all();

        return view('back.quoty.templates.index',[
            'allVat' => $allVat,
            'services' => $services
        ]);
    }

    public function findPDF($billingTypeId,$pdfType)
    {

        switch ($pdfType) {
            case "invoice":
                $storagePath = BillingStorage::findOrFail($billingTypeId);

                $filename = $storagePath->name . '.pdf'; //file name with extension add..
                $filepath = storage_path() . '/pdf/billing/invoice/' . $filename;

                $pdf = [
                    'filepath' => $filepath,
                    'filename' => $filename
                ];
                return $pdf;
            break;

            case "quote":
                $storagePath = BillingQuoteStorage::findOrFail($billingTypeId);

                $filename = $storagePath->name . '.pdf'; //file name with extension add..
                $filepath = storage_path() . '/pdf/billing/quote/' . $filename;

                $pdf = [
                    'filepath' => $filepath,
                    'filename' => $filename
                ];
                return $pdf;
                break;
        }
    }

    public function downloadQuotePDF($billingTypeId)
    {
        $pdfType = 'quote';
        return response()->download($this->findPDF($billingTypeId,$pdfType)['filepath']);
    }

    public function downloadPDF($billingTypeId)
    {
        $pdfType = 'invoice';
        return response()->download($this->findPDF($billingTypeId,$pdfType)['filepath']);
    }

    public function makeInvoiceMail($projectId,$billingId,$billingTypeId)
    {
        $pdfType = "invoice";
        $pdf = $this->findPDF($billingTypeId,$pdfType);

        $project = Project::findOrfail($projectId);
        $contactPerson = $project->contactPerson;
        $comp = Company::all();
        $company = $comp->first();
        return Mail::to($contactPerson->email)->send(new send_invoice($pdf,$contactPerson,$company));
    }

    public function makeQuoteMail($projectId,$billingId,$billingTypeId)
    {
        $pdfType = "quote";
        $pdf = $this->findPDF($billingTypeId,$pdfType);

        $project = Project::findOrFail($projectId);
        $contactPerson = $project->contactPerson;
        $comp = Company::all();
        $company = $comp->first();

        return Mail::to($contactPerson->email)->send(new send_quote($pdf,$contactPerson,$company));
    }

    public function sendQuote($projectId,$billingId,$billingTypeId){
        $this->makeQuoteMail($projectId,$billingId,$billingTypeId);

        $billingQuote = BillingQuote::findOrFail($billingTypeId);
        $billingQuote->sent = 1;
        $billingQuote->update();

        $project = Project::findOrFail($projectId);

        $projectStatus = new ProjectStatus();
        $projectStatus->updateStatusToQuotySend($projectId);

        $uncompletedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '0')
            ->simplePaginate(5);
        $completedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '1')
            ->simplePaginate(5);

        $allVat = BillingVat::all();
        $services = BillingService::all();

        $billing = Billing::all()->where('project_id','=',$project->id)->first();

        $billingInvoiceNotGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',0)
            ->simplePaginate(5);


        $billingInvoiceGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',1)
            ->simplePaginate(5);


        $billingQuoteNotGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 0)
            ->simplePaginate(5);


        $billingQuoteGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 1)
            ->simplePaginate(5);

        return view('back.crm.project.index',[
            'billing' => $billing,
            'billingInvoiceNotGenerated' => $billingInvoiceNotGenerated,
            'billingInvoiceGenerated' => $billingInvoiceGenerated,
            'billingQuoteNotGenerated' => $billingQuoteNotGenerated,
            'billingQuoteGenerated' => $billingQuoteGenerated,
            'project' => $project,
            'hoursWorked' => $uncompletedWorky,
            'completedWorky' => $completedWorky,
            'allVat' => $allVat,
            'services' => $services
        ]);
    }


    public function sendInvoice($projectId,$billingId,$billingTypeId)
    {

        $this->makeInvoiceMail($projectId,$billingId,$billingTypeId);

        $billingInvoice = BillingInvoice::findOrFail($billingTypeId);
        $billingInvoice->sent = 1;
        $billingInvoice->update();

        $project = Project::findOrFail($projectId);

        //Update project status
        $projectStatus = new ProjectStatus();
        $projectStatus->updateStatusToInvoicySend($projectId);


        $uncompletedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '0')
            ->simplePaginate(5);
        $completedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '1')
            ->simplePaginate(5);

        $allVat = BillingVat::all();
        $services = BillingService::all();

        $billing = Billing::all()->where('project_id','=',$project->id)->first();

        $billingInvoiceNotGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',0)
            ->simplePaginate(5);


        $billingInvoiceGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',1)
            ->simplePaginate(5);


        $billingQuoteNotGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 0)
            ->simplePaginate(5);


        $billingQuoteGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 1)
            ->simplePaginate(5);

        return view('back.crm.project.index',[
            'billing' => $billing,
            'billingInvoiceNotGenerated' => $billingInvoiceNotGenerated,
            'billingInvoiceGenerated' => $billingInvoiceGenerated,
            'billingQuoteNotGenerated' => $billingQuoteNotGenerated,
            'billingQuoteGenerated' => $billingQuoteGenerated,
            'project' => $project,
            'hoursWorked' => $uncompletedWorky,
            'completedWorky' => $completedWorky,
            'allVat' => $allVat,
            'services' => $services
        ]);
    }

}
