<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function show()
    {
        $comp = Company::all();
        $company = $comp->first();


        return view('back.settings.company.index',[
            'company' => $company
        ]);
    }
}
