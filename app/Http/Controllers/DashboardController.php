<?php
/**
 * Created by PhpStorm.
 * User: Armando
 * Date: 12/30/2018
 * Time: 8:13 PM
 */

namespace App\Http\Controllers;


class DashboardController
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.dashboard.index');
    }
}