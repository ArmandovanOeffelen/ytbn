<?php

namespace App\Http\Controllers;

use App\BillingService;
use App\BillingVat;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    public function show()
    {
        $allVat = BillingVat::all();
        $service = BillingService::all();
        return view('back.settings.general.index',[
            'allVat' => $allVat,
            'service' => $service
        ]);
    }
}
