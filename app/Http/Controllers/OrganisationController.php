<?php

namespace App\Http\Controllers;

use App\Billing;
use App\ClockingSystem;
use App\Project;
use App\ProjectStatus;
use app\User;
use  App\Model\Organisation;
use App\WorkyGeneralJob;
use Illuminate\Http\Request;

class OrganisationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $paginationOrganisations = Organisation::simplePaginate(10);

        return view('back.organisation.index',[
            'paginationOrganisations'  => $paginationOrganisations
        ]);
    }



    public function showOrganisation($id)
    {

        $organisation = Organisation::find($id);

        $project = Project::where('organisation_id',$organisation->id)->get();

        $worky = ClockingSystem::where('organisation_id',$organisation->id)->where('completed','=',1)->get();
        $billing = Billing::find($organisation->id);

        $organisationWorkies = WorkyGeneralJob::all();

        $projectManagers = User::all();
        $projectStatus = ProjectStatus::all();

        return view('back.crm.organisation.index',[
            'organisationWorkies' => $organisationWorkies,
            'projectManagers' => $projectManagers,
            'projectStatus' => $projectStatus,
            'organisation' => $organisation,
            'project' => $project,
            'worky' => $worky,
            'billing' => $billing
        ]);
    }
}