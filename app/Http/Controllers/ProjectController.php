<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingInvoice;
use App\BillingQuote;
use App\BillingService;
use App\BillingVat;
use App\ClockingSystem;
use App\Project;

class ProjectController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index($id){

        $project = Project::find($id);

        $allVat = BillingVat::all();
        $services = BillingService::all();

        $noncompletedWorky = ClockingSystem::where('project_id','=',$project->id)->where('completed','=',0)->latest()->limit(5)->get();


        $completedWorky = ClockingSystem::
        where('project_id', '=', $project->id)
            ->where('completed', '=', '1')
            ->latest()->limit(5)->get();

        $billing = Billing::all()->where('project_id','=',$project->id)->first();

        $billingInvoiceNotGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',0)
            ->latest()->limit(5)->get();


        $billingInvoiceGenerated = BillingInvoice::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=',1)
            ->latest()->limit(5)->get();

        $billingQuoteNotGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 0)
            ->latest()->limit(5)->get();
        $billingQuoteGenerated = BillingQuote::
        where('billing_id','=',$billing->id)
            ->where('has_pdf','=', 1)
            ->latest()->limit(5)->get();

        return view('back.crm.project.index',[
            'billing' => $billing,
            'billingInvoiceNotGenerated' => $billingInvoiceNotGenerated,
            'billingInvoiceGenerated' => $billingInvoiceGenerated,
            'billingQuoteNotGenerated' => $billingQuoteNotGenerated,
            'billingQuoteGenerated' => $billingQuoteGenerated,
            'project' => $project,
            'noncompletedWorky' => $noncompletedWorky,
            'completedWorky' => $completedWorky,
            'allVat' => $allVat,
            'services' => $services
        ]);
    }
}
