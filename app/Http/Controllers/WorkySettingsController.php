<?php

namespace App\Http\Controllers;

use App\WorkyGeneralJob;
use Illuminate\Http\Request;

class WorkySettingsController extends Controller
{

    public function show(){


        $workyJob = WorkyGeneralJob::all();

        return view('back.settings.worky.index',[
            'workyJob' => $workyJob
        ]);
    }
}
