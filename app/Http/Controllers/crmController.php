<?php

namespace App\Http\Controllers;

use App\Billing;
use App\ClockingSystem;
use App\Project;
use App\ProjectStatus;
use app\User;
use  App\Model\Organisation;
use Illuminate\Http\Request;

class CrmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $organisations = Organisation::orderBy('id','desc')->limit(5)->get();
        $projectOverview = Project::orderBy('id','desc')->limit(5)->get();

        //Getting collections for quick create project.
        //These are needed in order to use selectize in the createTemplate.
        $organisation = Organisation::all();
        $projectStatus = ProjectStatus::all();
        $projectManagers = User::all();

        return view('back.crm.index',[
            'organisations'  => $organisations,
            'projectOverview'  => $projectOverview,
            'projectStatus'  => $projectStatus,
            'organisation' => $organisation,
            'projectManagers' => $projectManagers
        ]);
    }




}
