<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
//            'description' => 'required|min:3|max:3',
//            'price' => 'required|min:3|max:20',
//            'time' => 'required|integer',
//            'vat' => 'required|integer',
//            'discount' => 'required|integer',
        ];

        foreach($this->request->get('tableArray') as $key => $val){
            $rules['description'.$key] = "required|max:255";
            $rules['price'.$key] = "required|integer";
            $rules['hour'.$key] = "required|integer";
            $rules['vat'.$key] = "required|integer|max:2";
            $rules['discount'.$key] = "required|integer";

        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('items') as $key => $val)
        {
//            $messages['items.'.$key.'.max'] = 'The field labeled "Book Title '.$key.'" must be less than :max characters.';
        }
        return $messages;
    }
}
