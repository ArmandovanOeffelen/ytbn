<?php

namespace App\Mail;

use App\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class send_invoice extends Mailable
{
    use Queueable, SerializesModels;

    public $pdf;
    public $contactPerson;
    public $company;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf,$contactPerson,$company)
    {
        $this->pdf = $pdf;
        $this->contactPerson = $contactPerson;
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from($this->company->email,$this->company->name)
            ->subject('The invoice.')
            ->with([
            ])
            ->view('mails.billing.invoice.invoice',[

            ])->attach($this->pdf['filepath'],[
                'as' =>$this->pdf['filename'],
                'mime' => 'application/pdf'
            ]);

    }
}
