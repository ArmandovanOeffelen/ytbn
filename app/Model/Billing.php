<?php

namespace App;

use App\Model\Organisation;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billing';

    protected $fillable = ['organisation_id','project_id','user_id'];

    protected $hidden = ['updated_at'];

    public function organisation()
    {
        return $this->belongsTo(Organisation::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function billingInvoice()
    {
        return $this->hasMany(BillingInvoice::class);
    }

    public function billingQuote()
    {
        return $this->hasMany(BillingQuote::class);
    }
}
