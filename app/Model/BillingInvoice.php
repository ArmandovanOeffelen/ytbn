<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingInvoice extends Model
{
    protected $table = 'billing_invoice';

    protected $fillable = ['billing_id'];

    protected $date = 'updated_at';

    public function billing()
    {
        return $this->belongsTo(Billing::class);
    }

    public function billingInvoiceRow()
    {
        return $this->hasMany(BillingInvoiceRow::class);
    }

    public function billingStorage()
    {
        return $this->hasOne(BillingStorage::class);
    }
}
