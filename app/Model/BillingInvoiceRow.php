<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingInvoiceRow extends Model
{
    protected $table = 'billing_invoice_row';

    protected $primaryKey = 'id';

    protected $fillable = ['billing_invoice_id','billing_service_id','description','price','hour','vat','discount','total_price_excl','vat_diff','total_price_incl'];


    public function billingInvoice()
    {
        return $this->belongsTo(BillingInvoice::class);
    }
}
