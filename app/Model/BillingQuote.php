<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingQuote extends Model
{
    protected $table = 'billing_quote';
    protected $dates = ['created_at'];

    public function billing()
    {
        return $this->belongsTo(Billing::class);
    }

    public function billingQuoteRow()
    {
        return $this->hasMany(BillingQuoteRow::class);
    }

    public function billingQuoteStorage()
    {
        return $this->hasOne(BillingQuoteStorage::class);
    }
}
