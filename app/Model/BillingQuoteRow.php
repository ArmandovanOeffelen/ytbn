<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingQuoteRow extends Model
{
    protected $table = 'billing_quote_row';

    protected $fillable = ['billing_quote_id','billing_service_id','description','price','hour','vat','discount','total_price_excl','vat_diff','total_price_incl'];

    public function billingQuote()
    {
        return $this->belongsTo(BillingQuote::class);
    }
}
