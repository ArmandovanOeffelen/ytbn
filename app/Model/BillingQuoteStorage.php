<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingQuoteStorage extends Model
{
    protected $table = 'billing_quote_pdf_storage';

    public function billingQuote()
    {
        return $this->belongsTo(BillingQuote::class);
    }
}
