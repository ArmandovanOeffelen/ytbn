<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingService extends Model
{
    protected $table = 'billing_service';


    public function clockingSystem()
    {
        return $this->hasMany(ClockingSystem::class);
    }
}
