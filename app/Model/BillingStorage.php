<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingStorage extends Model
{
    protected $table = 'billing_invoice_pdf_storage';

    public function billingInvoice()
    {
        return $this->belongsTo(BillingInvoice::class);
    }
}
