<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingVat extends Model
{
    protected $table = 'billing_vat';
}
