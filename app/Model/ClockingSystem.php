<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClockingSystem extends Model
{
    protected $table = 'worky';

    protected $hidden = ['updated_at'];


    public function clockingSystemHours() {
        return $this->belongsTo( ClockingSystemHours::class);
    }

    public function billingService(){
        return $this->belongsTo(BillingService::class);
    }
}
