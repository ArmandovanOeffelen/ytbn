<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClockingSystemHours extends Model
{    protected $table = 'worky_hours';
    protected $hidden = ['updated_at'];
    public $dates = ['start_time','end_time'];
    public $timestamps = true;



    public function clockingSystem() {
        return $this->hasMany( ClockingSystem::class);
    }

}
