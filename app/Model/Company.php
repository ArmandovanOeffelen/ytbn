<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';


    public function loadCompanyLogo(){

        $company = Company::all();
        $logo = $company->first()->company_logo_path;

        return $logo;
    }
}
