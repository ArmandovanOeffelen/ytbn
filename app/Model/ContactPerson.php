<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactPerson extends Model
{
    protected $table = 'contact_person';

    protected $hidden = ['updated_at'];


    public function projects()
    {
        return $this->hasMany(Project::class);
    }

}
