<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Crm extends Model{

    protected $table = 'crm';

    protected $hidden = ['updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
//
//    public function comment()
//    {
//        return $this->hasMany(Comment::class,'id');
//    }

}