<?php

namespace App\Model;

use App\Billing;
use App\Project;
use Illuminate\Database\Eloquent\Model;

class Organisation extends Model{

    protected $table = 'organisation';

    protected $hidden = ['updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function billing()
    {
        return $this->hasMany(Billing::class);
    }
}