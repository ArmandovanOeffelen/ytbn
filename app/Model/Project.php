<?php

namespace App;

use App\Model\Organisation;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';

    protected $hidden = ['updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    public function organisation()
    {
        return $this->belongsTo(Organisation::class);
    }

    public function projectStatus()
    {
        return $this->belongsTo(ProjectStatus::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function contactPerson() {
        return $this->belongsTo( ContactPerson::class);
    }

    public function billing()
    {
        return $this->hasMany(Billing::class);
    }


}
