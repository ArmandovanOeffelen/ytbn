<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
    protected $table = 'project_status';

    protected $hidden = ['updated_at'];

    
    public function projects()
    {
        return $this->hasMany(Project::class);
    }


    public function updateStatusToInitiated($projectId)
    {
        $project = Project::findOrFail($projectId);
        $project->project_status_id = 4;
        $project->update();

        return ;
    }

    public function updateStatusToInvoicySend($projectId)
    {
        $project = Project::findOrFail($projectId);

        if($project->project_status_id != 2){
            $project->project_status_id = 2;
            $project->update();
        }

        return ;
    }

    public function updateStatusToQuotySend($projectId)
    {
        $project = Project::findOrFail($projectId);

        if($project->project_status_id != 6){
            $project->project_status_id = 6;
            $project->update();
        }
        return;
    }

}
