<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkyGeneral extends Model
{
    protected $table = 'worky_general';


    public function WorkyGeneralJob()
    {
        return $this->hasOne(WorkyGeneral::class,'worky_general_job_id');
    }
}
