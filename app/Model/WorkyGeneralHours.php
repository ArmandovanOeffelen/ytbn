<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkyGeneralHours extends Model
{
    protected $table = 'worky_general_hours';
}
