<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkyGeneralJob extends Model
{
    protected $table = 'worky_general_job';


    public function checkMaximumWorkies()
    {
        $totalAmount = count(WorkyGeneralJob::all());


        if($totalAmount >= 5){

            return true;
        } else {
            return false;
        }
    }

    public function WorkyGeneral()
    {
        return $this->belongsTo(WorkyGeneralJob::class);
    }


}
