<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkySettings extends Model
{
    protected $table = 'worky_settings';
}
