<?php

namespace App\Providers;

use App\Company;
use Illuminate\Support\ServiceProvider;

class fetchCompanyDataServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*',function($view){
            $companyAll = Company::all();
            $company = $companyAll->first();

            return $view->with('company',$company);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
