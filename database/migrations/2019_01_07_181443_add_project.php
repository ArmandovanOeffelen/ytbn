<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->integer('user_id')->references('id')->on('users');
            $table->text('summary');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('organisation_id')->references('id')->on('organisation');
            $table->integer('contact_person_id')->references('id')->on('contactperson');
            $table->integer('project_status_id')->references('id')->on('project_status');
            $table->tinyInteger('finished')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
