<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClockingSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worky', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('project_id')->references('id')->on('project');
            $table->integer('organisation_id')->references('id')->on('organisation');
            $table->integer('billing_service_id')->references('id')->on('billing_service');
            $table->decimal('price', 10,2);
            $table->text('job');
            $table->tinyInteger('paused')->default(0);
            $table->integer('vat');
            $table->integer('total_time')->default(0);
            $table->decimal('total_price',10,2)->default(0);
            $table->tinyInteger('completed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worky');
    }
}
