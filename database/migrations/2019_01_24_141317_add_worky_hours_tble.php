<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkyHoursTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worky_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('start_time')->nullable(true);
            $table->timestamp('end_time')->nullable(true);
            $table->integer('total_time')->nullable(true);
            $table->integer('worky_id')->references('id')->on('worky');
            $table->integer('employee_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worky_hours');
    }
}
