<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInvoiceRows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_invoice_row', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('billing_invoice_id')->references('id')->on('billing_invoice');
            $table->tinyInteger('billing_service_id')->references('id')->on('billing_service');
            $table->string('description');
            $table->decimal('price',10,2)->default(0);
            $table->integer('hour')->nullable(true);
            $table->tinyInteger('vat')->references('id')->on('vat');
            $table->integer('discount')->nullable(true);
            $table->decimal('total_price_excl',10,2);
            $table->decimal('vat_diff',10,2);
            $table->decimal('total_price_incl',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_invoice_row');
    }
}
