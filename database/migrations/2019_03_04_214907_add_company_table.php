<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('website');
            $table->string('phone');
            $table->string('street');
            $table->string('street_number');
            $table->string('postalcode');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('coc_number');
            $table->string('vat_number');
            $table->string('bankaccount_number');
            $table->string('company_logo_path')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
