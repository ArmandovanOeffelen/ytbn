<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_role', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role_name');
            $table->tinyInteger('organisation');
            $table->tinyInteger('invoicy_send');
            $table->tinyInteger('quoty_send');
            $table->tinyInteger('delete_organisation');
            $table->tinyInteger('delete_project');
            $table->tinyInteger('project_status');
            $table->tinyInteger('employee');
            $table->tinyInteger('delete_employee');
            $table->tinyInteger('quoty');
            $table->tinyInteger('company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_role');
    }
}
