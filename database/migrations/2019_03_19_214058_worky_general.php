<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkyGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worky_general', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('organisation_id')->references('id')->on('organisation');
            $table->decimal('price', 10,2);
            $table->integer('work_general_job_id')->references('id')->on('worky_general_job');
            $table->tinyInteger('paused')->default(0);
            $table->integer('vat');
            $table->integer('total_time')->default(0);
            $table->decimal('total_price',10,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worky_general');
    }
}
