<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(add_organisation_mockup_data::class);
        $this->call(add_contact_person::class);
        $this->call(add_project_mockup_data::class);
        $this->call(add_project_status_mockup_data::class);
        $this->call(add_vat_data::class);
        $this->call(add_billing_data_2_9::class);
        $this->call(add_company_data::class);
        $this->call(add_employee_role_data::class);
        $this->call(add_service_data::class);
    }
}
