<?php

use Illuminate\Database\Seeder;

class add_billing_data_2_9 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billing')->insert([
            'organisation_id' => '1',
            'project_id' => '1',
        ]);
    }
}
