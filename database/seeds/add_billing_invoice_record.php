<?php

use Illuminate\Database\Seeder;

class add_billing_invoicerecord extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billing_invoice')->insert([
            'billing_id' => '1',
            'description' => 'Dit is een test record',
            'price_hour' => '5',
            'hours' => '1',
            'vat' => '21',
            'discount' => '0',
            'total_price_excl' => '1',
            'vat_diff' => '1',
            'total_price_incl' => '1',
        ]);
    }
}
