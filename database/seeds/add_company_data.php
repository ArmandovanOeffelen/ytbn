<?php

use Illuminate\Database\Seeder;

class add_company_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->insert([
            'name' => 'Rmnd Design',
            'email' => 'arrieality@gmail.com',
            'phone' => '234234243',
            'website' => 'rmnddesign.nl',
            'phone' => '234234243',
            'street' => 'Biezelingsestraat',
            'street_number' => '88',
            'city' => 'Kapelle',
            'postalcode' => '4421BT',
            'state' => 'Zeeland',
            'country' => 'Nederland',
            'coc_number' => '234234234dsfsz',
            'vat_number' => '23r234323rfdasdfsad',
            'bankaccount_number' => '23r23adsfasdfaasdfsad',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
