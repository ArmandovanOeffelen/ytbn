<?php

use Illuminate\Database\Seeder;

class add_contact_person extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact_person')->insert([
            'name' => 'Jesse Test',
            'email' => 'arrieality@gmail.com',
            'phone' => '0648707457',


        ]);
    }
}
