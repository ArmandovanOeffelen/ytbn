<?php

use Illuminate\Database\Seeder;

class add_employee_role_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_role')->insert([
            'role_name' => 'MASTER',
            'organisation' => '1',
            'invoicy_send' => '1',
            'quoty_send' => '1',
            'delete_organisation' => '1',
            'delete_project' => '1',
            'project_status' => '1',
            'employee' => '1',
            'delete_employee' => '1',
            'quoty' => '1',
            'company' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('employee_role')->insert([
            'role_name' => 'Account Manager',
            'organisation' => '1',
            'invoicy_send' => '1',
            'quoty_send' => '1',
            'delete_organisation' => '1',
            'delete_project' => '1',
            'project_status' => '1',
            'employee' => '0',
            'delete_employee' => '0',
            'quoty' => '1',
            'company' => '0',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
