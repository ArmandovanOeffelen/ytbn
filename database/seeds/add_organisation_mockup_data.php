<?php

use Illuminate\Database\Seeder;

class add_organisation_mockup_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organisation')->insert([
            'name' => 'Armando',
            'email' => 'arrieality@gmail.com',
            'phone' => '234234243',
            'website' => 'rmnddesign.nl',
            'phone' => '234234243',
            'street' => 'Biezelingsestraat',
            'street_number' => '88',
            'city' => 'Kapelle',
            'postalcode' => '4421BT',
            'state' => 'Zeeland',
            'country' => 'Nederland',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }


}
