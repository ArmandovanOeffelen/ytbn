<?php

use Illuminate\Database\Seeder;

class add_project_mockup_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project')->insert([
            'name'                  => 'YTBN',
            'user_id'               => '1',
            'summary'               => 'Dit project is gemaakt voor Dennis',
            'start_date'            => \Carbon\Carbon::now(),
            'end_date'              => \Carbon\Carbon::now(),
            'organisation_id'       => '1',
            'contact_person_id'     => '1',
            'project_status_id'     => '1',
            'finished'              => '0'


        ]);
    }
}
