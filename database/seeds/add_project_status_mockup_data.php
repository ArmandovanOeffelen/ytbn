<?php

use Illuminate\Database\Seeder;

class add_project_status_mockup_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_status')->insert([
            'status_type' => 'Project negotiations'
        ]);

        DB::table('project_status')->insert([
            'status_type' => 'Project Invoicy has been sent'
        ]);

        DB::table('project_status')->insert([
            'status_type' => 'Project started'
        ]);

        DB::table('project_status')->insert([
            'status_type' => 'Project ended'
        ]);
        DB::table('project_status')->insert([
            'status_type' => 'Project initiated'
        ]);

        DB::table('project_status')->insert([
            'status_type' => 'Project Quoty has been sent.'
        ]);
    }
}
