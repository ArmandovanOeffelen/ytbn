<?php

use Illuminate\Database\Seeder;

class add_service_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billing_service')->insert([
            'name' => 'Reparations',
            'vat' => '21',
        ]);
    }
}
