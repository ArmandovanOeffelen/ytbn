<?php

use Illuminate\Database\Seeder;

class add_vat_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billing_vat')->insert([
            'vat' => '21'
        ]);
    }
}
