function addRow() {

    let $increaseCounter = $('.modal-body').find('#number-amount');

    let $number = parseInt($increaseCounter.text());

    if ($number > 1) {
        for (let i = 0; i < $number; i++) {
            let cloneCount = $('.table-form-row').length;

            e.preventDefault();
            $("#form-row-0 .delete-row").removeClass('disabled');
            $("#form-row-0").clone().appendTo('#table-body').attr({'id': 'form-row-' + cloneCount}).insertAfter('[form-row-^=id]:last');

            $('#form-row-' + cloneCount + ' .form-group').removeClass('has-error');
            $('#form-row-' + cloneCount + " .help-block").remove();
            $('#form-row-' + cloneCount + " .description-field").attr({"name": 'description[' + cloneCount + "]"});
            $('#form-row-' + cloneCount + " .price-field").attr({"name": 'price[' + cloneCount + "]"});
            $('#form-row-' + cloneCount + " .time-field").attr({"name": 'hour[' + cloneCount + "]"});
            $('#form-row-' + cloneCount + " .vat-field").attr({"name": 'vat[' + cloneCount + "]"});
            $('#form-row-' + cloneCount + " .discount-field").attr({"name": 'discount[' + cloneCount + "]"});
            $('#form-row-' + cloneCount + " .price-total").attr({"name": 'total-price-field-' + cloneCount});
            $('#form-row-' + cloneCount + " input").val("");
            $('#form-row-' + cloneCount + " .discount-field").val("0");
            $('#form-row-' + cloneCount + " select").val(1);
            $('#form-row-' + cloneCount + " #total-price-excl-field").text("€ 0.00");
            $('#form-row-' + cloneCount + " #vat-difference-field").text("€ 0.00");
            $('#form-row-' + cloneCount + " #total-price-field").text("€ 0.00");

            if (cloneCount > 1) {
                $('.delete-row').removeClass('disabled');
            }
        }
    }
    else {
        let cloneCount = $('.table-form-row').length;

        e.preventDefault();
        $("#form-row-0 .delete-row").removeClass('disabled');
        $("#form-row-0").clone().appendTo('#table-body').attr({'id': 'form-row-' + cloneCount}).insertAfter('[form-row-^=id]:last');

        $('#form-row-' + cloneCount + ' .form-group').removeClass('has-error');
        $('#form-row-' + cloneCount + " .help-block").remove();
        $('#form-row-' + cloneCount + " .description-field").attr({"name": 'description[' + cloneCount + "]"});
        $('#form-row-' + cloneCount + " .price-field").attr({"name": 'price[' + cloneCount + "]"});
        $('#form-row-' + cloneCount + " .time-field").attr({"name": 'hour[' + cloneCount + "]"});
        $('#form-row-' + cloneCount + " .vat-field").attr({"name": 'vat[' + cloneCount + "]"});
        $('#form-row-' + cloneCount + " .discount-field").attr({"name": 'discount[' + cloneCount + "]"});
        $('#form-row-' + cloneCount + " .price-total").attr({"name": 'total-price-field-' + cloneCount});
        $('#form-row-' + cloneCount + " input").val("");
        $('#form-row-' + cloneCount + " .discount-field").val("0");
        $('#form-row-' + cloneCount + " select").val(1);
        $('#form-row-' + cloneCount + " #total-price-excl-field").text("€ 0.00");
        $('#form-row-' + cloneCount + " #vat-difference-field").text("€ 0.00");
        $('#form-row-' + cloneCount + " #total-price-field").text("€ 0.00");

        if (cloneCount > 1) {
            $('.delete-row').removeClass('disabled');
        }

    }
}
