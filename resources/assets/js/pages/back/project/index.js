$(function () {

    var templates = {
        createWorky: template('createWorky'),
        deleteWorky: template('deleteWorky'),
        createInvoicy: template('createInvoicy'),
        // updateInvoicy: template('updateInvoicy'),
        createQuoty: template('createQuoty'),
        loading: template('loading')
    };


    $('#content')

    /* Pause Worky */
        .on('click', '.pauseWorky', function () {
            var objectData = $(this).data('worky');
            var objectData1 = $(this).data('project');
            console.log(objectData);
            $.ajax({
                type: 'put',
                url: variables.pauseWorkyUrl.format(objectData1.id, objectData.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })

        /* Generate Invoice */
        .on('click', '.btn-generate-invoice', function () {
            var billing = $(this).data('billing');
            var project = $(this).data('project');
            var invoice = $(this).data('invoice');
            $.ajax({
                type: 'put',
                url: variables.generateInvoice.format(project.id, billing.id,invoice.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })

        /* Generate quoty */
        .on('click', '.btn-generate-quoty', function () {
            var billing = $(this).data('billing');
            var project = $(this).data('project');
            var invoice = $(this).data('invoice');
            $.ajax({
                type: 'put',
                url: variables.generateQuoty.format(project.id, billing.id,invoice.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })
        /* download invoicy */
        .on('click', '.btn-download-invoicy', function () {
            var billing = $(this).data('billing');
            var project = $(this).data('project');
            var invoice = $(this).data('invoice');
            $.ajax({
                type: 'put',
                url: variables.downloadInvoice.format(project.id, billing.id,invoice.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })
        /* unpause Worky */
        .on('click', '.unPauseWorky', function () {
            var objectData = $(this).data('worky');
            var objectData1 = $(this).data('project');
            console.log(objectData);
            $.ajax({
                type: 'post',
                url: variables.unPauseWorkyUrl.format(objectData1.id, objectData.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })

        /* complete Worky */
        .on('click', '.completeWorky', function () {
            var objectData = $(this).data('worky');
            var objectData1 = $(this).data('project');
            $.ajax({
                type: 'put',
                url: variables.completeWorkyUrl.format(objectData1.id, objectData.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })

        /* Restart Worky */
        .on('click', '.restartWorky', function () {
            var objectData = $(this).data('worky');
            var objectData1 = $(this).data('project');
            console.log(objectData);
            $.ajax({
                type: 'put',
                url: variables.restartWorkyUrl.format(objectData1.id, objectData.id),
                data: {
                    _token: variables.csrfToken
                },
                success: function (data) {
                    $('#content').html(data);
                },
                error: function (error) {
                    if (error.responseJSON.error != null) {
                        $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                    }
                }
            });
        })
        //
        /* Create invoicy from Workys*/
        //
        .on('click', '.createInvoicyFromWorky', function () {
            var project = $(this).data('project');
            var billing = $(this).data('billing');
            BootstrapDialog.show({
                title: 'Create invoicy from workys',
                message: $(templates.loading).load(variables.loadAllWorkysUrl.format(project.id,billing.id), function (response, status) {
                    if (status == 'error') {
                        var data = JSON.parse(response);
                        if (data.error != null) {
                            console.log(response);
                            $('#errors').html(data.error.html ? data.error.html : '');
                            dialog.close();
                        }

                        return;
                    }

                    var onchange = (el, row, value)=>{
                        let obj = $(el)[0];
                        let vat = obj.options[value]['vat'];
                        let service = obj.options[value]['id'];
                        row.find('#vat-percentage-field').text(vat + " %");
                        row.find('.service_hidden').val(service);
                    };

                    let tableRow = $('.table-form-row');

                    for(let row of tableRow){
                        row = $(row);

                        let selectize = row.find('.js-service-select').selectize({
                            onChange: function (value) {
                                onchange(this, row, value);
                            }
                        });

                        console.log(selectize);
                        let selectizeControl = selectize[0].selectize;

                        let initialValue = selectize.val(),
                            initialOption = selectizeControl.options[initialValue];
                        let $vat =  row.find('#vat-percentage-field');
                        console.log($vat);
                        $vat.text(initialOption['vat'] + " %");
                    }

                    $('.modal-dialog').addClass('modal-xl');

                }),
                nl2br: false,
                size: 'size-wide',
                onshown: function onshown() {

                    $('.modal-body').on('click','#btn-addrow', function (e) {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();
                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);

                                let row = $('#table-body').find("tr:last-of-type");
                                //selectizing in last created row
                                let selectize = $(row).find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        row.find('#vat-percentage-field').text(vat + " %");

                                        row.find('.service_hidden').val(service);
                                    }
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }
                        }
                    })
                        .on('click','.delete-row', function () {
                            let cloneCount = $('.table-form-row').length;
                            console.log("clone",cloneCount);
                            if(cloneCount > 1){
                                console.log("row",$(this).closest('tr'));
                                $(this).closest('tr').remove();
                                cloneCount--;
                                if (cloneCount === 1){
                                    $('.delete-row').addClass('disabled');
                                }
                            } else {
                                return;
                            }
                        }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    })
                    ;


                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);

                    };

                },
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function action(dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function action(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var tableRowLength = $('.table-form-row').length;
                        var tableArray = [];

                        for (var i = 0; i < tableRowLength; i++) {
                            var tableRowArray = {
                                id: $('#form-row-' + i).find('#hidden-field').val(),
                                description: $('#form-row-' + i).find('#description-field').val(),
                                price: $('#form-row-' + i).find('#price-field').val(),
                                hour: $('#form-row-' + i).find('#time-field').val(),
                                discount: $('#form-row-' + i).find('#discount-field').val(),
                                vat: $('#form-row-' + i).find('#vat-field option:selected').text(),
                                totalExcl: $('#form-row-' + i).find('#total-price-excl-field').text().replace("€ ", ''),
                                vatDiff: $('#form-row-' + i).find('#vat-difference-field').text().replace("€ ", ''),
                                totalIncl: $('#form-row-' + i).find('#total-price-field').text().replace("€ ", '')
                            };
                            tableArray[i] = tableRowArray;
                        }
                        $.ajax({
                            type: 'put',
                            url: variables.CreateInvoicyFromWorky.format(project.id, billing.id),
                            data: {
                                _token: variables.csrfToken,
                                tableArray: tableArray
                            },
                            success: function success(data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function error(error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }
                                let onchange = (el, row, value)=>{
                                    let obj = $(el)[0];
                                    let vat = obj.options[value]['vat'];
                                    let service = obj.options[value]['id'];
                                    row.find('#vat-percentage-field').text(vat + " %");
                                    row.find('.service_hidden').val(service);
                                };

                                let tableRow = $('.table-form-row');

                                for(let row of tableRow){
                                    row = $(row);

                                    let selectize = row.find('.js-service-select').selectize({
                                        onChange: function (value) {
                                            onchange(this, row, value);
                                        }
                                    });

                                    console.log(selectize);
                                    let selectizeControl = selectize[0].selectize;

                                    let initialValue = selectize.val(),
                                        initialOption = selectizeControl.options[initialValue];
                                    let $vat =  row.find('#vat-percentage-field');
                                    console.log($vat);
                                    $vat.text(initialOption['vat'] + " %");
                                }
                            }
                        });
                    }
                }]
            });
        })
        /* Create Invoicy */
        .on('click', '.createinvoicy', function () {
            var objectData1 = $(this).data('project');
            BootstrapDialog.show({
                title: 'Create Invoicy',
                size: 'size-wide',
                message: templates.createInvoicy,
                nl2br: false,
                onshown: function (){
                    $('.modal-dialog').addClass('modal-xl');
                    let rowCount = $('.table-form-row').length;
                    if (rowCount === 1) {
                        $('.delete-row').addClass('disabled');
                    }

                    let selectize = $('.modal-body').find('.js-service-select').selectize({
                        onChange: function(value){
                            let obj = $(this)[0];
                            let vat = obj.options[value]['vat'];
                            let service = obj.options[value]['id'];

                            $('.modal-body').find('#vat-percentage-field').text(vat + " %");

                            $('.modal-body').find('.service_hidden').val(service);
                        }
                    });

                    let selectizeControl = selectize[0].selectize;


                    $('.modal-body').find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                    $('.modal-body').on('click','#btn-addrow', function (e) {

                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();
                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);


                                let row = $('#table-body').find("tr:last-of-type");
                                //selectizing in last created row
                                let selectize = $(row).find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        row.find('#vat-percentage-field').text(vat + " %");

                                        row.find('.service_hidden').val(service);
                                    }
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }

                        }



                    })
                    .on('click','.delete-row', function () {
                        let cloneCount = $('.table-form-row').length;
                        console.log("clone",cloneCount);
                        if(cloneCount > 1){
                            console.log("row",$(this).closest('tr'));
                            $(this).closest('tr').remove();
                            cloneCount--;
                            if (cloneCount === 1){
                                $('.delete-row').addClass('disabled');
                            }
                        } else {
                            return;

                        }

                    }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    })
                    ;


                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);

                    };
                },

                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);
                        let table = document.getElementById("table-invoice");

                        let tableRowLength = $('.table-form-row').length;

                        let tableArray = [];
                        for(let i = 0; i<tableRowLength; i++){
                            let tableRowArray = {
                                service: $('#form-row-'+i).find('#service-field').val(),
                                description: $('#form-row-'+i).find('#description-field').val(),
                                price: $('#form-row-'+i).find('#price-field').val(),
                                hour: $('#form-row-'+i).find('#time-field').val(),
                                discount: $('#form-row-'+i).find('#discount-field').val(),
                                vat: $('#form-row-'+i).find('#vat-percentage-field').text().replace(" %",""),
                                total_price_excl: $('#form-row-'+i).find('#total-price-excl-field').text().replace("€ ",''),
                                vat_diff: $('#form-row-'+i).find('#vat-difference-field').text().replace("€ ",''),
                                total_price_incl: $('#form-row-'+i).find('#total-price-field').text().replace("€ ",''),
                            };
                            tableArray[i] = tableRowArray;
                        }

                        $.ajax({
                            type: 'post',
                            url: variables.createInvoicyUrl.format(objectData1.id),
                            data: {
                                _token: variables.csrfToken,
                                tableArray: tableArray,
                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }
                                let tableRow = $('.table-form-row');

                                for(let row of tableRow){
                                    row = $(row);

                                    let selectize = row.find('.js-service-select').selectize({
                                        onChange: function (value) {
                                            onchange(this, row, value);
                                        }
                                    });

                                    console.log(selectize);
                                    let selectizeControl = selectize[0].selectize;

                                    let initialValue = selectize.val(),
                                        initialOption = selectizeControl.options[initialValue];
                                    let $vat =  row.find('#vat-percentage-field');
                                    console.log($vat);
                                    $vat.text(initialOption['vat'] + " %");
                                }

                            }
                        });

                    }
                }]
            });


        })

        /* Create Quoty */
        .on('click', '.createQuoty', function () {
            var objectData1 = $(this).data('project');
            var objectData = $(this).data('billing');
            let cloneCount = 0;
            let rowCount = $('.table-form-row').length;
            BootstrapDialog.show({
                title: 'Create Quoty',
                size: 'size-wide',
                message: templates.createQuoty,
                nl2br: false,
                onshown: function (){
                    $('.modal-dialog').addClass('modal-xl');
                    let rowCount = $('.table-form-row').length;
                    if (rowCount === 1) {
                        $('.delete-row').addClass('disabled');
                    }

                    let selectize = $('.modal-body').find('.js-service-select').selectize({
                        onChange: function(value){
                            let obj = $(this)[0];
                            let vat = obj.options[value]['vat'];
                            let service = obj.options[value]['id'];

                            $('.modal-body').find('#vat-percentage-field').text(vat + " %");

                            $('.modal-body').find('.service_hidden').val(service);
                        }
                    });

                    let selectizeControl = selectize[0].selectize;


                    $('.modal-body').find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                    $('.modal-body').on('click','#btn-addrow', function (e) {

                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();

                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);


                                //selectizing in last created row
                                let selectize = $row.find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        $row.find('#vat-percentage-field').text(vat + " %");

                                        $row.find('.service_hidden').val(service);
                                    },
                                    onError: console.error
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                $row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }

                        }



                    })
                        .on('click','.delete-row', function () {
                            let cloneCount = $('.table-form-row').length;
                            console.log("clone",cloneCount);
                            if(cloneCount > 1){
                                console.log("row",$(this).closest('tr'));
                                $(this).closest('tr').remove();
                                cloneCount--;
                                if (cloneCount === 1){
                                    $('.delete-row').addClass('disabled');
                                }
                            } else {
                                return;

                            }

                        }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    });

                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);
                    };

                },

                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        let tableRowLength = $('.table-form-row').length;
                        console.log(tableRowLength);

                        let tableArray = [];
                        for(let i = 0; i<tableRowLength; i++){
                            let tableRowArray = {
                                service: $('#form-row-'+i).find('#service-field').val(),
                                description: $('#form-row-'+i).find('#description-field').val(),
                                price: $('#form-row-'+i).find('#price-field').val(),
                                hour: $('#form-row-'+i).find('#time-field').val(),
                                discount: $('#form-row-'+i).find('#discount-field').val(),
                                vat: $('#form-row-'+i).find('#vat-percentage-field').text().replace(" %",""),
                                total_price_excl: $('#form-row-'+i).find('#total-price-excl-field').text().replace("€ ",''),
                                vat_diff: $('#form-row-'+i).find('#vat-difference-field').text().replace("€ ",''),
                                total_price_incl: $('#form-row-'+i).find('#total-price-field').text().replace("€ ",''),
                            };
                            tableArray[i] = tableRowArray;
                        }
                        console.log(tableArray);

                        $.ajax({
                            type: 'post',
                            url: variables.createQuotyUrl.format(objectData1.id, objectData.id),
                            data: {

                                _token: variables.csrfToken,
                                tableArray: tableArray,

                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();




                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }
                                let tableRow = $('.table-form-row');

                                for(let row of tableRow){
                                    row = $(row);

                                    let selectize = row.find('.js-service-select').selectize({
                                        onChange: function (value) {
                                            onchange(this, row, value);
                                        }
                                    });

                                    console.log(selectize);
                                    let selectizeControl = selectize[0].selectize;

                                    let initialValue = selectize.val(),
                                        initialOption = selectizeControl.options[initialValue];
                                    let $vat =  row.find('#vat-percentage-field');
                                    console.log($vat);
                                    $vat.text(initialOption['vat'] + " %");
                                }

                            }
                        });

                    }
                }]
            });


        })
        /* Create Worky */
        .on('click', '#startWorky', function () {
            var objectData1 = $(this).data('project');
            BootstrapDialog.show({
                title: 'Add worky',
                message: $(templates.loading).load(variables.loadWorkyForm.format(objectData1.id), function (response, status) {
                if (status == 'error') {
                    var data = JSON.parse(response);
                    if (data.error != null) {
                        console.log(response);
                        $('#errors').html(data.error.html ? data.error.html : '');
                        dialog.close();
                    }

                    return;

                }

                var $this = $(this);

                $this.find('.js-service-select').selectize({

                    onChange: function(value){
                        let obj = $(this)[0];
                        let vat = obj.options[value]['vat'];
                        let service = obj.options[value]['id'];


                        //TODO:: Make this look pretty in the gui.
                        $('.modal-body').find('.vat-percentage').text(vat + " %");

                        console.log("vat xD",obj.options[value])

                        $('.modal-body').find('.vat_hidden').val(vat);
                        $('.modal-body').find('.service_hidden').val(service);
                    }
                });
            }),
                nl2br: false,
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();


                    }

                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'post',
                            url: variables.addWorkyUrl.format(objectData1.id),
                            data: {
                                _token: variables.csrfToken,
                                job: dialog.getModalContent().find('textarea[name="job"]').val(),
                                service: dialog.getModalContent().find('select[name="service"]').val(),
                                price: dialog.getModalContent().find('input[name="price"]').val(),
                                vat: dialog.getModalContent().find('input[name="vat"]').val()
                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                            }
                        });

                    }
                }]
            });
        })

        /*
        Add completed Worky
         */
        .on('click', '.addFinishedWorky', function () {
            var project = $(this).data('project');
            BootstrapDialog.show({
                title: 'Edit Quoty',
                message: $(templates.loading)
                    .load(variables.loadCompletedWorkyUrl.format(project.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    console.log(response);
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }

                                return;
                            }

                            let $selectize = $('.modal-body').find('.js-service-select').selectize({
                                onChange: function(value){
                                    let obj = $(this)[0];
                                    let vat = obj.options[value]['vat'];
                                    let service = obj.options[value]['id'];

                                    $('.modal-body').find('#vat-percentage-field').val(vat);
                                    $('.modal-body').find('#shown-vat-percentage-field').text(vat + " %");

                                    console.log("vat",$('.modal-body').find('#vat-percentage-field'));

                                }
                            });

                            let $selectizeControl = $selectize[0].selectize;

                            $('.modal-body').find('#shown-vat-percentage-field').text($selectizeControl.options[1]['vat'] + " %");
                            $('.modal-body').find('#vat-percentage-field').val($selectizeControl.options[1]['vat']);
                        }),
                nl2br: false,
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,

                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);


                        $.ajax({
                            type: 'post',
                            url: variables.addCompletedWorkyUrl.format(project.id),

                            data: {
                                _token: variables.csrfToken,
                                job: dialog.getModalContent().find('textarea[name="job"]').val(),
                                price: dialog.getModalContent().find('input[name="price"]').val(),
                                vat: dialog.getModalContent().find('input[name="vat"]').val(),
                                service: dialog.getModalContent().find('select[name="service"]').val(),
                                hours: dialog.getModalContent().find('input[name="hours"]').val(),
                                minutes: dialog.getModalContent().find('select[name="minutes"]').val(),

                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                            }
                        });

                    }
                }]
            });
        })


        //
        /* Update Quoty */
        //
        .on('click', '.btn-edit-quoty', function () {
            var project = $(this).data('project');
            var billing = $(this).data('billing');
            var quoty = $(this).data('quoty');

            BootstrapDialog.show({
                title: 'Edit Quoty',
                message: $(templates.loading)
                    .load(variables.showQuotyUrl.format(project.id,quoty.id,billing.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    console.log(response);
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }

                                return;
                            }

                            var onchange = (el, row, value)=>{
                                let obj = $(el)[0];
                                let vat = obj.options[value]['vat'];
                                let service = obj.options[value]['id'];
                                row.find('#vat-percentage-field').text(vat + " %");
                                row.find('.service_hidden').val(service);
                            };

                            let tableRow = $('.table-form-row');

                            for(let row of tableRow){
                                row = $(row);

                                let selectize = row.find('.js-service-select').selectize({
                                    onChange: function (value) {
                                        onchange(this, row, value);
                                    }
                                });

                                console.log(selectize);
                                let selectizeControl = selectize[0].selectize;

                                let initialValue = selectize.val(),
                                    initialOption = selectizeControl.options[initialValue];
                                let $vat =  row.find('#vat-percentage-field');
                                console.log($vat);
                                $vat.text(initialOption['vat'] + " %");
                            }

                            $('.modal-dialog').addClass('modal-xl');
                        }),
                nl2br: false,
                size: 'size-wide',
                onshown: function (){
                    $('.modal-body').on('click','#btn-addrow', function (e) {

                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();

                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);


                                //selectizing in last created row
                                let selectize = $row.find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        $row.find('#vat-percentage-field').text(vat + " %");

                                        $row.find('.service_hidden').val(service);
                                    },
                                    onError: console.error
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                $row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }

                        }



                    })
                        .on('click','.delete-row', function () {
                            let cloneCount = $('.table-form-row').length;
                            console.log("clone",cloneCount);
                            if(cloneCount > 1){
                                console.log("row",$(this).closest('tr'));
                                $(this).closest('tr').remove();
                                cloneCount--;
                                if (cloneCount === 1){
                                    $('.delete-row').addClass('disabled');
                                }
                            } else {
                                return;

                            }

                        }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    });

                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);
                    };
                },
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,

                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);


                        let tableRowLength = $('.table-form-row').length;

                        let tableArray = [];
                        for(let i = 0; i<tableRowLength; i++){
                            let tableRowArray = {
                                id: $('#form-row-'+i).find('#hidden-field').val(),
                                service: $('#form-row-'+i).find('#service-field').val(),
                                description: $('#form-row-'+i).find('#description-field').val(),
                                price: $('#form-row-'+i).find('#price-field').val(),
                                hour: $('#form-row-'+i).find('#time-field').val(),
                                discount: $('#form-row-'+i).find('#discount-field').val(),
                                vat: $('#form-row-'+i).find('#vat-percentage-field').text().replace(" %",""),
                                total_price_excl: $('#form-row-'+i).find('#total-price-excl-field').text().replace("€ ",''),
                                vat_diff: $('#form-row-'+i).find('#vat-difference-field').text().replace("€ ",''),
                                total_price_incl: $('#form-row-'+i).find('#total-price-field').text().replace("€ ",''),
                            };
                            tableArray[i] = tableRowArray;

                        }
                        $.ajax({
                            type: 'put',
                            url: variables.updateQuotyUrl.format(project.id,billing.id,quoty.id),

                            data: {
                                _token: variables.csrfToken,
                                tableArray: tableArray,

                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                                let onchange = (el, row, value)=>{
                                    let obj = $(el)[0];
                                    let vat = obj.options[value]['vat'];
                                    let service = obj.options[value]['id'];
                                    row.find('#vat-percentage-field').text(vat + " %");
                                    row.find('.service_hidden').val(service);
                                };

                                let tableRow = $('.table-form-row');

                                for(let row of tableRow){
                                    row = $(row);

                                    let selectize = row.find('.js-service-select').selectize({
                                        onChange: function (value) {
                                            onchange(this, row, value);
                                        }
                                    });

                                    console.log(selectize);
                                    let selectizeControl = selectize[0].selectize;

                                    let initialValue = selectize.val(),
                                        initialOption = selectizeControl.options[initialValue];
                                    let $vat =  row.find('#vat-percentage-field');
                                    console.log($vat);
                                    $vat.text(initialOption['vat'] + " %");
                                }

                            }
                        });

                    }
                }]
            });
        })
        //
        /* Update Invoicy */
        //
        .on('click', '.btn-edit-invoicy', function () {
            var project = $(this).data('project');
            var billing = $(this).data('billing');
            var invoice = $(this).data('invoice');
            BootstrapDialog.show({
                title: 'Edit Invoicy',
                message: $(templates.loading)
                    .load(variables.showInvoicyUrl.format(project.id,invoice.id,billing.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    console.log(response);
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }

                                return;
                            }

                            var onchange = (el, row, value)=>{
                                let obj = $(el)[0];
                                let vat = obj.options[value]['vat'];
                                let service = obj.options[value]['id'];
                                row.find('#vat-percentage-field').text(vat + " %");
                                row.find('.service_hidden').val(service);
                            };

                            let tableRow = $('.table-form-row');

                            for(let row of tableRow){
                                row = $(row);

                                let selectize = row.find('.js-service-select').selectize({
                                    onChange: function (value) {
                                        onchange(this, row, value);
                                    }
                                });

                                console.log(selectize);
                                let selectizeControl = selectize[0].selectize;

                                let initialValue = selectize.val(),
                                    initialOption = selectizeControl.options[initialValue];
                                let $vat =  row.find('#vat-percentage-field');
                                console.log($vat);
                                $vat.text(initialOption['vat'] + " %");
                            }

                            $('.modal-dialog').addClass('modal-xl');
                        }),
                nl2br: false,
                size: 'size-wide',
                onshown: function (){

                    $('.modal-body').on('click','#btn-addrow', function (e) {

                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();

                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);


                                //selectizing in last created row
                                let selectize = $row.find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        $row.find('#vat-percentage-field').text(vat + " %");

                                        $row.find('.service_hidden').val(service);
                                    },
                                    onError: console.error
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                $row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }
                        }
                    })
                        .on('click','.delete-row', function () {
                            let cloneCount = $('.table-form-row').length;
                            console.log("clone",cloneCount);
                            if(cloneCount > 1){
                                console.log("row",$(this).closest('tr'));
                                $(this).closest('tr').remove();
                                cloneCount--;
                                if (cloneCount === 1){
                                    $('.delete-row').addClass('disabled');
                                }
                            } else {
                                return;

                            }

                        }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    });
                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);
                    };
                },
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,

                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);


                        let tableRowLength = $('.table-form-row').length;
                        console.log(tableRowLength);

                        let tableArray = [];
                        for(let i = 0; i<tableRowLength; i++){
                            let tableRowArray = {
                                id: $('#form-row-'+i).find('#hidden-field').val(),
                                service: $('#form-row-'+i).find('#service-field').val(),
                                description: $('#form-row-'+i).find('#description-field').val(),
                                price: $('#form-row-'+i).find('#price-field').val(),
                                hour: $('#form-row-'+i).find('#time-field').val(),
                                discount: $('#form-row-'+i).find('#discount-field').val(),
                                vat: $('#form-row-'+i).find('#vat-percentage-field').text().replace(" %",""),
                                total_price_excl: $('#form-row-'+i).find('#total-price-excl-field').text().replace("€ ",''),
                                vat_diff: $('#form-row-'+i).find('#vat-difference-field').text().replace("€ ",''),
                                total_price_incl: $('#form-row-'+i).find('#total-price-field').text().replace("€ ",''),
                            };
                            tableArray[i] = tableRowArray;
                        }
                        console.log(tableArray);
                        $.ajax({
                            type: 'put',
                            url: variables.updateInvoicyUrl.format(project.id,invoice.id,billing.id),
                            data: {
                                _token: variables.csrfToken,
                                tableArray: tableArray,

                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                                let onchange = (el, row, value)=>{
                                    let obj = $(el)[0];
                                    let vat = obj.options[value]['vat'];
                                    let service = obj.options[value]['id'];
                                    row.find('#vat-percentage-field').text(vat + " %");
                                    row.find('.service_hidden').val(service);
                                };

                                let tableRow = $('.table-form-row');

                                for(let row of tableRow){
                                    row = $(row);

                                    let selectize = row.find('.js-service-select').selectize({
                                        onChange: function (value) {
                                            onchange(this, row, value);
                                        }
                                    });

                                    console.log(selectize);
                                    let selectizeControl = selectize[0].selectize;

                                    let initialValue = selectize.val(),
                                        initialOption = selectizeControl.options[initialValue];
                                    let $vat =  row.find('#vat-percentage-field');
                                    console.log($vat);
                                    $vat.text(initialOption['vat'] + " %");
                                }
                            }
                        });

                    }
                }]
            });
        })

        //
        /* Update project */
        //
        .on('click', '.buttonUpdate', function () {
            var objectData = $(this).data('data');
            BootstrapDialog.show({
                title: 'Edit project',
                message: $(templates.loading)
                    .load(variables.updateProjectTemplateUrl.format(objectData.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    console.log(response);
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }
                                return;
                            }

                            var $this = $(this);
                            $this.find('.js-organisation-select').selectize();
                            $this.find('.js-project-status-select').selectize();
                            $this.find('.js-project-manager-select').selectize();
                        }),
                nl2br: false,
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,

                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'put',
                            url: variables.updateProjectUrl.format(objectData.id),
                            data: {
                                _token: variables.csrfToken,
                                name: dialog.getModalContent().find('input[name="name"]').val(),
                                project_manager: dialog.getModalContent().find('select[name="project_manager"]').val(),
                                organisation: dialog.getModalContent().find('select[name="organisation"]').val(),
                                project_status: dialog.getModalContent().find('select[name="project_status"]').val(),
                                start_date: dialog.getModalContent().find('input[name="start_date"]').val(),
                                end_date: dialog.getModalContent().find('input[name="end_date"]').val(),
                                project_summary: dialog.getModalContent().find('textarea[name="project_summary"]').val(),
                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }
                            }
                        });
                    }
                }]
            });
        })

        /* Update ContactPerson */
        .on('click', '.buttonUpdateContactPerson', function () {
            var objectData = $(this).data('data');
            var objectData1 = $(this).data('project');
            var projectId = objectData1.id;

            BootstrapDialog.show({
                title: 'Edit contact person',
                message: $(templates.loading)
                    .load(variables.editContactPersonTemplateUrl.format(objectData.id),
                        function (response, status) {
                            console.log(response);
                            console.log(status);
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    console.log(response);
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }
                            }
                        }),
                nl2br: false,
                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'put',
                            url: variables.editContactPersonUrl.format(projectId, objectData.id),
                            data: {
                                _token: variables.csrfToken,
                                //project details
                                name: dialog.getModalContent().find('input[name="name"]').val(),
                                phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                email: dialog.getModalContent().find('input[name="email"]').val()
                            },
                            success: function (data) {
                                dialog.close();
                                console.log(data);
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();
                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }
                            }
                        });
                    }
                }]
            });
        })

        /* Delete Worky */
        .on('click', '.deleteWorky', function () {
            var objectData = $(this).data('worky');
            var objectData1 = $(this).data('project');
            console.log("test");
            BootstrapDialog.show({
                title: 'Delete Worky',
                message: templates.deleteWorky.format(objectData.job),
                nl2br: false,
                buttons: [{
                    label: 'Cancel',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'Delete',
                    label: 'Delete',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'delete',
                            url: variables.deleteWorkyUrl.format(objectData1.id, objectData.id),
                            data: {
                                _token: variables.csrfToken
                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('Delete').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                }
                            }
                        });
                    }
                }]
            });
        })

        /* Delete */
        .on('click', '.buttonDelete', function () {
            var objectData = $(this).closest('tr').data('data');
            console.log(objectData);
            BootstrapDialog.show({
                title: 'Prospect verwijderen',
                message: templates.delete.format(objectData.customer_name),
                nl2br: false,
                buttons: [{
                    label: 'Annuleren',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'Verwijderen',
                    label: 'Verwijderen',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'delete',
                            url: variables.deleteOrganisationUrl.format(objectData.id),
                            data: {
                                _token: variables.csrfToken
                            },
                            success: function (data) {
                                dialog.close();

                                $('#content').html(data.html);

                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                }

                            }
                        });

                    }
                }]
            });
        });
});

//TODO:: Check if inputs are Numbers else.
calculateTotalPrice = function(tr) {
        let pricefield = tr.find('#price-field').val();
        let timefield = tr.find('#time-field').val();
        let discountfield = tr.find('#discount-field').val();
        let vatfield = tr.find('#vat-percentage-field').text().replace(" %","");
        let totalExcl = tr.find('#total-price-excl-field');
        let totalDiff = tr.find('#vat-difference-field');
        let totalIncl = tr.find('#total-price-field');

        let totalWithoutDiscountOrVat = pricefield * timefield;

        let one = "1.";
        let vat = one.concat(vatfield);
        let vatFloat = parseFloat(vat).toFixed(2);

        if(discountfield > 100){
            setTimeout(5000,tr.find('#time-field').addClass('naNFound'));

            return;
        }

        //prijs = beginprijs - (beginprijs * (korting / 100) )
        //TODO:: iets bedenken voor als de discount hoger dan 100 is.
        //total excl vat
        let totalPriceWithoutVat = totalWithoutDiscountOrVat - (totalWithoutDiscountOrVat * (discountfield/100));
        let totalPriceWithoutVatFloat = parseFloat(totalPriceWithoutVat).toFixed(2);

        //total incl vat
        let totalpriceInclVat = totalPriceWithoutVat * vatFloat;

        let totalPriceInclVatFloat = parseFloat(totalpriceInclVat).toFixed(2);

        //vat difference in euro.
        let totalVatDiff = totalpriceInclVat - totalPriceWithoutVat;
        let totalVatDiffFloat = parseFloat(totalVatDiff).toFixed(2);

        totalExcl.text("€ " +totalPriceWithoutVatFloat);
        totalIncl.text("€ " +totalPriceInclVatFloat);
        totalDiff.text("€ " +totalVatDiffFloat);
    };

    checkforNaNAndImpossibleDiscount = function (tr) {
        let pricefield = tr.find('#price-field');
        let timefield = tr.find('#time-field');
        let discountfield = tr.find('#discount-field');
        let totalExcl = tr.find('#total-price-excl-field');
        let totalDiff = tr.find('#vat-difference-field');
        let totalIncl = tr.find('#total-price-field');



        if(isNaN(pricefield.val())){
            setTimeout(3000,pricefield.addClass('naNFound'));
            totalExcl.text("€ 0.00");
            totalDiff.text("€ 0.00");
            totalIncl.text("€ 0.00");
        } else if (!isNaN(pricefield.val())){
            setTimeout(3000,pricefield.removeClass('naNFound'));
        }
        if(isNaN(timefield.val())){
            setTimeout(3000,timefield.addClass('naNFound'));
            totalExcl.text("€ 0.00");
            totalDiff.text("€ 0.00");
            totalIncl.text("€ 0.00");
        } else if (!isNaN(timefield.val())){
            setTimeout(3000,timefield.removeClass('naNFound'));
        }
        if(isNaN(discountfield.val()) || discountfield.val() >100){
            setTimeout(3000,discountfield.addClass('naNFound'));
            totalExcl.text("€ 0.00");
            totalDiff.text("€ 0.00");
            totalIncl.text("€ 0.00");
        } else if (!isNaN(discountfield.val())){
            setTimeout(3000,discountfield.removeClass('naNFound'));
        }
    };
