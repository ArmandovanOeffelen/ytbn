$(function () {

    var templates = {
        delete: template('delete'),
        loading: template('loading'),
        createQuoty: template('createQuoty'),
    };

    $('#content')

        .on('click', '.createQuoty', function () {
            var objectData1 = $(this).data('project');
            var objectData = $(this).data('billing');
            console.log(objectData1,"-", objectData);
            BootstrapDialog.show({
                title: 'Create Quoty',
                size: 'size-wide',
                message: templates.createQuoty,
                nl2br: false,
                onshown: function (){
                    $('.modal-dialog').addClass('modal-xl');
                    let rowCount = $('.table-form-row').length;
                    if (rowCount === 1) {
                        $('.delete-row').addClass('disabled');
                    }

                    let selectize = $('.modal-body').find('.js-service-select').selectize({
                        onChange: function(value){
                            let obj = $(this)[0];
                            let vat = obj.options[value]['vat'];
                            let service = obj.options[value]['id'];

                            $('.modal-body').find('#vat-percentage-field').text(vat + " %");

                            $('.modal-body').find('.service_hidden').val(service);
                        }
                    });

                    let selectizeControl = selectize[0].selectize;


                    $('.modal-body').find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                    $('.modal-body').on('click','#btn-addrow', function (e) {

                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        if ($number => 1) {
                            for (let i = 0; i < $number; i++) {
                                //selecting last created row
                                let selectingLastRow = $('#table-body').find("tr:last-of-type");
                                //getting id of last row parse it into int so that we can change index.
                                let id = parseInt(selectingLastRow.attr('id').replace("form-row-",""));
                                id++;
                                // getting the template row.
                                let template = $('#template_row').html();

                                //replacing the placeholder with the counter.
                                template = template.replace(/_index_/g,id);
                                // make jquery object of html
                                var $row = $(template);

                                // add to tablebody
                                $('#table-body').append($row);


                                //selectizing in last created row
                                let selectize = $row.find('.js-service-select').selectize({
                                    onChange: function(value){
                                        let obj = $(this)[0];
                                        let vat = obj.options[value]['vat'];
                                        let service = obj.options[value]['id'];

                                        $row.find('#vat-percentage-field').text(vat + " %");

                                        $row.find('.service_hidden').val(service);
                                    },
                                    onError: console.error
                                });
                                //setting the vat value in the formrow
                                let selectizeControl = selectize[0].selectize;
                                $row.find('#vat-percentage-field').text(selectizeControl.options[1]['vat'] + " %");

                                if (id > 1) {
                                    $('.delete-row').removeClass('disabled');
                                }
                            }

                        }



                    })
                        .on('click','.delete-row', function () {
                            let cloneCount = $('.table-form-row').length;
                            console.log("clone",cloneCount);
                            if(cloneCount > 1){
                                console.log("row",$(this).closest('tr'));
                                $(this).closest('tr').remove();
                                cloneCount--;
                                if (cloneCount === 1){
                                    $('.delete-row').addClass('disabled');
                                }
                            } else {
                                return;

                            }

                        }).on('click','#btn-increase', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());

                        $number ++;

                        $increaseCounter.text($number);
                    }).on('click','#btn-decrease', function () {
                        let $increaseCounter = $('.modal-body').find('#number-amount');

                        let $number = parseInt($increaseCounter.text());
                        if ($number <= 1 ){
                            return;
                        } else {
                            $number --;
                        }
                        $increaseCounter.text($number);
                    });

                    calculatePrices = function(tr){
                        calculateTotalPrice(tr);
                        checkforNaNAndImpossibleDiscount(tr);

                    };

                },

                buttons: [{
                    label: 'Cancel',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        let tableRowLength = $('.table-form-row').length;
                        console.log(tableRowLength);

                        let tableArray = [];
                        for(let i = 0; i<tableRowLength; i++){
                            let tableRowArray = {
                                service: $('#form-row-'+i).find('#service-field').val(),
                                description: $('#form-row-'+i).find('#description-field').val(),
                                price: $('#form-row-'+i).find('#price-field').val(),
                                hour: $('#form-row-'+i).find('#time-field').val(),
                                discount: $('#form-row-'+i).find('#discount-field').val(),
                                vat: $('#form-row-'+i).find('#vat-percentage-field').text().replace(" %",""),
                                total_price_excl: $('#form-row-'+i).find('#total-price-excl-field').text().replace("€ ",''),
                                vat_diff: $('#form-row-'+i).find('#vat-difference-field').text().replace("€ ",''),
                                total_price_incl: $('#form-row-'+i).find('#total-price-field').text().replace("€ ",''),
                            };
                            tableArray[i] = tableRowArray;
                        }
                        console.log(tableArray);

                        $.ajax({
                            type: 'post',
                            url: variables.createQuotyUrl.format(objectData1.id, objectData.id),
                            data: {

                                _token: variables.csrfToken,
                                tableArray: tableArray,

                            },
                            success: function (data) {
                                dialog.close();
                                $('#content').html(data);
                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();




                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                            }
                        });

                    }
                }]
            });


        });
});

calculateTotalPrice = function(tr) {
    let pricefield = tr.find('#price-field').val();
    let timefield = tr.find('#time-field').val();
    let discountfield = tr.find('#discount-field').val();
    let vatfield = tr.find('#vat-percentage-field').text().replace(" %","");
    let totalExcl = tr.find('#total-price-excl-field');
    let totalDiff = tr.find('#vat-difference-field');
    let totalIncl = tr.find('#total-price-field');
    console.log('taking vat',vatfield);

    let totalWithoutDiscountOrVat = pricefield * timefield;

    let one = "1.";
    let vat = one.concat(vatfield);
    let vatFloat = parseFloat(vat).toFixed(2);

    if(discountfield > 100){
        setTimeout(5000,tr.find('#time-field').addClass('naNFound'));

        return;
    }

    //prijs = beginprijs - (beginprijs * (korting / 100) )
    //total excl vat
    let totalPriceWithoutVat = totalWithoutDiscountOrVat - (totalWithoutDiscountOrVat * (discountfield/100));
    let totalPriceWithoutVatFloat = parseFloat(totalPriceWithoutVat).toFixed(2);

    //total incl vat
    let totalpriceInclVat = totalPriceWithoutVat * vatFloat;

    let totalPriceInclVatFloat = parseFloat(totalpriceInclVat).toFixed(2);

    //vat difference in euro.
    let totalVatDiff = totalpriceInclVat - totalPriceWithoutVat;
    let totalVatDiffFloat = parseFloat(totalVatDiff).toFixed(2);

    totalExcl.text("€ " +totalPriceWithoutVatFloat);
    totalIncl.text("€ " +totalPriceInclVatFloat);
    totalDiff.text("€ " +totalVatDiffFloat);
};

checkforNaNAndImpossibleDiscount = function (tr) {
    let pricefield = tr.find('#price-field');
    let timefield = tr.find('#time-field');
    let discountfield = tr.find('#discount-field');
    let totalExcl = tr.find('#total-price-excl-field');
    let totalDiff = tr.find('#vat-difference-field');
    let totalIncl = tr.find('#total-price-field');



    if(isNaN(pricefield.val())){
        setTimeout(3000,pricefield.addClass('naNFound'));
        totalExcl.text("€ 0.00");
        totalDiff.text("€ 0.00");
        totalIncl.text("€ 0.00");
    } else if (!isNaN(pricefield.val())){
        setTimeout(3000,pricefield.removeClass('naNFound'));
    }
    if(isNaN(timefield.val())){
        setTimeout(3000,timefield.addClass('naNFound'));
        totalExcl.text("€ 0.00");
        totalDiff.text("€ 0.00");
        totalIncl.text("€ 0.00");
    } else if (!isNaN(timefield.val())){
        setTimeout(3000,timefield.removeClass('naNFound'));
    }
    if(isNaN(discountfield.val()) || discountfield.val() >100){
        setTimeout(3000,discountfield.addClass('naNFound'));
        totalExcl.text("€ 0.00");
        totalDiff.text("€ 0.00");
        totalIncl.text("€ 0.00");
    } else if (!isNaN(discountfield.val())){
        setTimeout(3000,discountfield.removeClass('naNFound'));
    }
};