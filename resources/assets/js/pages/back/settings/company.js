$(function () {

    var templates = {
        delete: template('delete'),
        loading: template('loading')
    };

    $('#content')

    //update Company Logo
        .on('click', '.buttonUpdateLogo', function () {
            var objectData = $(this).data('data');
            BootstrapDialog.show({
                title: 'Upload logo',
                message: $(templates.loading)
                    .load(variables.loadCompanyLogoUrl.format(objectData.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }
                            }
                        }),
                nl2br: false,
                buttons: [{
                    label: 'cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var formData = new FormData();

                        formData.append('logo', $('#company-logo')[0].files[0]);

                        $.ajax({
                            type: 'post',
                            contentType: false,
                            processData: false,
                            cache: false,
                            url: variables.updateLogoUrl.format(objectData.id),
                            data: formData,
                            success: function (data) {
                                dialog.close();

                                $('#content').html(data);

                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                            }
                        });

                    }
                }]
            });
        })
        //update Company
        .on('click', '.buttonUpdate', function () {
            console.log('updatebutton clicked')

            var objectData = $(this).data('data');
            BootstrapDialog.show({
                title: 'Edit company',
                message: $(templates.loading)
                    .load(
                        variables.loadCompanyUrl.format(objectData.id),
                        function (response, status) {
                            if (status == 'error') {
                                var data = JSON.parse(response);
                                if (data.error != null) {
                                    $('#errors').html(data.error.html ? data.error.html : '');
                                    dialog.close();
                                }
                            }
                        }
                    ),
                nl2br: false,
                buttons: [{
                    label: 'cancel',
                    cssClass: 'btn-danger',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }, {
                    id: 'submit',
                    label: 'Save',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        $.ajax({
                            type: 'put',
                            url: variables.updateCompanyUrl.format(objectData.id),
                            data: {
                                // _token: '{{ csrf_token() }}',
                                name: dialog.getModalContent().find('input[name="name"]').val(),
                                email: dialog.getModalContent().find('input[name="email"]').val(),
                                phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                website: dialog.getModalContent().find('input[name="website"]').val(),
                                adress_street: dialog.getModalContent().find('input[name="adress_street"]').val(),
                                adress_street_number: dialog.getModalContent().find('input[name="adress_street_number"]').val(),
                                adress_postal: dialog.getModalContent().find('input[name="adress_postal"]').val(),
                                adress_city: dialog.getModalContent().find('input[name="adress_city"]').val(),
                                adress_state: dialog.getModalContent().find('input[name="adress_state"]').val(),
                                adress_country: dialog.getModalContent().find('input[name="adress_country"]').val(),
                                coc_number: dialog.getModalContent().find('input[name="coc_number"]').val(),
                                vat_number: dialog.getModalContent().find('input[name="vat_number"]').val(),
                                bankaccount_number: dialog.getModalContent().find('input[name="bankaccount_number"]').val()
                            },
                            success: function (data) {
                                dialog.close();

                                $('#content').html(data);

                            },
                            error: function (error) {
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                dialog.getButton('submit').stopSpin();

                                if (error.responseJSON.error != null) {
                                    $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                    dialog.close();
                                } else {
                                    dialog.setMessage(error.responseJSON.html);
                                }

                            }
                        });

                    }
                }]
            });
        });
});