<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <link rel="stylesheet" href="../public/css/template.css">

    <style></style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="../public/images/logo.png" />
    </div>
    <h1>INVOICE</h1>
    <div id="company" class="clearfix">
        <div>{{$company->name}}</div>
        <div>{{$company->street}} {{$company->street_number}},<br /> {{$company->postalcode}},{{$company->city}},{{$company->country}}</div>
        <div>{{$company->phone}}</div>
        <div>{{$company->email}}</div>
    </div>
    <div id="project">
        <div><span>PROJECT</span> {{$project->name}}</div>
        <div><span>CLIENT</span> {{ $project->contactPerson->name }}</div>
        <div><span>INVOICE #</span> {{ $billingInvoice->id }}</div>
        <div><span>ADDRESS</span> {{$organisation->street}} {{$organisation->street_number}} {{$organisation->city}} {{$organisation->postalcode}} {{$organisation->country}}</div>
        <div><span>EMAIL</span> {{$organisation->email}}</div>
        <div><span>DATE</span> {{$billingInvoice->created_at}}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="desc">DESCRIPTION</th>
            <th>PRICE</th>
            <th>HOUR</th>
            <th>VAT</th>
            <th>PRICE EXCL</th>
            <th>PRICE INCL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($billingInvoiceRows as $iRows)

            <tr>
                <td class="desc">{{$iRows->description}}</td>
                <td class="unit">{{$iRows->price_hour}}</td>
                <td class="unit">{{$iRows->hours}}</td>
                <td class="unit">{{$iRows->vat}}</td>
                <td class="unit">€ {{$iRows->total_price_excl}}</td>
                <td class="unit">€ {{$iRows->total_price_incl}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" class="grand total">TOTAL PRICE</td>
            <td class="grand total">€ {{$totalPrice}}</td>
        </tr>
        </tbody>
    </table>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
    </div>
</main>
<footer>
    This invoice was generated via the YTBN app and is valid without signature and seal.
</footer>
</body>
</html>