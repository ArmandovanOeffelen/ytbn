@foreach((Request::input('tableArray') ?? $invoiceRows ?? $quoteRows ?? [[]] ) as $index => $array)
       <tr id="form-row-{{$index}}" class="table-form-row">
        <td>
            <div id="service-test" class="form-group service-select-quoty {{$errors->has($index.'.service') ? 'has-error': '' }} ">
                <label class="control-label checkbox">
                    <select id="service-field" class="form-control testt js-service-select service-select-quoty" onchange="calculatePrices($(this).closest('tr'))" name="service[{{$index}}]">
                        @foreach($services as $serviceindex => $service)
                                @if(isset($array['billing_service_id']))
                                    @if($service->id === $array['billing_service_id'])
                                        <option value="{{$service->id}}" data-data="{{$service}}" selected>{{$service->name}}</option>
                                    @endif
                                @endif
                            <option value="{{$service->id}}" data-data="{{$service}}">{{$service->name}}</option>
                        @endforeach
                    </select>
                </label>
                @if ($errors->has($index.'.service'))
                    <span class="help-block">{{ $errors->first($index.'.service') }}</span>
                @endif
            </div>
        </td>
        <td>
            <div class="form-group {{$errors->has($index.'.description') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="description-field" class="description-field form-control testt" name="description[{{$index}}]" type="text" value="{{ $array['description'] ?? ''}}">
                </label>
                    @if ($errors->has($index.'.description'))
                        <span class="help-block">{{ $errors->first($index.'.description') }}</span>
                    @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.price') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="price-field" class="price-field form-control testt" name="price[{{$index}}]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="{{ $array['price'] ?? '' }}">
                </label>
                @if ($errors->has($index.'.price'))
                    <span class="help-block">{{ $errors->first($index.'.price') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.hour') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="time-field" class="time-field form-control" name="hour[{{$index}}]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="{{$array['hour'] ?? ''}}">
                </label>
                @if ($errors->has($index.'.hour'))
                    <span class="help-block">{{ $errors->first($index.'.hour') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.discount') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="discount-field" class="discount-field form-control" onchange="calculatePrices($(this).closest('tr'))" name="discount[{{$index}}]" type="text" value="{{ $array['discount'] ?? '0'}}">
                    <input id="hidden-field" class="hidden-field form-control" type="hidden" value="{{$array['id'] ?? ''}}">
                </label>
                @if ($errors->has($index.'.discount'))
                    <span class="help-block">{{ $errors->first($index.'.discount') }}</span>
                @endif
            </div>
        </td>
        {{--TODO::add € when validation fails--}}
        <td id="vat-percentage-field" class="total-price price-total ">{{ $array['vat'] ?? '0 %'}}</td>
        <td id="total-price-excl-field" class="total-price price-total ">{{$array['total_price_excl'] ?? '€ 0.00'}}</td>
        <td id="vat-difference-field" class="total-price price-total ">{{$array['vat_diff'] ??'€ 0.00'}}</td>
        <td id="total-price-field" class="total-price price-total ">{{$array['total_price_incl'] ??'€ 0.00'}}</td>
        {{--TODO:: Make total-price a global class--}}
        <td class="total-price "><button id="delete-row-{{$index}}" type="button" class="delete-row btn btn-danger pull-right"><i class="fa fa-trash-alt" aria-hidden="true"></i></button></td>
    </tr>
@endforeach
<template id="template_row">
    <tr id="form-row-_index_" class="table-form-row">
        <td>
            <div id="service-test" class="form-group service-select-quoty ">
                <label class="control-label checkbox">
                    <select id="service-field" class="form-control testt js-service-select service-select-quoty" onchange="calculatePrices($(this).closest('tr'))" name="service[_index_]">
                        @foreach($services as $serviceindex => $service)
                            <option value="{{$service->id}}" data-data="{{$service}}">{{$service->name}}</option>
                        @endforeach
                    </select>
                </label>
            </div>
        </td>
        <td>
            <div class="form-group">
                <label class="control-label checkbox">
                    <input id="description-field" class="description-field form-control testt" name="description[_index_]" type="text" value="">
                </label>
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group">
                <label class="control-label checkbox">
                    <input id="price-field" class="price-field form-control testt" name="price[_index_]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="">
                </label>
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group">
                <label class="control-label checkbox">
                    <input id="time-field" class="time-field form-control" name="hour[_index_]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="">
                </label>
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group">
                <label class="control-label checkbox">
                    <input id="discount-field" class="discount-field form-control" onchange="calculatePrices($(this).closest('tr'))" name="discount[_index_]" type="text" value="0">
                    <input id="hidden-field" class="hidden-field form-control" type="hidden" value="">
                </label>
            </div>
        </td>
        {{--TODO::add € when validation fails--}}
        <td id="vat-percentage-field" class="total-price price-total ">0%</td>
        <td id="total-price-excl-field" class="total-price price-total ">€ 0.00</td>
        <td id="vat-difference-field" class="total-price price-total ">€ 0.00</td>
        <td id="total-price-field" class="total-price price-total ">€ 0.00</td>
        {{--TODO:: Make total-price a global class--}}
        <td class="total-price "><button id="delete-row-_index_" type="button" class="delete-row btn btn-danger pull-right"><i class="fa fa-trash-alt" aria-hidden="true"></i></button></td>
    </tr>
</template>