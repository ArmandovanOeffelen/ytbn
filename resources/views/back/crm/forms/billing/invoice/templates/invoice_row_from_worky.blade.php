
@foreach((Request::input('tableArray') ?? $invoiceRows ?? [[]] ) as $index => $array)

    <tr id="form-row-{{$index}}" class="table-form-row">
        <td>
            <div class="form-group {{$errors->has($index.'.description') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="description-field" class="description-field form-control testt" name="description[{{$index}}]" type="text" value="{{ $array['description'] ?? ''}}">
                </label>
                @if ($errors->has($index.'.description'))
                    <span class="help-block">{{ $errors->first($index.'.description') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.price') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="price-field" class="price-field form-control testt" name="price[{{$index}}]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="{{ $array['price'] ?? '' }}">
                </label>
                @if ($errors->has($index.'.price'))
                    <span class="help-block">{{ $errors->first($index.'.price') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.hour') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="time-field" class="time-field form-control" name="hour[{{$index}}]" onchange="calculatePrices($(this).closest('tr'))" type="text" value="{{$array['hour'] ?? ''}}">
                </label>
                @if ($errors->has($index.'.hour'))
                    <span class="help-block">{{ $errors->first($index.'.hour') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.discount') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <input id="discount-field" class="discount-field form-control" onchange="calculatePrices($(this).closest('tr'))" name="discount[{{$index}}]" type="text" value="{{ $array['discount'] ?? '0'}}">
                    <input id="hidden-field" class="hidden-field form-control" type="hidden" value="{{$array['id'] ?? ''}}">
                </label>
                @if ($errors->has($index.'.discount'))
                    <span class="help-block">{{ $errors->first($index.'.discount') }}</span>
                @endif
            </div>
        </td>
        <td class="vat-field">
            <div class="form-group {{$errors->has($index.'.vat') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <select id="vat-field" class="vat-field form-control testt" onchange="calculatePrices($(this).closest('tr'))" name="vat[{{$index}}]">
                        @foreach($allVat as $vat)
                            <option value="{{$vat->id}}">{{$vat->vat}}</option>
                        @endforeach
                    </select>
                </label>
                @if ($errors->has($index.'.vat'))
                    <span class="help-block">{{ $errors->first($index.'.vat') }}</span>
                @endif
            </div>
        </td>
        {{--TODO::add € when validation fails--}}
        <td id="total-price-excl-field" class="total-price price-total ">{{$array['total_price_excl'] ?? '€ 0.00'}}</td>
        <td id="vat-difference-field" class="total-price price-total ">{{$array['vat_diff'] ??'€ 0.00'}}</td>
        <td id="total-price-field" class="total-price price-total ">{{$array['total_price_incl'] ??'€ 0.00'}}</td>
        {{--TODO:: Make total-price a global class--}}
        <td class="total-price "><button id="delete-row-{{$index}}" type="button" class="delete-row btn btn-danger pull-right"><i class="fa fa-trash-alt" aria-hidden="true"></i></button></td>
    </tr>
@endforeach