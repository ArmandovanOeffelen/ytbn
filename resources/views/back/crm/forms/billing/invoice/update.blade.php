<div class="row">
    <div class="col-md-5">
        <h4>Invoicy details</h4>
    </div>
    <div class="col-md-7">
        <div class="pull-right">
            <btn id="btn-decrease" class="btn-increase btn btn-class btn-primary"><i class="fa fa-minus" aria-hidden="true"></i></btn>
            <span class="text-Increase">1</span>
            <btn id="btn-increase" class="btn-increase btn btn-class btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></btn>
            <btn id="btn-addrow" class="btn-increase btn btn-class btn-primary">Add row(s)</btn>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-row col-md-12">
        <table id="table-invoice" class="table table-responsive">
            <thead>
            <tr>
                <td>Description</td>
                <td>Price</td>
                <td>Time</td>
                <td>discount %</td>
                <td>Vat %</td>
                <td>excl. vat</td>
                <td>vat</td>
                <td>incl. vat</td>
                <td></td>
            </tr>
            </thead>
            <tbody id="table-body">
            @include('back.crm.forms.billing.invoice.templates.invoice_row')
            </tbody>
        </table>
    </div>
</div>