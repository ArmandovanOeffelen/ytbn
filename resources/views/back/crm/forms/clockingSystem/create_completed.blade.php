<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Worky details</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group service-selectize {{$errors->has('service') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Select service</span>
                <select class="js-service-select form-control services-select" name="service">
                    @foreach($services as $service)
                        <option value="{{$service->id}}" data-data="{{$service}}">{{$service->name}}</option>
                    @endforeach
                </select>
                <input id="vat-percentage-field" class="form-control vat_hidden" type="hidden" name="vat"/>
                <input class="form-control service_hidden" type="hidden" name="service_id"/>
            </label>
            @if ($errors->has('service'))
                <span class="help-block">{{ $errors->first('service') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('job') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Job description</span>
                <textarea class="form-control" name="job" value="{{ Request::input('job')}}"></textarea>
            </label>
            @if ($errors->has('job'))
                <span class="help-block">{{ $errors->first('job') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group {{$errors->has('price') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Price</span>
                <input class="form-control" name="price" type="text" value="{{ Request::input('price')}}">
            </label>
            @if ($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
            <div class="form-group{{$errors->has('vat') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Vat</span><br />
                    <span id="shown-vat-percentage-field">testt</span>
                </label>
                @if ($errors->has('vat'))
                    <span class="help-block">{{ $errors->first('vat') }}</span>
                @endif
            </div>
    </div>
    <div class="col-md-3">

    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group {{$errors->has('hours') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Time hours </span>
                <input id="timeField" class="form-control" name="hours" type="text" value="{{ Request::input('hours')}}">
            </label>
            @if ($errors->has('hours'))
                <span class="help-block">{{ $errors->first('hours') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group {{$errors->has('minutes') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Time minutes</span>
                <select class="form-control testt" name="minutes">
                    <option value="0">0</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                </select>
            </label>
            @if ($errors->has('minutes'))
                <span class="help-block">{{ $errors->first('minutes') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
