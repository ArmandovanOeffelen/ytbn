<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Worky details</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-8">
        <div class="form-group service-selectize {{$errors->has('service') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Select service</span>
                <select class="js-service-select form-control services-select" name="service">
                    @foreach($services as $service)
                        <option value="{{$service->id}}" data-data="{{$service}}">{{$service->name}}</option>
                    @endforeach
                </select>
                <input class="form-control vat_hidden" type="hidden" name="vat"/>
                <input class="form-control service_hidden" type="hidden" name="service_id"/>
            </label>
            @if ($errors->has('service'))
                <span class="help-block">{{ $errors->first('service') }}</span>
            @endif
        </div>

    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label checkbox">
                <span>Vat %</span>
                <span class="vat-percentage"></span>
            </label>

        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('job') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Job description</span>
                <textarea class="form-control" name="job" value="{{ Request::input('job')}}"></textarea>
            </label>
            @if ($errors->has('job'))
                <span class="help-block">{{ $errors->first('job') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group {{$errors->has('price') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Price</span>
                <input class="form-control" name="price" type="text" value="{{ Request::input('price')}}">
            </label>
            @if ($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>

