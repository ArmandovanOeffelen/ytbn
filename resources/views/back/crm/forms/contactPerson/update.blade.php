<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Details Contact Person</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Company name</span>
                <input class="form-control" name="name" type="text" value="{{ Request::input('name') ?: $contactPerson->name}}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group {{$errors->has('email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Email</span>
                <input class="form-control" name="email" type="email" value="{{ Request::input('email') ?: $contactPerson->email }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('phone') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Phone number</span>
                <input class="form-control" name="phone" type="text" value="{{ Request::input('phone') ?: $contactPerson->phone }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('phone'))
                <span class="help-block">{{ $errors->first('phone') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>