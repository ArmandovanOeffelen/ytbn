<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Organisation details</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Organisation name</span>
                <input class="form-control" name="name" type="text" value="{{ Request::input('name') }}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group {{$errors->has('email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Email</span>
                <input class="form-control" name="email" type="email" value="{{ Request::input('email') }}">
            </label>
            @if ($errors->has('email'))
                <span class="help-block">{{ $errors->first('email') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('website') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Website</span>
                <input class="form-control" name="website" type="text" value="{{ Request::input('website') }}">
            </label>
            @if ($errors->has('website'))
                <span class="help-block">{{ $errors->first('website') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('phone') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Phone number</span>
                <input class="form-control" name="phone" type="text" value="{{ Request::input('phone') }}">
            </label>
            @if ($errors->has('phone'))
                <span class="help-block">{{ $errors->first('phone') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Contact details</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-8">
        <div class="form-group{{$errors->has('adress_street') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Street</span>
                <input class="form-control" name="adress_street" type="text" value="{{ Request::input('adress_street') }}">
            </label>
            @if ($errors->has('adress_street'))
                <span class="help-block">{{ $errors->first('adress_street') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{$errors->has('adress_street_number') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>number</span>
                <input class="form-control" name="adress_street_number" type="text" value="{{ Request::input('adress_street_number') }}">
            </label>
            @if ($errors->has('adress_street'))
                <span class="help-block">{{ $errors->first('adress_street_number') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <div class="form-group{{$errors->has('adress_postal') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Postalcode</span>
                <input class="form-control" name="adress_postal" type="text" value="{{ Request::input('adress_postal') }}">
            </label>
            @if ($errors->has('adress_postal'))
                <span class="help-block">{{ $errors->first('adress_postal') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{$errors->has('adress_city') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>City</span>
                <input class="form-control" name="adress_city" type="text" value="{{ Request::input('adress_city') }}">
            </label>
            @if ($errors->has('adress_city'))
                <span class="help-block">{{ $errors->first('adress_city') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('adress_state') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>State</span>
                <input class="form-control" name="adress_state" type="text" value="{{ Request::input('adress_state') }}">
            </label>
            @if ($errors->has('adress_state'))
                <span class="help-block">{{ $errors->first('adress_state') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('adress_country') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Country</span>
                <input class="form-control" name="adress_country" type="text" value="{{ Request::input('adress_country') }}">
            </label>
            @if ($errors->has('adress_country'))
                <span class="help-block">{{ $errors->first('adress_country') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>



