<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Details organisation</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Project Name</span>
                <input class="form-control" name="name" type="text"
                       value="{{ Request::input('name') }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('project_manager') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Select project manager</span>
                <select class="js-project-manager-select form-control" name="project_manager">
                    @foreach($projectManagers as $projectManager)
                        <option value="{{$projectManager->id}}">{{$projectManager->name}}</option>
                    @endforeach
                </select>
            </label>
            @if ($errors->has('project_manager'))
                <span class="help-block">{{ $errors->first('project_manager') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('start_date') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Start date</span>
                <input class="form-control" name="start_date" type="date" value="{{ Request::input('start_date') }}">
            </label>
            @if ($errors->has('start_date'))
                <span class="help-block">{{ $errors->first('start_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group{{$errors->has('end_date') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>End date</span>
                <input class="form-control" name="end_date" type="date" value="{{ Request::input('end_date') }}">
            </label>
            @if ($errors->has('end_date'))
                <span class="help-block">{{ $errors->first('end_date') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('organisation') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Select organisation</span>
                <select class="js-organisation-select form-control" name="organisation">
                    @foreach($organisation as $organisations)
                        <option value="{{$organisations->id}}">{{$organisations->name}}</option>
                    @endforeach
                </select>
            </label>
            @if ($errors->has('organisation'))
                <span class="help-block">{{ $errors->first('organisation') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('project_status') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Select project status</span>
                <select class="js-project-status-select form-control" name="project_status">
                    @foreach($projectStatus as $status)
                        <option value="{{$status->id}}">{{$status->status_type}}</option>
                    @endforeach
                </select>
            </label>
            @if ($errors->has('project_status'))
                <span class="help-block">{{ $errors->first('project_status') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Details contact person</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('contactperson_name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>naam</span>
                <input class="form-control" name="contactperson_name" type="text"
                       value="{{ Request::input('contactperson_name') }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('contactperson_name'))
                <span class="help-block">{{ $errors->first('contactperson_name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('contactperson_email') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>email</span>
                <input class="form-control" name="contactperson_email" type="text"
                       value="{{ Request::input('contactperson_email') }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('contactperson_email'))
                <span class="help-block">{{ $errors->first('contactperson_email') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('contactperson_phone') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>telefoon</span>
                <input class="form-control" name="contactperson_phone" type="text"
                       value="{{ Request::input('contactperson_phone') }}">
                <small>This field needs to contain atleast 3 characters</small>
            </label>
            @if ($errors->has('contactperson_phone'))
                <span class="help-block">{{ $errors->first('contactperson_phone') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Summary of project</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('project_summary') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span></span>
                <textarea class="form-control" name="project_summary" value="{{ Request::input('project_summary') }}"></textarea>
                <small>Een opmerking is optioneel</small>
            </label>
            @if ($errors->has('project_summary'))
                <span class="help-block">{{ $errors->first('project_summary') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>