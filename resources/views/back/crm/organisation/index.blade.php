@extends('back.layouts.layout')

@section('title', 'Organisations')

@section('content')
    @include('back.crm.organisation.templates.index')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                createProject: `@include('back.crm.forms.project.createForOrganisation')`,
                delete: '<span>Weet u zeker dat u werkplek <strong>{0}</strong> wilt verwijderen?</span>',
                loading: '<span>Loading...</span>'
            };

            $('#content')
                //Create project for Organisation
                .on('click', '.buttonCreateProject', function() {
                    var objectData = $('.buttonCreateProject').data('data');
                    BootstrapDialog.show({
                        title: 'create project',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/crm/organisation/{0}/show-content-for-form') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        onshown: function(dialog) {
                            dialog.$modal.find('.js-project-status-select').selectize({});
                            dialog.$modal.find('.js-project-manager-select').selectize({});
                        },
                        buttons: [{
                            label: 'Cancel',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('admin/ajax/template/crm/organisation/{0}/create-project')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        project_manager: dialog.getModalContent().find('select[name="project_manager"]').val(),
                                        project_status: dialog.getModalContent().find('select[name="project_status"]').val(),
                                        start_date: dialog.getModalContent().find('input[name="start_date"]').val(),
                                        end_date: dialog.getModalContent().find('input[name="end_date"]').val(),
                                        project_summary: dialog.getModalContent().find('textarea[name="project_summary"]').val(),
                                        //contactperson
                                        contactperson_name:dialog.getModalContent().find('input[name="contactperson_name"]').val(),
                                        contactperson_phone:dialog.getModalContent().find('input[name="contactperson_phone"]').val(),
                                        contactperson_email:dialog.getModalContent().find('input[name="contactperson_email"]').val(),
                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                /* Start Organisation Worky */
                .on('click', '.btn-startWorky', function () {
                    var objectData = $(this).data('organisation');
                    var objectData1 = $(this).data('worky');
                    console.log(objectData);
                    $.ajax({
                        type: 'post',
                        url: '{{ url('admin/ajax/template/crm/organisation/{0}/create-project')}}'.format(objectData1.id, objectData.id),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function (data) {
                            $('#content').html(data);
                        },
                        error: function (error) {
                            if (error.responseJSON.error != null) {
                                $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                            }
                        }
                    });
                })
            /* Update */
                .on('click', '#buttonUpdate', function() {
                    var objectData = $(this).data('data');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'prospect bewerken',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/crm/organisation/{0}') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'Cancel',
                            cssClass:'btn-danger',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function(dialog){
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('/admin/ajax/template/crm/organisation/{0}')}}'
                                        .format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        website: dialog.getModalContent().find('input[name="website"]').val(),
                                        phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),
                                        adress_street: dialog.getModalContent().find('input[name="adress_street"]').val(),
                                        adress_street_number: dialog.getModalContent().find('input[name="adress_street_number"]').val(),
                                        adress_postal: dialog.getModalContent().find('input[name="adress_postal"]').val(),
                                        adress_city: dialog.getModalContent().find('input[name="adress_city"]').val(),
                                        adress_state: dialog.getModalContent().find('input[name="adress_state"]').val(),
                                        adress_country: dialog.getModalContent().find('input[name="adress_country"]').val(),
                                    },
                                    success: function(data){
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                /* Delete */
                .on('click', '.buttonDelete', function() {
                    var objectData = $(this).closest('tr').data('data');
                    console.log(objectData);
                    BootstrapDialog.show({
                        title: 'Prospect verwijderen',
                        message: templates.delete.format(objectData.customer_name),
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'Verwijderen',
                            label: 'Verwijderen',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function(dialog){
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('/admin/ajax/template/crm/organisation/{0}')}}'
                                        .format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}'
                                    },
                                    success: function(data){
                                        dialog.close();

                                        $('#content').html(data.html);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }

                                    }
                                });

                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
