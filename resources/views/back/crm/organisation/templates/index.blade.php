@include('partials.flash-message')
<div class="container-fluid">
    <ul class="project-nav nav nav-tabs nav-justified">
        <li class="nav-item active"><a href="#organisation-block" role="tab" data-toggle="tab">Organisation</a></li>
        <li class="nav-item"><a href="#project-block"  role="tab" data-toggle="tab">Latest projects</a></li>
        <li class="nav-item"><a href="#worky-block"  role="tab" data-toggle="tab">Latest workies</a></li>
        <li class="nav-item"><a href="#worky-organisation-block"  role="tab" data-toggle="tab">Organisation workies</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="organisation-block" class="tab-pane active">
            @include('back.crm.organisation.templates.organisation_index')
        </div>
        <div role="tabpanel" id="project-block" class="tab-pane">
            @include('back.crm.organisation.templates.project_overview')
        </div>
        <div role="tabpanel" id="worky-block" class="tab-pane">
            @include('back.crm.organisation.templates.worky_overview')
        </div>
        <div role="tabpanel" id="worky-organisation-block" class="tab-pane">
            @include('back.crm.organisation.templates.worky_organisation')
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <button type="button" class="buttonDelete btn btn-danger pull-right"><i class="fa fa-trash-alt" aria-hidden="true"></i> Delete organisation</button>
                </div>
            </div>
        </div>
    </div>
</div>