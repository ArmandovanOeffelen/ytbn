<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Overview - {{$organisation->name}}</h2>
        </div>
        <div class="pull-right" style="padding-top: 25px;">
            <button id="buttonUpdate" type="button" class="btn btn-primary buttonUpdate" data-data="{{ $organisation }}" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit organisation</button>
            <button id="buttonCreateProject" type="button" class="btn btn-primary buttonCreateProject " data-data="{{ $organisation }}"><i class="fa fa-plus" aria-hidden="true"></i> Add project</button>
        </div>
    </div>
</div>
<div class="row">
    <h3>Details Organisation</h3>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Organisation Name</td>
                <td>{{$organisation->name}}</td>
            </tr>
            <tr>
                <td>Organisation's email</td>
                <td>{{$organisation->email}}</td>
            </tr>
            <tr>
                <td>Organisation's website</td>
                <td><a href="{{$organisation->website}}">{{$organisation->website}}</a></td>
            </tr>
            <tr>
                <td>Organisation's phone</td>
                <td>{{$organisation->phone}}</td>
            </tr>
            <tr>
                <td>Organisation's adress</td>
                <td>{{$organisation->street}} {{$organisation->street_number}}</td>
            </tr>
            <tr>
                <td>Organisation's postal code</td>
                <td>{{$organisation->postalcode}}</td>
            </tr>
            <tr>
                <td>Organisation's state</td>
                <td>{{$organisation->state}}</td>
            </tr>
            <tr>
                <td>Organisation's city</td>
                <td>{{$organisation->city}}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>