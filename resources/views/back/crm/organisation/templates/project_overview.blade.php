<div class="row">
    <div class="col-lg-12">
        <h3>Latest Projects</h3>
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td>Project name</td>
                <td>Project Summary</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($project as $projects)
                <tr>
                    <td>{{$projects->name}}</td>
                    <td>{{$projects->summary}}</td>
                    <td><a href="{{ url('/back/crm/project/'.$projects->id) }}" class="btn btn-primary pull-right"><i class="fa fa-eye" aria-hidden="true"></i> Check project</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>