<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Overview - {{$organisation->name}}</h2>
        </div>
        <div class="pull-right" style="padding-top: 25px;">
            <button id="buttonCreateProject" type="button" class="btn btn-primary buttonCreateProject " data-data="{{ $organisation }}"><i class="fa fa-money-bill-wave" aria-hidden="true"></i> Make invoicy from workies</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3>Latest Workies</h3>
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td>Worky name</td>
                <td>worked time (Hours)</td>
                <td>total price</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($organisationWorkies as $ogWorky)
                <tr>
                    <td>{{$ogWorky->name}}</td>
                    <td> 0</td>
                    <td>
                        <button type="button" class="btn-worky completeWorky btn btn-success pull-right"
                                data-organisation="{{ $organisation }}" data-worky="{{ $ogWorky }}">
                            <i class="fa fa-check"aria-hidden="true"></i>
                            Complete Worky
                        </button>

                        <button type="button" class="btn-startWorky btn-worky btn btn-success pull-right"
                                data-organisation="{{ $organisation }}" data-worky="{{ $ogWorky }}">
                            <i class="fa fa-play"aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>