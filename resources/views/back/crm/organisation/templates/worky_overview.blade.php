<div class="row">
    <div class="col-lg-12">
        <h3>Latest Workies</h3>
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td>summary</td>
                <td>worked time (Hours)</td>
                <td>total price</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($worky as $workies)
                <tr>
                    <td>{{$workies->job}}</td>
                    <td>{{$workies->total_time/3600}}</td>
                    <td>{{$workies->total_price}}</td>
                    <td><a href="{{ url('/back/crm/project/'.$workies->project_id) }}" class="btn btn-primary pull-right"><i class="fa fa-eye" aria-hidden="true"></i> Check project</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>