@extends('back.layouts.layout')

@section('content')
    @include('back.crm.project.templates.index')
@endsection

@section('templates')
    <template id="template_createWorky">
        @include('back.crm.forms.clockingSystem.start')
    </template>
    <template id="template_createInvoicy">
        @include('back.crm.forms.billing.invoice.create')
    </template>
    <template id="template_updateInvoicy">
        @include('back.crm.forms.billing.invoice.update')
    </template>
    <template id="template_updateQuoty">
        @include('back.crm.forms.billing.quote.update')
    </template>
    <template id="template_createQuoty">
        @include('back.crm.forms.billing.quote.create')
    </template>
    <template id="template_deleteWorky">
        <span>Are you sure that you want to delete this  <strong>{0}</strong> worky?</span>
    </template>
    <template id="template_loading">
        <span>Loading...</span>
    </template>
@endsection

@section('scripts')
    @parent

    <script>
        var variables = {
            csrfToken: '{{ csrf_token() }}',
            createQuotyUrl: '{{ url('admin/ajax/template/crm/project/{0}/billing/{1}/create-quote')}}',
            loadAllWorkysUrl: '{{ url('admin/ajax/template/crm/project/{0}/Billing/{1}/invoice/load-all-worky')}}',
            CreateInvoicyFromWorky: '{{ url('admin/ajax/template/crm/project/{0}/Billing/{1}/invoice/create-invoicy-from-worky')}}',
            updateQuotyUrl: '{{ url('admin/ajax/template/crm/project/{0}/Billing/{1}/quoty/{2}/update-quoty')}}',
            showQuotyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/quoty/{2}/show-quoty')}}',
            generateInvoice: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/invoice/{2}/generate-invoice')}}',
            generateQuoty: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/quoty/{2}/generate-quote')}}',
            downloadInvoice: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/invoice/{2}/download')}}',
            createInvoicyUrl: '{{ url('admin/ajax/template/crm/project/{0}/createinvoice')}}',
            updateInvoicyUrl: '{{ url('admin/ajax/template/crm/project/{0}/Billing/{1}/invoicy/{2}/update-invoicy')}}',
            showInvoicyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/invoicy/{2}/show-invoicy')}}',
            pauseWorkyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/pauseworky/{1}')}}',
            unPauseWorkyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/unpauseworky/{1}')}}',
            completeWorkyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/completeworky/{1}')}}',
            restartWorkyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/restartworky/{1}')}}',
            addWorkyUrl: '{{ url('admin/ajax/template/crm/project/{0}/')}}',
            loadCompletedWorkyUrl: '{{ url('admin/ajax/template/crm/project/{0}/showForm')}}',
            loadWorkyForm: '{{ url('admin/ajax/template/crm/project/{0}/showFormForNewWorky')}}',
            addCompletedWorkyUrl: '{{ url('admin/ajax/template/crm/project/{0}/add-completed-worky')}}',
            updateProjectTemplateUrl: '{{ url('/admin/ajax/template/crm/project/{0}') }}',
            updateProjectUrl: '{{ url('/admin/ajax/template/crm/project/{0}')}}',
            editContactPersonTemplateUrl: '{{ url('/admin/ajax/template/crm/project/contactperson/{0}') }}',
            editContactPersonUrl: '{{ url('/admin/ajax/template/crm/project/{0}/contactperson/{1}') }}',
            deleteWorkyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/deleteworky/{1}')}}',
            deleteOrganisationUrl: '{{ url('/admin/ajax/template/crm/organisation/{0}')}}',
        };

    </script>
    <script src="{{ asset('js/pages/back/project/index.js') }}"></script>
@endsection
