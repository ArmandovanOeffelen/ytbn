@include('partials.flash-message')
<div class="container-fluid">
    <ul class="project-nav nav nav-tabs nav-justified">
        <li class="nav-item active"><a href="#project-block" role="tab" data-toggle="tab">Project</a></li>
        <li class="nav-item"><a href="#worky-running-block"  role="tab" data-toggle="tab">Worky - Running</a></li>
        <li class="nav-item"><a href="#worky-completed-block"  role="tab" data-toggle="tab">Worky - Completed</a></li>
        <li class="nav-item"><a href="#quoty-quoty-block" data-toggle="tab">Quoty - Quoty</a></li>
        <li class="nav-item"><a href="#quoty-invoicy-block" data-toggle="tab">Quoty - Invoicy</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="project-block" class="tab-pane active">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h2>Project : {{$project->name}} - {{$project->organisation->name}} </h2>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-primary buttonUpdate" data-data="{{$project}}">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit project
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                @include('back.crm.project.templates.project.project_intro')
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <h3>Contact person</h3>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="buttonUpdateContactPerson btn btn-primary pull-right"
                                data-data="{{ $project->contactPerson }}" data-project="{{ $project }}">
                            <i class="fa fa-pencil-square-o"aria-hidden="true"></i>
                            Edit contactperson
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                @include('back.crm.project.templates.project.contact_person')
            </div>
        </div>
        <div role="tabpanel" id="worky-running-block" class="tab-pane">
            @include('back.crm.project.templates.worky.table_uncompleted')
        </div>
        <div role="tabpanel" id="worky-completed-block" class="tab-pane">
            @include('back.crm.project.templates.worky.table_completed')
        </div>
        <div role="tabpanel" id="quoty-quoty-block" class="tab-pane">
            @include('back.crm.project.templates.quoty.quoty_template')
        </div>
        <div role="tabpanel" id="quoty-invoicy-block" class="tab-pane">
            @include('back.crm.project.templates.invoicy.invoicy_template')
        </div>


        <div class="row">
            <div class="col-lg-10">
            </div>
            <div class="col-lg-1">
                <button type="button" class="buttonDelete btn btn-danger pull-right">
                    <i class="fa fa-trash-alt" aria-hidden="true"></i>
                    Delete Project
                </button>
            </div>
            <div class="col-lg-1">
                <button type="button" class="buttonDelete btn btn-success pull-right">
                    <i class="fa fa-check"aria-hidden="true"></i>
                    FinishProject
                </button>
            </div>
        </div>
    </div>
</div>