<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <td>
            Date
        </td>

        <td>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($billingInvoiceGenerated as $index=>$invoiceGenerated)
    <tr>

        <td>
            {{$invoiceGenerated->billingStorage->name}}
        </td>
        <td class="pull-right">
            {{--TODO:: Fix if pdf is already send that you cant send another one.--}}
            <a type="button"
               href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/invoice/'.$invoiceGenerated->id.'/download')}}"
               class=" btn btn-primary pull-right"><i class="fa fa-file-download" aria-hidden="true"></i> Download
                Invoicy</a>
            <a type="button"
               href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/invoice/'.$invoiceGenerated->id.'/send-invoice')}}"
               class=" btn btn-success pull-right"><i class="fa fa-envelope" aria-hidden="true"></i> Send Invoicy</a>

        </td>
    </tr>
    @endforeach
    </tbody>
</table>
