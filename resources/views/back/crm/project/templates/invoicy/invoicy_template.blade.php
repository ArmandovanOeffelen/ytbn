<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Quoty - Invoicy</h2>
        </div>
        <div class="pull-right">
            <button type="button" class="createInvoicyFromWorky btn btn-primary"
                    data-project="{{ $project }}" data-billing="{{ $billing }}">
                <i class="fa fa-money-bill-wave" aria-hidden="true"></i>
                Create Invoicy from Worky's
            </button>
            <button type="button" class="btn-worky createinvoicy btn btn-primary"
                    data-project="{{ $project }}"><i class="fa fa-money-bill-wave" aria-hidden="true"></i>
                Create
                Invoicy
            </button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <h4><i>Un generated Invoicy's</i></h4>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        <h4><i>Generated Invoicy's</i></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        @include('back.crm.project.templates.invoicy.ungenerated')
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        @include('back.crm.project.templates.invoicy.generated')
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right all-button">
            <a href="{{ url('/back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quoty-overview') }}" class="btn btn-primary"> See all Organisations</a>
        </div>
    </div>
</div>