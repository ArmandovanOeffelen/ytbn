<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <td>Date</td>
        <td></td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($billingInvoiceNotGenerated as $index=>$invoiceUnGenerated)
        <tr>
            <td>
                {{$invoiceUnGenerated->created_at}}
            </td>
            <td>
                <button type="button" class="btn-edit-invoicy btn btn-primary pull-right" data-project="{{ $project }}"
                        data-invoice="{{ $invoiceUnGenerated }}" data-billing="{{$billing}}"> Edit Invoice
                </button>
                <button type="button" class="btn-generate-invoice btn btn-success pull-right"
                        data-project="{{ $project }}" data-billing="{{ $billing }}"
                        data-invoice="{{$invoiceUnGenerated}}"> Generate PDF
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>