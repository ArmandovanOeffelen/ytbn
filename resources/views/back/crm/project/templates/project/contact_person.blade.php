<div class="col-lg-12">
    <table class="overViewTable table table-striped">
        <thead>
        <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Phone</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$project->contactPerson->name}}</td>
            <td>{{$project->contactPerson->email}}</td>
            <td>{{$project->contactPerson->phone}}</td>
        </tr>
        </tbody>
    </table>
</div>
