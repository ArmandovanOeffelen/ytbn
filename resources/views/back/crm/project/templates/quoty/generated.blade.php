<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <td>Name</td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($billingQuoteGenerated as $index=>$billingQuoteGeneratedx)
        <tr>
            <td>
                {{$billingQuoteGeneratedx->created_at}}
            </td>
            <td>
                <a type="button"
                   href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quote/'.$billingQuoteGeneratedx->id.'/download')}}"
                   class=" btn btn-primary pull-right"><i class="fa fa-file-download" aria-hidden="true"></i> Download
                    Quoty</a>
                <a type="button"
                   href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quote/'.$billingQuoteGeneratedx->id.'/send-quote')}}"
                   class=" btn btn-success pull-right"><i class="fa fa-envelope" aria-hidden="true"></i> Send Quoty</a>
            </td>
        </tr>

    @endforeach
    </tbody>
</table>