<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Quoty - Quoty</h2>
        </div>
        <div class="pull-right">
            <button type="button" class="createQuoty btn btn-primary pull-right" data-project="{{ $project }}"
                    data-billing="{{$billing}}">
                <i class="fa fa-money-bill-wave" aria-hidden="true"></i>
                Create Quoty
            </button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <h4><i>Un generated Quoty's</i></h4>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        <h4><i>Generated Quoty's</i></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        @include('back.crm.project.templates.quoty.ungenerated')
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        @include('back.crm.project.templates.quoty.generated')
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right all-button">
            <a href="{{ url('/back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quoty-overview') }}" class="btn btn-primary"> View all Quoties</a>

        </div>
    </div>
</div>
