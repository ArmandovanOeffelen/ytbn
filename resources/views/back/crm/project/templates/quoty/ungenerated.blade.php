<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <td>Name</td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($billingQuoteNotGenerated as $index=>$billingQuoteNotGeneratedx)
        <tr>
            <td>
                {{$billingQuoteNotGeneratedx->created_at}}
            </td>
            <td>
                <button type="button" class="btn-edit-quoty btn btn-primary pull-right" data-project="{{ $project }}"
                        data-billing="{{ $billing }}" data-quoty="{{$billingQuoteNotGeneratedx}}"><i class="fa fa-edit"
                                                                                                     aria-hidden="true"></i>
                    Edit Quoty
                </button>
                <button type="button" class="btn-generate-quoty btn btn-primary pull-right"
                        data-project="{{ $project }}" data-billing="{{ $billing }}"
                        data-invoice="{{$billingQuoteNotGeneratedx}}"><i class="fa fa-envelope-open"
                                                                         aria-hidden="true"></i> Generate PDF
                </button>
            </td>
        </tr>

    @endforeach
    </tbody>
</table>