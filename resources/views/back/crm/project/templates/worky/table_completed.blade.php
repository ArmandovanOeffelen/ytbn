<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Worky - Completed</h2>
        </div>
        <div class="pull-right">
            <button type="button" class="addFinishedWorky btn btn-primary pull-right" data-project="{{ $project }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Finished Worky</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td>Job</td>
                <td>Total Hours</td>
                <td>Price /H</td>
                <td>VAT</td>
                <td>Total Price</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($completedWorky as $completedWorkies)
                <tr>
                    <td>{{$completedWorkies->job}}</td>
                    <td>{{round($completedWorkies->total_time/3600,2,PHP_ROUND_HALF_UP)}}</td>
                    <td>€ {{$completedWorkies->price}}</td>
                    <td>{{$completedWorkies->vat}}</td>
                    <td>€ {{$completedWorkies->total_price}}</td>
                    <td>
                        <button type="button" class="btn-worky restartWorky btn btn-success pull-right"
                                data-project="{{ $project }}" data-worky="{{ $completedWorkies }}">
                            <i class="fa fa-check"aria-hidden="true"></i>
                            Restart Worky
                        </button>
                        <button type="button" class="btn-worky deleteWorky btn btn-danger pull-right"
                                data-project="{{ $project }}" data-worky="{{ $completedWorkies }}">
                            <i class="fa fa-trash-alt"aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        
            </tbody>
        </table>
    </div>
</div>