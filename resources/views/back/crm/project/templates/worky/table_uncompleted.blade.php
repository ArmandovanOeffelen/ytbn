<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h2>Worky - Running</h2>
        </div>
        <div class="pull-right">
            <button id="startWorky" type="button" class="btn btn-primary pull-right" data-project="{{ $project }}"><i class="fa fa-plus" aria-hidden="true"></i> Add Worky</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="overViewTable table table-striped">
            <thead>
            <tr>
                <td>Service</td>
                <td>Job</td>
                <td>Total time</td>
                <td>Price /H</td>
                <td>VAT</td>
                <td>Paused</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($noncompletedWorky as $nonCompleted)
                <tr>
                    <td>{{$nonCompleted->billingService->name}}</td>
                    <td>{{$nonCompleted->job}}</td>
                    <td>{{round($nonCompleted->total_time/3600,2,PHP_ROUND_HALF_UP)}}</td>
                    <td>{{$nonCompleted->price}}</td>
                    <td>21</td>
                    @if($nonCompleted->paused == 0)
                        <td><i class="fa fa-times" aria-hidden="true"></i></td>
                    @else
                        <td><i class="fa fa-check" aria-hidden="true"></i></td>
                    @endif
                    <td>
                        <button type="button" class="btn-worky completeWorky btn btn-success pull-right"
                                data-project="{{ $project }}" data-worky="{{ $nonCompleted }}">
                            <i class="fa fa-check"aria-hidden="true"></i>
                            Complete Worky
                        </button>
                        @if($nonCompleted->paused == 0)
                            <button type="button" class="btn-worky pauseWorky btn btn-warning pull-right"
                                    data-project="{{ $project }}" data-worky="{{ $nonCompleted }}">
                                <i class="fa fa-pause"ria-hidden="true"></i>
                            </button>
                        @else
                            <button type="button" class="btn-worky unPauseWorky btn btn-success pull-right"
                                    data-project="{{ $project }}" data-worky="{{ $nonCompleted }}">
                                <i class="fa fa-play"aria-hidden="true"></i>
                            </button>
                        @endif
                        <button type="button" class="btn-worky deleteWorky btn btn-danger pull-right"
                                data-project="{{ $project }}" data-worky="{{ $nonCompleted }}">
                            <i class="fa fa-trash-alt"aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>