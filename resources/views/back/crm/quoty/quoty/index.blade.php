@extends('back.layouts.layout')

@section('content')
    @include('back.crm.quoty.quoty.template.index')
@endsection
@section('templates')
    <template id="template_createQuoty">
        @include('back.crm.forms.billing.quote.create')
    </template>
    <template id="template_delete">
        <span>Are you sure that you want to delete this organisation?</span>
    </template>
    <template id="template_loading">
        <span>Loading...</span>
    </template>
@endsection

@section('scripts')
    @parent

    <script>
        var variables = {
            updateQuotyUrl: '{{ url('admin/ajax/template/crm/project/{0}/Billing/{1}/quoty/{2}/update-quoty')}}',
            showQuotyUrl: '{{ url('/admin/ajax/template/crm/project/{0}/Billing/{1}/quoty/{2}/show-quoty')}}',
            createQuotyUrl: '{{ url('admin/ajax/template/crm/project/{0}/billing/{1}/create-quote-for-overview')}}',
        };

    </script>
    <script src="{{ asset('js/pages/back/quoty/quoty.js') }}"></script>
@endsection
