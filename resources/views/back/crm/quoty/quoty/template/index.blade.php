@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="pull-left">
                <h2>Quoty overview - Project : {{$project->name}}</h2>
            </div>
        </div>
        <div class="col-md-7">
            <div class="pull-right">
                <a href="{{ url('/back/crm/project/'.$project->id) }}" type="button" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back to Project</a>
                <button id="buttonCreateOrganisation" type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> See all Invoicies</button>
                <button type="button" class="createQuoty btn btn-primary pull-right" data-project="{{ $project }}"
                        data-billing="{{$billing}}">
                    <i class="fa fa-money-bill-wave" aria-hidden="true"></i>
                    Create Quoty
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="overViewTable table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>name</th>
                    <th>date created</th>
                    <th>created by</th>
                    <th>date sent</th>
                    <th class="tableButtonsProspects">
                        <div class="pull-right btn-increase">
                            <div class="input-group-btn search-panel">
                                <span class="input-group-btn">
                                    <input type="text" class="form-control btn-increase" name="x" placeholder="Search term...">
                                    <button class="btn btn-default btn-increase" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($quoties as $index=>$quoty)
                    <tr data-data="{{ $quoty }}">
                        <td>{{ ($quoties->perPage() * ($quoties->currentPage() - 1)) + $index + 1 }}</td>
                        <td>
                            @if(isset($quoty->billingQuoteStorage->name))
                                {{$quoty->billingQuoteStorage->name}}
                            @else
                                <p>This quoty is not generated to a pdf yet.</p>
                            @endif
                        </td>
                        <td>{{$quoty->created_at->format('d/m/Y')}}</td>
                        <td>{{$quoty->created_at->format('d/m/Y')}}</td>
                        <td class="text-right">
                            @if($quoty->has_pdf === 0)
                                <a type="button"
                                   href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quote/'.$quoty->id.'/send-quote')}}"
                                   class=" btn btn-success pull-right disabled" disabled><i class="fa fa-envelope" aria-hidden="true"></i>
                                    Send Quoty
                                </a>
                                <button type="button" class="btn-generate-quoty btn btn-primary pull-right"
                                        data-project="{{ $project }}" data-billing="{{ $billing }}"
                                        data-invoice="{{$quoty}}">
                                    <i class="fa fa-envelope-open"aria-hidden="true"></i>
                                    Generate PDF
                                </button>
                                <button type="button" class="btn-edit-quoty btn btn-primary pull-right" data-project="{{ $project }}"
                                        data-billing="{{ $billing }}" data-quoty="{{$quoty}}">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    Edit Quoty
                                </button>
                            @else
                                <a type="button"
                                   href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quote/'.$quoty->id.'/send-quote')}}"
                                   class=" btn btn-success pull-right"><i class="fa fa-envelope" aria-hidden="true"></i>
                                    Send Quoty
                                </a>
                                <a type="button"
                                   href="{{url('back/crm/project/'.$project->id.'/Billing/'.$billing->id.'/quote/'.$quoty->id.'/download')}}"
                                   class=" btn btn-primary pull-right"><i class="fa fa-file-download" aria-hidden="true"></i> Download
                                    Quoty
                                </a>
                                <button type="button" class="btn-edit-quoty btn btn-primary pull-right disabled" data-project="{{ $project }}"
                                        data-billing="{{ $billing }}" data-quoty="{{$quoty}}" disabled>
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    Edit Quoty
                                </button>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! $quoties->links() !!}
            </div>
        </div>
    </div>
</div>