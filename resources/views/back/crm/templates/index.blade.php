@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <h2>Overview - CRM</h2>
    </div>
    <div class="row">
        <div class="pull-left">
        </div>
    </div>
    <div class="row">
        <div class="pull-right">
            <button id="buttonCreateOrganisation" type="button" class="btn btn-primary buttonCreateOrganisation"><i class="fa fa-plus" aria-hidden="true"></i> add organistation</button>
            <button id="buttonCreateProject" type="button" class="btn btn-primary buttonCreateProject"><i class="fa fa-plus" aria-hidden="true"></i> add project</button>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="pull-left">
              <h2>Recently added organisations</h2>
            </div>
            <table class="overViewTable table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>name</th>
                    <th>phone number</th>
                    <th>email</th>
                    <th class="tableButtonsProspects"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($organisations as $index => $organisation)
                        <tr data-data="{{ $organisation }}">
                            <td>#</td>
                            <td>{{$organisation->name}}</td>
                            <td>{{$organisation->phone}}</td>
                            <td>{{$organisation->email}}</td>
                            <td class="text-right">
                                <a a href="{{ url('/back/crm/organisation/'.$organisation->id) }}" type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View organisation</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-right">
                <a href="{{ url('/back/organisations/overview') }}" class="buttonUpdate btn btn-primary"> See all Organisations</a>
            </div>
        </div>
        <div class="col-lg-6">
            <h2>Recently added projects</h2>
            <table class="overViewTable table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>project name</th>
                    <th>organisation</th>
                    <th>project manager</th>
                    <th class="tableButtonsProspects"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($projectOverview as $index=>$project)
                        <tr data-data="{{ $project }}">
                            <td>#</td>
                            <td>{{$project->name}}</td>
                            <td>{{$project->organisation->name}}</td>
                            <td>{{$project->User->name}}</td>
                            <td class="text-right">
                                <a href="{{ url('/back/crm/project/'.$project->id) }}" type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View project</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>