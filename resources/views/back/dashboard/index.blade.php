@extends('back.layouts.layout')

@section('title', 'dashboard')

@section('content')
    @include('back.dashboard.templates.index')
@endsection

@section('scripts')
    @parent
@endsection
