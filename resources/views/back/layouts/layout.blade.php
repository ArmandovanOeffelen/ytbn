<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--TODO: Dit uncommenten when live gaan--}}
    {{--<meta http-equiv="refresh" content="60" >--}}

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>@yield('title') - YTBN</title>

    <!-- styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('sass/app.scss') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('library/bootstrap3-dialog/css/bootstrap-dialog.css') }}">
    <link rel="stylesheet" href="{{ asset('library/jquery-ui/jquery-ui.css') }}">

    @yield('styles')
    <script src="{{ asset('library/jquery/js/jquery.min.js') }}"></script>
    <script>

        var urlPrefix = '/';

    </script>
    @yield('scripts-head')


</head>
<body id="page">

<div id="app">
    {{--TODO:: Quick create in menu dynamisch maken.--}}
    @include('back.layouts.nav')
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main">
                <div class="col-sm-12 col-md-12 well" id="content">
                    @yield('content')
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div><!-- /#wrapper -->

@include('back.layouts.footer')

<div class="push"></div>

@yield('templates')

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('library/bootstrap3-dialog/js/bootstrap-dialog.js') }}"></script>
<script src="{{ asset('library/jquery-ui/jquery-ui.js') }}"></script>
<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('scripts')

</body>
</html>