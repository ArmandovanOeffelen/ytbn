<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/back/home')}}">
                <img class="logo" title="" src="{{asset($company->company_logo_path)}}" />
            </a>
        </div>
        <!-- Top Menu Items -->
        {{--TODO:: Quick create in menu dynamisch maken.--}}
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quick Create <b class="fa fa-angle-down"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#"><i class="fa fa-fw fa-user"></i> Organisation</a></li>
                    <li><a href="#"><i class="fa fa-fw fa-cog"></i> Project</a></li>
                </ul>
            </li>
            <li><a href="#" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="Stats"><i class="fa fa-bar-chart-o"></i>
                </a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Armando <b class="fa fa-angle-down"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#"><i class="fa fa-fw fa-user"></i> Edit Profile</a></li>
                    <li><a href="#"><i class="fa fa-fw fa-cog"></i> Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-fw fa-power-off"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-fw fa-users"></i>&nbsp; CRM<i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-1" class="collapse">
                        <li><a href="{{ url('/back/crm/overview') }}"><i class="fa fa-angle-double-right"></i> Overview</a></li>
                        <li><a href="{{ url('/back/crm/organisations/overview') }}"><i class="fa fa-angle-double-right"></i> All organisations</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-2"><i class="fa fa-clock"></i>&nbsp; Worky<i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-2" class="collapse">
                        <li><a href="{{ url('/back/worky/overview') }}"><i class="fa fa-angle-double-right"></i> General Worky</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> Projecten</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> Nieuw Project</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-3"><i class="fa fa-fw fa-file-invoice-dollar"></i>&nbsp; Quoty<i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-3" class="collapse">
                        <li><a href="{{ url('/back/quoty/overview') }}"><i class="fa fa-angle-double-right"></i> Overview</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> All Quoties</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> All Invoicies</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i> Sent Quoties</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-4"><i class="fa fa-align-justify"></i>&nbsp; Products<i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-4" class="collapse">
                        <li><a href="{{ url('/back/product/overview') }}"><i class="fa fa-angle-double-right"></i> All products</a></li>
                        <li><a href="{{ url('/back/product-category/overview') }}"><i class="fa fa-angle-double-right"></i> All categories</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#submenu-5"><i class="fa fa-fw fa-cogs"></i>&nbsp; Settings<i class="fa fa-fw fa-angle-down pull-right"></i></a>
                    <ul id="submenu-5" class="collapse">
                        <li><a href="{{ url('/back/settings/general') }}"><i class="fa fa-angle-double-right"></i> General Settings</a></li>
                        <li><a href="{{ url('/back/settings/company/') }}"><i class="fa fa-angle-double-right"></i> Company Settings</a></li>
                        <li><a href="{{ url('/back/settings/worky/') }}"><i class="fa fa-angle-double-right"></i> Worky Settings</a></li>
                        <li><a href="{{ url('/back/settings/quoty') }}"><i class="fa fa-angle-double-right"></i> Quoty Settings</a></li>
                        <li><a href="{{ url('/back/settings/employee/') }}"><i class="fa fa-angle-double-right"></i> Employee Settings</a></li>
                        <li><a href="{{ url('/back/settings/dashboard') }}"><i class="fa fa-angle-double-right"></i> Dashboard Settings</a></li>
                    </ul>
                </li>
                {{--<li>--}}
                    {{--<a href="#" data-toggle="collapse" data-target="#submenu-6"><i class="fa fa-fw fa-book-open"></i>&nbsp; Wiki<i class="fa fa-fw fa-angle-down pull-right"></i></a>--}}
                    {{--<ul id="submenu-6" class="collapse">--}}
                        {{--<li><a href="{{ url('/back/settings/dashboard') }}"><i class="fa fa-angle-double-right"></i> CRM Wiki</a></li>--}}
                        {{--<li><a href="{{ url('/back/settings/company/') }}"><i class="fa fa-angle-double-right"></i> Company Wiki</a></li>--}}
                        {{--<li><a href="{{ url('/back/settings/quoty') }}"><i class="fa fa-angle-double-right"></i> Quoty Wiki</a></li>--}}
                        {{--<li><a href="{{ url('/back/settings/employee/') }}"><i class="fa fa-angle-double-right"></i> Employee Wiki</a></li>--}}
                        {{--<li><a href="{{ url('/back/settings/dashboard') }}"><i class="fa fa-angle-double-right"></i> Dashboard Wiki</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>