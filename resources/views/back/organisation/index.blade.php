@extends('back.layouts.layout')

@section('title', 'dashboard')

@section('content')
    @include('back.organisation.templates.index')
@endsection

@section('scripts')
    @parent

    <script>
        $(function () {

            var templates = {
                createOrganisation: `@include('back.crm.forms.organisation.create')`,
                {{--createCrmProject: `@include('back.crm.forms.createCrmProject')`,--}}
                delete: '<span>Are you sure that you want to delete this {0} organisation?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            /* Create */
                .on('click', '#buttonCreateOrganisation', function () {
                    BootstrapDialog.show({
                        title: 'Add organisation',
                        message: templates.createOrganisation,
                        nl2br: false,
                        buttons: [{
                            label: 'Cancel',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('admin/ajax/template/crm/organisations/overview/create-organisation')}}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        website: dialog.getModalContent().find('input[name="website"]').val(),
                                        phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),
                                        adress_street: dialog.getModalContent().find('input[name="adress_street"]').val(),
                                        adress_street_number: dialog.getModalContent().find('input[name="adress_street_number"]').val(),
                                        adress_postal: dialog.getModalContent().find('input[name="adress_postal"]').val(),
                                        adress_city: dialog.getModalContent().find('input[name="adress_city"]').val(),
                                        adress_state: dialog.getModalContent().find('input[name="adress_state"]').val(),
                                        adress_country: dialog.getModalContent().find('input[name="adress_country"]').val(),
                                    },
                                    success: function (data) {
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                //Edit Organisation
                .on('click', '.editOrganisation', function() {
                    var objectData = $('.editOrganisation').data('data');
                    BootstrapDialog.show({
                        title: 'create project',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/crm/organisation/{0}') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'Cancel',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('admin/ajax/template/crm/organisation/{0}/update')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        website: dialog.getModalContent().find('input[name="website"]').val(),
                                        phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),
                                        adress_street: dialog.getModalContent().find('input[name="adress_street"]').val(),
                                        adress_street_number: dialog.getModalContent().find('input[name="adress_street_number"]').val(),
                                        adress_postal: dialog.getModalContent().find('input[name="adress_postal"]').val(),
                                        adress_city: dialog.getModalContent().find('input[name="adress_city"]').val(),
                                        adress_state: dialog.getModalContent().find('input[name="adress_state"]').val(),
                                        adress_country: dialog.getModalContent().find('input[name="adress_country"]').val(),
                                    },
                                    success: function (data) {
                                        dialog.close();
                                        $('#content').html(data);
                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })

                /* Delete organisation */
                .on('click', '.buttonDelete', function () {
                    var objectData = $(this).data('data');
                    BootstrapDialog.show({
                        title: 'Delete Worky',
                        message: templates.delete.format(objectData.name),
                        nl2br: false,
                        buttons: [{
                            label: 'Cancel',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/crm/organisations/overview/delete-organisation/{0}')}}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(_error16) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (_error16.responseJSON.error != null) {
                                            $('#errors').html(_error16.responseJSON.error.html ? _error16.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                })

                //create project
                .on('click', '#buttonCreateProject', function () {
                    BootstrapDialog.show({
                        title: 'Add project',
                        message: templates.createCrmProject,
                        nl2br: false,
                        onshown: function(dialog) {
                            dialog.$modal.find('.js-organisation-select').selectize({});
                            dialog.$modal.find('.js-project-status-select').selectize({});
                            dialog.$modal.find('.js-project-manager-select').selectize({});
                        },
                        buttons: [{
                            label: 'Cancel',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('admin/ajax/template/crm/overview')}}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        //project details
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        project_manager: dialog.getModalContent().find('select[name="project_manager"]').val(),
                                        organisation: dialog.getModalContent().find('select[name="organisation"]').val(),
                                        project_status: dialog.getModalContent().find('select[name="project_status"]').val(),
                                        start_date: dialog.getModalContent().find('input[name="start_date"]').val(),
                                        end_date: dialog.getModalContent().find('input[name="end_date"]').val(),
                                        project_summary: dialog.getModalContent().find('textarea[name="project_summary"]').val(),
                                        //contactPerson
                                        contactperson_name: dialog.getModalContent().find('input[name="contactperson_name"]').val(),
                                        contactperson_email: dialog.getModalContent().find('input[name="contactperson_email"]').val(),
                                        contactperson_phone: dialog.getModalContent().find('input[name="contactperson_phone"]').val(),
                                    },
                                    success: function (data) {
                                        console.log(data);
                                        dialog.close();

                                        $('#content').html(data.html);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
