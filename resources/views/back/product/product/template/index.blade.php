@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <div class="pull-left">
                <h4>Overview - all products</h4>
            </div>
        </div>
        <div class="col-md-10">
            <div class="pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> add product</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="overViewTable table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Category</th>
                    <th>Product name</th>
                    <th>Product price</th>
                    <th class="tableButtonsProspects">
                        <div class="pull-right btn-increase">
                            <div class="input-group-btn search-panel">
                                <span class="input-group-btn">
                                    <input type="text" class="form-control btn-increase" name="x" placeholder="Search term...">
                                    <button class="btn btn-default btn-increase" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                {{--@foreach($paginationOrganisations as $index=>$organisation)--}}
                    {{--<tr data-data="{{ $organisation }}">--}}
                        {{--<td>{{ ($paginationOrganisations->perPage() * ($paginationOrganisations->currentPage() - 1)) + $index + 1 }}</td>--}}
                        {{--<td>{{$organisation->name}}</td>--}}
                        {{--<td>{{$organisation->phone}}</td>--}}
                        {{--<td>{{$organisation->email}}</td>--}}
                        {{--<td class="text-right">--}}
                            {{--<a href="{{ url('/back/crm/organisation/'.$organisation->id) }}" type="button" class="btn btn-primary"><i class="fas fa-building" aria-hidden="true"></i>&nbsp; Organisation overview</a>--}}
                            {{--<a href="{{ url('/back/crm/project/'.$organisation->id) }}" type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Check Projects</a>--}}
                            {{--<a type="button" class="editOrganisation btn btn-primary" data-data="{{$organisation}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Organisation</a>--}}
                            {{--<a type="button" class="disabled btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Check Clocky</a>--}}
                            {{--<a type="button" class="disabled btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Check Worky</a>--}}
                            {{--<button type="button" class="buttonDelete btn btn-danger" data-data="{{$organisation}}"><i class="fa fa-trash" aria-hidden="true"></i></button>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                </tbody>
            </table>
            <div class="text-center">
                {{--{!! $paginationOrganisations->links() !!}--}}
            </div>
        </div>
    </div>
</div>