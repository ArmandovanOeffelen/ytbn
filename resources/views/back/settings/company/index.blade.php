@extends('back.layouts.layout')

@section('title', 'Company settings')

@section('content')
    @include('back.settings.company.template.index')
@endsection

@section('templates')
    <template id="template_delete">
        <span>Are you sure that you want to delete this organisation?</span>
    </template>
    <template id="template_loading">
        <span>Loading...</span>
    </template>
@endsection

@section('scripts')
    @parent

    <script>
        var variables = {
            loadCompanyLogoUrl: '{{ url('/admin/ajax/template/settings/company/{0}/loadCompanyLogo') }}',
            updateLogoUrl: '{{ url('/admin/ajax/template/settings/company/{0}/updatelogo') }}',
            loadCompanyUrl: '{{ url('/admin/ajax/template/settings/company/{0}/loadCompany') }}',
            updateCompanyUrl: '{{ url('/admin/ajax/template/settings/company/{0}/update') }}',
        };

    </script>
    <script src="{{ asset('js/pages/back/settings/company.js') }}"></script>
@endsection
