@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="pull-left">
            <h2>Company Settings</h2>
        </div>
        <div class="pull-right">
            <button id="buttonUpdate" type="button" class="btn btn-primary buttonUpdate" data-data="{{ $company }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Company</button>
            <button id="buttonUpdate" type="button" class="btn btn-primary buttonUpdateLogo disabled" data-data="{{ $company }}"><i class="fa fa-image" aria-hidden="true"></i> Edit Logo</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="overViewTable table table-striped">
                <thead>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr>

                    <td>Company Logo</td>
                    <td><img src="{{asset($company->company_logo_path)}}" /></td>
                </tr>
                <tr>
                    <td>Company Name</td>
                    <td>{{$company->name}}</td>
                </tr>
                <tr>
                    <td>Company email</td>
                    <td>{{$company->email}}</td>
                </tr>
                <tr>
                    <td>Company website</td>
                    <td>{{$company->website}}</td>
                </tr>
                <tr>
                    <td>Company phone</td>
                    <td>{{$company->phone}}</td>
                </tr>
                <tr>
                    <td>Company street</td>
                    <td>{{$company->street}} {{$company->street_number}}</td>
                </tr>
                <tr>
                    <td>Company postalcode</td>
                    <td>{{$company->postalcode}}</td>
                </tr>
                <tr>
                    <td>Company city</td>
                    <td>{{$company->city}}</td>
                </tr>
                <tr>
                    <td>Company state</td>
                    <td>{{$company->state}}</td>
                </tr>
                <tr>
                    <td>Company country</td>
                    <td>{{$company->country}}</td>
                </tr>
                <tr>
                    <td>Company CoC</td>
                    <td>{{$company->coc_number}}</td>
                </tr>
                <tr>
                    <td>Company VAT number</td>
                    <td>{{$company->vat_number}}</td>
                </tr>
                <tr>
                    <td>Company bank account</td>
                    <td>{{$company->bankaccount_number}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>