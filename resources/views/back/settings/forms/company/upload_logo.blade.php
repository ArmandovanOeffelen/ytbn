<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Upload logo</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="text-center"><h4>Current Logo</h4></div>
       <div class="text-center"><img src="{{asset($company->company_logo_path)}}" /></div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="form-group{{$errors->has('logo') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <input id="company-logo" class="form-control" name="logo" type="file" value="{{ Request::input('logo')}}">
            </label>
            @if ($errors->has('logo'))
                <span class="help-block">{{ $errors->first('logo') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>