<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Details service</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-8">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Service name</span>
                <input class="form-control" name="name" type="text" value="{{ Request::input('name')}}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{$errors->has('vat') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Vat %</span>
                <input class="form-control" name="vat" type="text" value="{{ Request::input('vat')}}">
            </label>
            @if ($errors->has('vat'))
                <span class="help-block">{{ $errors->first('vat') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>


