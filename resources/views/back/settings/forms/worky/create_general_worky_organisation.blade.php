<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Worky</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Worky name</span>
                <input class="form-control" name="name" type="text" value="{{ Request::input('name')}}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{$errors->has('price') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Price</span>
                <input class="form-control" name="price" type="text" value="{{ Request::input('price')}}">
            </label>
            @if ($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-7">
        <div class="form-group{{$errors->has('price') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Price</span>
                <select class="form-control">
                    @foreach($billingService as $index => $service)
                        <option value="{{$service}}">{{$service->name}}</option>
                    @endforeach
                </select>
            </label>
            @if ($errors->has('price'))
                <span class="help-block">{{ $errors->first('price') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-3">
        <label class="control-label checkbox">
            <span>Price</span>
            <br />
            <span style="padding-top:15px;">21 %</span>
        </label>
    </div>
    <div class="col-md-1"></div>
</div>


