<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <h4>Worky</h4>
    </div>
    <div class="col-md-1"></div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-8">
        <div class="form-group{{$errors->has('name') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Worky name</span>
                <input class="form-control" name="name" type="text" value="{{$workyGeneralJob->name ?? Request::input('name')}}">
            </label>
            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-1"></div>
</div>


