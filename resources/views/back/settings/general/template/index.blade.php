@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="pull-left">
            <h2>General Settings</h2>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-lg-6">
                <div class="pull-left">
                    <h4>Delete this</h4>
                </div>
                @include('back.settings.general.template.vat_table')
            </div>
            <div class="col-lg-6">
                <h4>Service overview</h4>
                @include('back.settings.general.template.service_table')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="pull-left">
                <h4>Quoty PDF selector</h4>
            </div>
        </div>
        <div class="col-lg-6">
            <h4>Invoicy PDF selector</h4>
        </div>
    </div>
</div>