<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <td>Service name</td>
        <td>Vat %</td>
        <td>
            <div class="pull-right">
                <button type="button" class="addService btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Service</button>
            </div>
        </td>
    </tr>
    </thead>
    <tbody>
        @foreach($service as $services)
            <tr>
                <td>{{$services->name}}</td>
                <td>{{$services->vat .' %'}}</td>
                <td>
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" data-data="{{$services}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>