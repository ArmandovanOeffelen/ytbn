<table class="table table-striped table-responsive">
    <thead>
        <tr>
            <td>Amount</td>
            <td>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Vat</button>
                </div>
            </td>
        </tr>
    </thead>
    <tbody>
        @foreach($allVat as $vat)
            <tr>
                <td>{{$vat->vat ." %"}}</td>
                <td>
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" data-data="{{$vat}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>