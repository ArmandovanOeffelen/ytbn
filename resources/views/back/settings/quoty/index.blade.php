@extends('back.layouts.layout')

@section('title', 'Quoty settings')

@section('content')
    @include('back.settings.quoty.template.index')
@endsection

@section('scripts')
    @parent

    <script>
        $(function () {

            var templates = {
                {{--createOrganisation: `@include('back.crm.forms.createOrganisation')`,--}}
                        {{--createCrmProject: `@include('back.crm.forms.createCrmProject')`,--}}
                delete: '<span>Are you sure that you want to delete this organisation?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            //update Company
                .on('click', '.buttonUpdateLogo', function() {
                    var objectData = $(this).data('data');
                    BootstrapDialog.show({
                        title: 'Upload logo',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/settings/company/{0}/loadCompanyLogo') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'cancel',
                            cssClass: 'btn-danger',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function(dialog){
                                dialog.enableButtons(false);
                                dialog.setClosable(false);
                                var logo = $('#company-logo').prop('files')[0];
                                console.log(logo);
                                $.ajax({
                                    type: 'put',
                                    contentType: false,
                                    processData: false,
                                    url: '{{ url('/admin/ajax/template/settings/company/{0}/updatelogo') }}'
                                        .format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        logo: logo

                                    },
                                    success: function(data){
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                //update Company
                .on('click', '.buttonUpdate', function() {
                    console.log('updatebutton clicked')

                    var objectData = $(this).data('data');
                    BootstrapDialog.show({
                        title: 'Edit company',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/settings/company/{0}/loadCompany') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'cancel',
                            cssClass: 'btn-danger',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function(dialog){
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('/admin/ajax/template/settings/company/{0}/update') }}'
                                        .format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                        email: dialog.getModalContent().find('input[name="email"]').val(),
                                        phone: dialog.getModalContent().find('input[name="phone"]').val(),
                                        website: dialog.getModalContent().find('input[name="website"]').val(),
                                        adress_street: dialog.getModalContent().find('input[name="adress_street"]').val(),
                                        adress_street_number: dialog.getModalContent().find('input[name="adress_street_number"]').val(),
                                        adress_postal: dialog.getModalContent().find('input[name="adress_postal"]').val(),
                                        adress_city: dialog.getModalContent().find('input[name="adress_city"]').val(),
                                        adress_state: dialog.getModalContent().find('input[name="adress_state"]').val(),
                                        adress_country: dialog.getModalContent().find('input[name="adress_country"]').val(),
                                        coc_number: dialog.getModalContent().find('input[name="coc_number"]').val(),
                                        vat_number: dialog.getModalContent().find('input[name="vat_number"]').val(),
                                        bankaccount_number: dialog.getModalContent().find('input[name="bankaccount_number"]').val()
                                    },
                                    success: function(data){
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
