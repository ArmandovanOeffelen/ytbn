@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="pull-left">
            <h2>Quoty Settings</h2>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-lg-6">
                <div class="pull-left">
                    <h4>Change mail for Quoty</h4>
                </div>
                @include('back.settings.quoty.template.template.table_mail_quoty')
            </div>
            <div class="col-lg-6">
                <h4>Change mail for Invoicy</h4>
                @include('back.settings.quoty.template.template.table_mail_invoicy')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
          <div class="pull-left">
              <h4>Quoty PDF selector</h4>
          </div>
        </div>
        <div class="col-lg-6">
            <h4>Invoicy PDF selector</h4>
        </div>
    </div>
</div>