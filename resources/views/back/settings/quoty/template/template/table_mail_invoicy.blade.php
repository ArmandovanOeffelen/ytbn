<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <td>mail-part</td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Intro</td>
        <td>
            <div class="pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
            </div>
        </td>
    </tr>
    <tr>
        <td>middle</td>
        <td>
            <div class="pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
            </div>
        </td>
    </tr>
    <tr>
        <td>outro</td>
        <td>
            <div class="pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
            </div>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><b>check mail</b></td>
        <td>
            <div class="pull-right">
                <button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Check</button>
            </div>
        </td>
    </tr>
    </tbody>
</table>