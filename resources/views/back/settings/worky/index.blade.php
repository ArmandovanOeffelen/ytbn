@extends('back.layouts.layout')

@section('title', 'Worky settings')

@section('content')
    @include('back.settings.worky.templates.index')
@endsection

@section('scripts')
    @parent

    <script>
        $(function () {

            var templates = {
                delete: '<span>Are you sure that you want to delete this organisation?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            //Add worky
                .on('click', '.addWorky', function() {
                    BootstrapDialog.show({
                        title: 'Add general worky for organisations',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/settings/worky/add-worky-form') }}',
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'cancel',
                            cssClass: 'btn-danger',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'post',
                                    url: '{{ url('admin/ajax/template/settings/worky/add-worky')}}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                    },
                                    success: function (data) {
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                })
                //Edit worky
                .on('click', '.editWorky', function() {
                    var objectData = $(this).data('data');
                    BootstrapDialog.show({
                        title: 'Add general worky for organisations',
                        message: $(templates.loading)
                            .load('{{ url('/admin/ajax/template/settings/worky/{0}/load-worky-form') }}'.format(objectData.id),
                                function(response, status) {
                                    if (status == 'error') {
                                        var data = JSON.parse(response);
                                        if (data.error != null) {
                                            $('#errors').html(data.error.html ? data.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                }),
                        nl2br: false,
                        buttons: [{
                            label: 'cancel',
                            cssClass: 'btn-danger',
                            action: function(dialogRef){
                                dialogRef.close();
                            }
                        }, {
                            id: 'submit',
                            label: 'Save',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('/admin/ajax/template/settings/worky/{0}/update-worky') }}'.format(objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        name: dialog.getModalContent().find('input[name="name"]').val(),
                                    },
                                    success: function (data) {
                                        dialog.close();

                                        $('#content').html(data);

                                    },
                                    error: function (error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('submit').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        } else {
                                            dialog.setMessage(error.responseJSON.html);
                                        }

                                    }
                                });

                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection
