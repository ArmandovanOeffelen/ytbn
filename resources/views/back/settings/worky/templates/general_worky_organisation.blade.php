<table class="table table-striped table-responsive">
    <thead>
    <tr>
        <td>Worky name</td>
        <td>
            <div class="pull-right">
                <button type="button" class="addWorky btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Worky</button>
            </div>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($workyJob as $worky)
        <tr>
            <td>{{$worky->name}}</td>
            <td>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary editWorky" data-data="{{$worky}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                    <button type="button" class="btn btn-danger" data-data="{{$worky}}"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>