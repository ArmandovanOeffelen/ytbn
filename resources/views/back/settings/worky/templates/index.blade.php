@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="pull-left">
            <h2>Worky Settings</h2>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-lg-6">
                <div class="pull-left">
                    <h4>General Workies for organisations</h4>
                </div>
                @include('back.settings.worky.templates.general_worky_organisation')
            </div>
            <div class="col-lg-6">
                <h4>Change mail for Invoicy</h4>
                @include('back.settings.quoty.template.template.table_mail_invoicy')
            </div>
        </div>
    </div>
</div>