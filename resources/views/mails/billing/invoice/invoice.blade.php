Beste {{$contactPerson->name}},

Tijdens ons kennismakings gesprek aan de hand van uw vraag naar product 'x' <br />
hebben wij een offerte opgesteld. Deze offerte hebben wij voor u opgemaakt. De offerte kunt u vinden <br />
in de bijlage. Tevens hebben wij de project status ( die u binnenkort online volgen kunt.) gewijzigd naar: <br />
<i>Insert project Status here later on.</i>
<br />
<br />
Mocht u nog vragen hebben kunt u deze altijd stellen!

Met vriendelijke groeten,
<br />
{{ $company->name }}<br />
{{ $company->phone }} <br />
{{ $company->website}}