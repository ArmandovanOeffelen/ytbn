<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', function () {
        return view('welcome');
        //test
    });

    Auth::routes();



    Route::group(['prefix' => 'back','middle'=> ['auth:web']], function(){

        Route::get('/home', 'DashboardController@index');

        //Organisations
        Route::get('/crm/organisations/overview', 'OrganisationController@index');

        //specifc organisation
        Route::get('/crm/organisation/{id}', 'OrganisationController@showOrganisation');

        //Crm
        Route::get('/crm/overview', 'CrmController@index');

        //Billing
        Route::get('/quoty/overview', 'BillingController@index');
        Route::get('/crm/project/{projectId}/Billing/{billingId}/invoice/{billingTypeId}/download','BillingController@downloadPDF');
        Route::get('/crm/project/{projectId}/Billing/{billingId}/quote/{billingTypeId}/download','BillingController@downloadQuotePDF');

        //Project Billing-overview
        Route::get('/crm/project/{projectId}/Billing/{billingId}/quoty-overview','BillingController@showAllQuotiesForProject');

        //Projects
        Route::get('/crm/project/{id}', 'ProjectController@index')
            ->name('project_show');

        Route::get('/crm/project/{projectId}/Billing/{billingId}/invoice/{billingTypeId}/send-invoice','BillingController@sendInvoice');
        Route::get('/crm/project/{projectId}/Billing/{billingId}/quote/{billingTypeId}/send-quote','BillingController@sendQuote');

        //Products
        Route::get('/product/overview', 'ProductController@show');
        Route::get('/product-category/overview', 'ProductCategoryController@show');

        Route::get('/worky/overview','ClockingSystemController@show');

    });

    Route::group(['prefix' => 'back/settings','middle'=> ['auth:web']], function(){


    Route::get('/company','CompanyController@show');
    Route::get('/quoty','QuotySettingsController@show');
    Route::get('/general','GeneralSettingsController@show');
    Route::get('/worky','WorkySettingsController@show');

    });

    Route::group(['prefix' => 'admin/ajax/template/','namespace' => 'Ajax\\Template', 'middleware' => 'auth:web'], function(){

        Route::get('/settings/company/{id}/loadCompany','CompanyController@show');
        Route::get('/settings/company/{company}/content','CompanyController@showContent');
        Route::get('/settings/company/{company}/content','CompanyController@showContentWithCommand');
        Route::get('/settings/company/{id}/loadCompanyLogo','CompanyController@showForLogo');
        Route::put('/settings/company/{id}/update','CompanyController@update');
        Route::post('/settings/company/{id}/updatelogo','CompanyController@updatelogo');

        Route::post('/settings/general/create-service','GeneralSettingsController@createService');
        Route::get('/settings/general/show','GeneralSettingsController@show');

        Route::post('/settings/worky/add-worky','WorkySettingsController@create');
        Route::get('/settings/worky/add-worky-form','WorkySettingsController@showForm');
        Route::get('/settings/worky/{worky}/load-worky-form','WorkySettingsController@showFormForUpdate');
        Route::put('/settings/worky/{worky}/update-worky','WorkySettingsController@update');
    });

    Route::group(['prefix' => 'admin/ajax/template/','namespace' => 'Ajax\\Template', 'middleware' => 'auth:web'], function(){


        //CRM
        Route::get('/crm/overview', 'crmController@showContent')
            ->name('admin_ajax_template_crm_overview_content');


        Route::delete('/crm/organisation/delete/{id}', 'CrmController@delete');

        //Organisations

        Route::get('/crm/overview', 'crmController@showOrganisation')
            ->name('admin_ajax_template_organisation_show_content');
        Route::post('/crm/overview/create-organisation','OrganisationController@create');
        Route::post('/crm/organisation/{id}/create-project','ProjectController@createProject');
        Route::get('/crm/organisation/{id}/show-content-for-form', 'OrganisationController@showContent');
        Route::put('/crm/organisation/{id}', 'OrganisationController@update');
        Route::put('/crm/organisation/{id}/update', 'OrganisationController@updateForAllOrganisations');
        Route::get('/crm/organisation/{id}', 'OrganisationController@show');

        //Organisations-All
        Route::post('/crm/organisations/overview/create-organisation','organisationController@createOrganisation');
        Route::delete('/crm/organisations/overview/delete-organisation/{id}','organisationController@delete');
        Route::post('/crm/organisations/overview','organisationController@showAllOrganisations');

            //Projects
        Route::post('/crm/overview/create-project','ProjectController@create');
        Route::put('/crm/project/{id}', 'ProjectController@update');
        Route::get('/crm/project/{id}', 'ProjectController@show');

        Route::get('/crm/project/{project}/content', 'ProjectController@showContent')
            ->name('admin_ajax_template_crm_project_content');

        //Project-ContactPerson
        Route::get('/crm/project/contactperson/{id}', 'ContactPersonController@show');
        Route::put('/crm/project/{projectid}/contactperson/{id}', 'ContactPersonController@update');

        //Project-Worky
        Route::post('/crm/project/{projectid}','ClockingSystemController@start');
        Route::put('/crm/project/{projectid}/pauseworky/{id}','ClockingSystemController@pause');
        Route::post('/crm/project/{projectid}/unpauseworky/{id}','ClockingSystemController@unpause');
        Route::put('/crm/project/{projectid}/completeworky/{id}','ClockingSystemController@complete');
        Route::put('/crm/project/{projectid}/restartworky/{id}','ClockingSystemController@restart');
        Route::delete('/crm/project/{projectid}/deleteworky/{id}','ClockingSystemController@delete');
        Route::get('/crm/project/{projectid}/showForm', 'ClockingSystemController@show');
        Route::get('/crm/project/{projectid}/showFormForNewWorky', 'ClockingSystemController@showNewWorky');
        Route::post('/crm/project/{projectid}/add-completed-worky','ClockingSystemController@createCompletedWorky');

        //Project-Billing-Invoicy
        Route::post('/crm/project/{projectId}/createinvoice','BillingController@create');
        Route::get('/crm/project/{projectId}/Billing/{billingId}/invoicy/{invoicyId}/show-invoicy','BillingController@showInvoicy');
        Route::put('/crm/project/{projectId}/Billing/{billingId}/invoicy/{invoicyId}/update-invoicy','BillingController@updateInvoicy');
        Route::put('/crm/project/{projectId}/Billing/{billingId}/invoice/{billingTypeId}/generate-invoice', 'BillingController@generateInvoice');

        Route::get('/crm/project/{projectId}/Billing/{billingId}/invoice/load-all-worky','BillingController@loadWorky');
        Route::put('/crm/project/{projectId}/Billing/{billingId}/invoice/create-invoicy-from-worky','BillingController@createInvoicyFromWorky');

        //Project-Billing-Quote
        Route::post('/crm/project/{projectId}/billing/{billingId}/create-quote','BillingController@createQuote');
        Route::post('/crm/project/{projectId}/billing/{billingId}/create-quote-for-overview','BillingController@createQuoteForOverview');
        Route::get('/crm/project/{projectId}/Billing/{billingId}/quoty/{quotyid}/show-quoty','BillingController@showQuoty');
        Route::put('/crm/project/{projectId}/Billing/{billingId}/quoty/{quotyid}/update-quoty','BillingController@updateQuoty');
        Route::put('/crm/project/{projectId}/Billing/{billingId}/quoty/{billingTypeId}/generate-quote', 'BillingController@generateQuote');

        //Project Billing-overview
        Route::get('/crm/project/{projectId}/Billing/{billingId}/quoty-overview','BillingController@showAllQuotiesForProject');


        //
        Route::post('/crm/organisation/{organisation}/worky/{workyJob}/start','ClockingSystemController@createWorkyForOrganisation');


    });