let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/pages/back/project/index.js', 'public/js/pages/back/project');
mix.js('resources/assets/js/pages/back/settings/company.js', 'public/js/pages/back/settings');
mix.js('resources/assets/js/pages/back/quoty/quoty.js', 'public/js/pages/back/quoty');